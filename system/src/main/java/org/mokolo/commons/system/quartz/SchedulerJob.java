package org.mokolo.commons.system.quartz;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.mokolo.commons.data.TimestampAdapter;

public class SchedulerJob {
  @XmlAttribute public String key;
  @XmlAttribute(name = "class-name") public String className;
  @XmlAttribute @XmlJavaTypeAdapter(TimestampAdapter.class) public Date nextFireTime;
  
  public Map<String, Object> toModel() {
    Map<String, Object> model = new HashMap<>();
    model.put("key", this.key);
    model.put("className", this.className);
    model.put("nextFireTime", this.nextFireTime);
    return model;
  }
}

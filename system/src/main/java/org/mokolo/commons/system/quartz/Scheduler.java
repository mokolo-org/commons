package org.mokolo.commons.system.quartz;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.extern.slf4j.Slf4j;

import org.mokolo.commons.lang.DateFormat;
import org.quartz.JobExecutionContext;
import org.quartz.JobKey;
import org.quartz.SchedulerException;
import org.quartz.impl.matchers.GroupMatcher;

@Slf4j
@XmlRootElement
public class Scheduler {
  @XmlElementWrapper(name = "job-groups")
  @XmlElement(name = "group")
  public List<JobGroup> jobGroups;
  
  @XmlElementWrapper(name = "currently-executing-jobs")
  @XmlElement(name = "job")
  public List<CurrentlyExecutingJob> currentlyExecutingJobs;
  
  public Scheduler() {
    this.jobGroups = new ArrayList<>();
    this.currentlyExecutingJobs = new ArrayList<>();
  }
  
  public void logIt() {
    for (JobGroup group : this.jobGroups)
      for (SchedulerJob job : group.schedulerJobs)
        if (job.nextFireTime != null)
          log.info(String.format("Scheduler Job: key=%s, class=%s, nextFireTime=%s",
              job.key,
              job.className,
              DateFormat.DATETIME_MS_TZ.getSimpleDateFormat().format(job.nextFireTime)));
        else
          log.info(String.format("Scheduler Job: key=%s, class=%s",
              job.key,
              job.className));
    for (CurrentlyExecutingJob job : this.currentlyExecutingJobs)
      log.info(String.format("Currently Executing Job: class=%s, fireTime=%s",
          job.className,
          DateFormat.DATETIME_MS_TZ.getSimpleDateFormat().format(job.fireTime)));
  }
  
  public static Scheduler extract(org.quartz.Scheduler quartzScheduler) {
    Scheduler scheduler = new Scheduler();

    try {
      for (String groupName : quartzScheduler.getJobGroupNames()) {
        JobGroup jobGroup = new JobGroup();
        for (JobKey jobKey : quartzScheduler.getJobKeys(GroupMatcher.jobGroupEquals(groupName))) {
          SchedulerJob job = new SchedulerJob();
          job.key = jobKey.getName();
          job.className = quartzScheduler.getJobDetail(jobKey).getJobClass().getName();
          if (quartzScheduler.getTriggersOfJob(jobKey).size() > 0)
            job.nextFireTime = quartzScheduler.getTriggersOfJob(jobKey).get(0).getNextFireTime();
          jobGroup.schedulerJobs.add(job);
        }
        scheduler.jobGroups.add(jobGroup);
      }
      
      for (JobExecutionContext jec : quartzScheduler.getCurrentlyExecutingJobs()) {
        CurrentlyExecutingJob job = new CurrentlyExecutingJob();
        job.className = jec.getJobDetail().getJobClass().getName();
        job.fireTime = jec.getFireTime();
        scheduler.currentlyExecutingJobs.add(job);
      }
    } catch (SchedulerException e) {
      log.error(e.getClass().getName()+" occurred. Msg = "+e.getMessage());
    }
    return scheduler;
  }

}

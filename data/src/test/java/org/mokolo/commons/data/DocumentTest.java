/*
 * Copyright Ⓒ 2019 Fred Vos
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.mokolo.commons.data;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.UUID;

public class DocumentTest {
  
  @Test
  public void canCreateDocumentAndSetAndGetValues() {
    Document document = new Document();
    setupDocument(document);    
    performSimpleTests(document, null);
  }
  
  @Test
  public void canCreateCloneOfDocument() {
    Document document = new Document();
    setupDocument(document);
    Document document2 = document.clone();
    performSimpleTests(document2, null);
    assertTrue(document2.getUuid().compareTo(document.getUuid()) != 0, "UUIDs must differ");
  }
  
  private static void setupDocument(Document document) {
    document.addString("foo", "bar");
    document.addString("foo", "baz");
    document.addString("dit", "dat");
  }
  
  private static void performSimpleTests(Document document, UUID uuid) {
    if (uuid != null)
      assertTrue(uuid.equals(document.getUuid()), "UUID matches");
    assertEquals(2, document.get("foo").size(), "2 values for foo");
    assertEquals("bar", document.getString("foo"), "foo(0) = bar");
    assertEquals("dat", document.getString("dit"), "dit(0) = dat");
  }
  
}

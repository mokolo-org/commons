package org.mokolo.commons.data;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import lombok.Cleanup;
import lombok.extern.slf4j.Slf4j;

import org.junit.jupiter.api.Test;

@Slf4j
public class JAXBDocumentSchemaTest {

  @Test
  public void canSerializeAndDeserializeDocumentSchemaAsXml() throws Exception {
    DocumentSchema schemaOrig = DocumentSchema.getTestSchema();

    // Serialize to String:
    JAXBDocumentSchema jaxbSchemaOrig = JAXBDocumentSchema.toJAXBDocumentSchema(schemaOrig);
    @Cleanup ByteArrayOutputStream baos = new ByteArrayOutputStream();
    JAXBDocumentSchema.serializeToXml(jaxbSchemaOrig, baos);
    String s = baos.toString();
    
    log.debug(s);
    
    // Deserialize String to Document
    @Cleanup ByteArrayInputStream bais = new ByteArrayInputStream(s.getBytes());
    JAXBDocumentSchema jaxbSchemaCopy = JAXBDocumentSchema.parseXml(bais);
    DocumentSchema schemaCopy = JAXBDocumentSchema.parseJAXBDocumentSchema(jaxbSchemaCopy);
    
    assertNotNull(schemaCopy, "Copy not null");
    assertEquals(schemaOrig.getKeyDescriptions().size(), schemaCopy.getKeyDescriptions().size(), "Equal size");    
  }


}

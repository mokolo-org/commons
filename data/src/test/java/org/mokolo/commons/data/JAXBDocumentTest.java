package org.mokolo.commons.data;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import lombok.Cleanup;
import lombok.extern.slf4j.Slf4j;

import org.junit.jupiter.api.Test;

@Slf4j
public class JAXBDocumentTest {

  @Test
  public void canSerializeAndDeserializeDocumentAsXml() throws Exception {
    Document documentOrig = DocumentSchema.getTestDocument();

    // Serialize to String:
    JAXBDocument jaxbDocumentOrig = JAXBDocument.toJAXBDocument(documentOrig);
    @Cleanup ByteArrayOutputStream baos = new ByteArrayOutputStream();
    JAXBDocument.serializeToXml(jaxbDocumentOrig, baos);
    String s = baos.toString();
    
    log.debug(s);
    
    // Deserialize String to Document
    @Cleanup ByteArrayInputStream bais = new ByteArrayInputStream(s.getBytes());
    JAXBDocument jaxbDocumentCopy = JAXBDocument.parseXml(bais);
    Document documentCopy = JAXBDocument.parseJAXBDocument(jaxbDocumentCopy);
    
    assertNotNull(documentCopy, "Copy not null");
    assertEquals(documentOrig.size(), documentCopy.size(), "Equal size");    
  }


}

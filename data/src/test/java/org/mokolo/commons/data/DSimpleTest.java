package org.mokolo.commons.data;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.regex.Matcher;

import org.junit.jupiter.api.Test;

public class DSimpleTest {

  @Test
  public void canAnalyzeLink() throws Exception {
    doTest("<a href=\"fiets\">trui</a>", "fiets", "trui");
    doTest("bla <a href=\"fiets\">trui</a> bla", "fiets", "trui");
  }
  
  private void doTest(String source, String url, String text) {
    Matcher matcher = DSimple.linkPattern.matcher(source);
    assertTrue(matcher.find());
    assertEquals(url, matcher.group(1));
    assertEquals(text, matcher.group(2));
  }

}

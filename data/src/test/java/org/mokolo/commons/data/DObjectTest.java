package org.mokolo.commons.data;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Date;

import org.junit.jupiter.api.Test;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DObjectTest {

  @Test
  public void canSerializeAndParseIntegerToAndFromXml() throws Exception {
    DInteger five = new DInteger(5);
    String fiveXml = DObject.toXml(five);
    log.debug("fiveXml:\n"+fiveXml);
    InputStream bais = new ByteArrayInputStream(fiveXml.getBytes());
    DInteger fiveB = DInteger.parseXml(bais);
    assertEquals(five.getValue().intValue(), fiveB.getValue().intValue());
  }

  @Test
  public void canSerializeAndParseDateToAndFromXml() throws Exception {
    DDate now = new DDate(new Date());
    String nowXml = DObject.toXml(now);
    log.debug("nowXml:\n"+nowXml);
    InputStream bais = new ByteArrayInputStream(nowXml.getBytes());
    DDate nowB = DDate.parseXml(bais);
    assertEquals(now.getValue(), nowB.getValue());
  }

  @Test
  public void canSerializeAndParseStringToAndFromXml() throws Exception {
    DString ann = new DString("Capsicum annuum");
    String annXml = DObject.toXml(ann);
    log.debug("annXml:\n"+annXml);
    InputStream bais = new ByteArrayInputStream(annXml.getBytes());
    DString annB = DString.parseXml(bais);
    assertEquals(ann.getValue(), annB.getValue());
  }

  @Test
  public void canSerializeAndParseListToAndFromXml() throws Exception {
    DList chilliList = new DList();
    chilliList.add(new DString("Lemon drop"));
    chilliList.add(new DString("Bishop's crown"));
    assertTrue(chilliList.contains(new DString("Lemon drop")));
    String chilliesXml = DObject.toXml(chilliList);
    log.debug("chilliesXml: "+chilliesXml);
    InputStream bais = new ByteArrayInputStream(chilliesXml.getBytes());
    DList chilliListB = DList.parseXml(bais);
    assertTrue(chilliListB.contains(new DString("Bishop's crown")));
  }

  @Test
  public void canSerializeAndParseSetToAndFromXml() throws Exception {
    DSet chilliSet = new DSet();
    chilliSet.add(new DString("Lemon drop"));
    chilliSet.add(new DString("Bishop's crown"));
    assertTrue(chilliSet.contains(new DString("Lemon drop")));
    String chilliesXml = DObject.toXml(chilliSet);
    log.debug("chilliesXml: "+chilliesXml);
    InputStream bais = new ByteArrayInputStream(chilliesXml.getBytes());
    DSet chilliSetB = DSet.parseXml(bais);
    assertTrue(chilliSetB.contains(new DString("Bishop's crown")));
  }

  @Test
  public void canSerializeAndParseMapToAndFromXml() throws Exception {
    DMap chilliMap = new DMap();
    chilliMap.put("ann", new DString("Capsicum annuum"));
    chilliMap.put("bac", new DString("Capsicum baccatum"));
    assertEquals("Capsicum baccatum", chilliMap.get("bac").toString());
    String chilliesXml = DObject.toXml(chilliMap);
    log.debug("chilliesXml: "+chilliesXml);
    log.debug("As YAML:\n"+chilliMap.toYAML());
    InputStream bais = new ByteArrayInputStream(chilliesXml.getBytes());
    DMap chilliMapB = DMap.parseXml(bais);
    assertEquals(chilliMap.getMap().size(), chilliMapB.getMap().size());
    assertEquals("Capsicum baccatum", chilliMapB.get("bac").toString());
  }
  
  @Test
  public void canConvertDTypesToAndFromObject() {
    Object o = null;
    DObject dob = null;

    // DDate
    Date now = new Date();
    DDate d = new DDate(now);
    o = d.getObject();
    dob = DObject.toDObject(o);
    assertTrue(dob instanceof DSimple);
    assertTrue(dob instanceof DAtom);
    assertTrue(dob instanceof DDate);
    assertEquals(now, ((DDate) dob).getValue());

    // DInteger
    DInteger i = new DInteger(6);
    o = i.getObject();
    dob = DObject.toDObject(o);
    assertTrue(dob instanceof DSimple);
    assertTrue(dob instanceof DAtom);
    assertTrue(dob instanceof DNumber);
    assertTrue(dob instanceof DInteger);
    assertEquals(6, ((DInteger) dob).getValue());

    // DString
    DString s = new DString("Foo");
    o = s.getObject();
    dob = DObject.toDObject(o);
    assertTrue(dob instanceof DSimple);
    assertTrue(dob instanceof DAtom);
    assertTrue(dob instanceof DString);
    assertEquals("Foo", ((DString) dob).getValue());
    
    // DList
    DList l = new DList();
    l.add(d);
    l.add(i);
    l.add(s);
    o = l.getObject();
    dob = DObject.toDObject(o);
    assertTrue(dob instanceof DSimple);
    assertTrue(dob instanceof DCollection);
    assertTrue(dob instanceof DList);
    
    DList dobl = (DList) dob;
    assertEquals(3, dobl.getList().size());
    
    assertTrue(dobl.getList().get(0) instanceof DDate);
    assertEquals(now, ((DDate) dobl.getList().get(0)).getValue());
    
    assertTrue(dobl.getList().get(1) instanceof DInteger);
    assertEquals(6, ((DInteger) dobl.getList().get(1)).getValue());
    
    assertTrue(dobl.getList().get(2) instanceof DString);
    assertEquals("Foo", ((DString) dobl.getList().get(2)).getValue());
    
    // DMap
    DMap m = new DMap();
    m.put("d", d);
    m.put("i", i);
    m.put("s", s);
    o = m.getObject();
    dob = DObject.toDObject(o);
    assertTrue(dob instanceof DSimple);
    assertTrue(dob instanceof DCollection);
    assertTrue(dob instanceof DMap);
    
    DMap dobm = (DMap) dob;
    assertEquals(3, dobm.getMap().size());
    
    assertTrue(dobm.get("d") instanceof DDate);
    assertEquals(now, ((DDate) dobm.get("d")).getValue());
    
    assertTrue(dobm.get("i") instanceof DInteger);
    assertEquals(6, ((DInteger) dobm.get("i")).getValue());
    
    assertTrue(dobm.get("s") instanceof DString);
    assertEquals("Foo", ((DString) dobm.get("s")).getValue());
    
    // DSet
    DSet set = new DSet();
    set.add(d);
    set.add(i);
    set.add(s);
    o = set.getObject();
    dob = DObject.toDObject(o);
    assertTrue(dob instanceof DSimple);
    assertTrue(dob instanceof DCollection);
    assertTrue(dob instanceof DSet);
    
    DSet dobs = (DSet) dob;
    assertEquals(3, dobs.getSet().size());
    
  }

}

/*
 * Copyright Ⓒ 2019 Fred Vos
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.mokolo.commons.data;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.UnsupportedEncodingException;

public class DocumentSchemaTest {
  
  @Test
  public void validDocumentValidates() throws ValidationException {
    DocumentSchema schema = DocumentSchema.getTestSchema();

    Document document = DocumentSchema.getTestDocument(); // Has no key 'must-have'...
    
    schema.validate(document);
    assertTrue(true);
  }
  
  @Test
  public void extraFieldValidatesInRelaxedMode() throws ValidationException {
    DocumentSchema schema = DocumentSchema.getTestSchema();
    
    Document document = DocumentSchema.getTestDocument(); // Has no key 'must-have'...
    document.addString("something", "extra");
    
    schema.validate(document);
    assertTrue(true);
  }

  @Test
  public void extraFieldDoesNotValidateInRestrictedMode() throws Exception {
    DocumentSchema schema = DocumentSchema.getTestSchema();
    schema.setRestrictedToListedKeys(true);

    Document document = DocumentSchema.getTestDocument(); // Has no key 'must-have'...
    document.addString("something", "extra");
        
    Assertions.assertThrows(ValidationException.class, () -> {
      schema.validate(document);
    });
    assertTrue(true);
  }

  @Test
  public void extraValueValidatesWhenPluralAllowed() throws ValidationException {
    DocumentSchema schema = DocumentSchema.getTestSchema();
    
    /*
     * Allow plural values for boolean:
     */
    DocumentKeyDescription description = new DocumentKeyDescription(
        "boolean",
        DocumentKeyDescription.Type.BOOLEAN);
    description.setPluralAllowed(true);
    schema.putKeyDescription(description); // Replaces setting from 'setupSchema()'

    Document document = DocumentSchema.getTestDocument(); // Has no key 'must-have'...
    document.addString("boolean", "false");
    
    schema.validate(document);
    assertTrue(true);
  }

  @Test
  public void extraValueDoesNotValidateInRestrictedMode() throws ValidationException {
    DocumentSchema schema = DocumentSchema.getTestSchema();
    DocumentKeyDescription description = new DocumentKeyDescription(
        "boolean",
        DocumentKeyDescription.Type.BOOLEAN);
    description.setPluralAllowed(false);
    schema.putKeyDescription(description); // Replaces setting from 'setupSchema()'
    
    Document document = DocumentSchema.getTestDocument(); // Has no key 'must-have'...
    document.addString("boolean", "false");

    Assertions.assertThrows(ValidationException.class, () -> {
      schema.validate(document);
    });
    assertTrue(true);
  }

  @Test
  public void missingRequiredKeyDoesNotValidate()
  throws ValidationException, UnsupportedEncodingException {
    DocumentSchema schema = DocumentSchema.getTestSchema();
    /*
     * Add required key 'must-have':
     */
    DocumentKeyDescription description = new DocumentKeyDescription(
        "must-have",
        DocumentKeyDescription.Type.FLOAT);
    description.setRequired(true);
    schema.putKeyDescription(description);
    
    Document document = DocumentSchema.getTestDocument(); // Has no key 'must-have'...
    
    Assertions.assertThrows(ValidationException.class, () -> {
      schema.validate(document);
    });
    assertTrue(true);
  }

}

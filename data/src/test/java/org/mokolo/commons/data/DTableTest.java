package org.mokolo.commons.data;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class DTableTest {
  
  private DTable strictTable;
  private DTable flexibleTable;
  
  @BeforeEach
  public void setup() throws DException {
    this.strictTable = new DTable();
    strictTable.addColumn(new DColumn("ir", null, DInteger.class, true));
    strictTable.addColumn(new DColumn("io", null, DInteger.class, false));

    this.flexibleTable = new DTable(false);
    flexibleTable.addColumn(new DColumn("ir", null, DInteger.class, true));
    flexibleTable.addColumn(new DColumn("io", null, DInteger.class, false));
  }

  @Test
  public void canSetupAndFillTable() throws Exception {
    DRow row;
    
    // Only required - no problem
    row = new DRow();
    row.put("ir", new DInteger(1));
    this.strictTable.addRow(row);
    
    // Required plus optional - no problem
    row = new DRow();
    row.put("ir", new DInteger(2));
    row.put("io", new DInteger(0));
    this.strictTable.addRow(row);
  }
  
  @Test
  public void duplicateColumnFails() {
    DException e = assertThrows(DException.class, () -> {
      this.strictTable.addColumn(new DColumn("ir", null, DInteger.class, true));
    }, "Must throw DException");
    assertTrue(e.getMessage().contains("Duplicate column name"));
  }
  
  @Test
  public void missingRequiredColumnFails() {
    DException e = assertThrows(DException.class, () -> {
      // Optional only - exception
      DRow erow = new DRow();
      erow.put("io", new DInteger(0));
      this.strictTable.addRow(erow);
    }, "Must throw DException");
    assertTrue(e.getMessage().contains("Missing required column"));
  }
  
  @Test
  public void unknownColumnFailsIfStrict() {
    DException e = assertThrows(DException.class, () -> {
      DRow erow = new DRow();
      erow.put("ir", new DInteger(2));
      erow.put("foo", new DInteger(0));
      this.strictTable.addRow(erow);
    }, "Must throw DException");
    assertTrue(e.getMessage().contains("Unknown column"));
  }
  
  @Test
  public void unknownColumnOkayIfFlexible() throws DException {
    DRow erow = new DRow();
    erow.put("ir", new DInteger(2));
    erow.put("foo", new DInteger(0));
    this.flexibleTable.addRow(erow);
  }
  
}

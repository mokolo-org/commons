package org.mokolo.commons.data;

public enum Processable {
  DLIST,
  DMAP,
  DPARAGRAPH,
  DSECTION,
  DTABLE
}

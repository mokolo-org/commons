/*
 * Copyright Ⓒ 2019 Fred Vos
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.mokolo.commons.data;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.extern.slf4j.Slf4j;

/**
 * Helper class to serialize and deserialize (parse) Document as XML
 *
 */
@Slf4j
@XmlRootElement(name = "document")
public class JAXBDocument {
  
  private static JAXBContext context;
  static {
    try {
      context = JAXBContext.newInstance(JAXBDocument.class);
    } catch (JAXBException e) {
      log.error("Cannot create context");
    }
  }
  private static Marshaller createMarshaller() throws JAXBException {
    Marshaller marshaller = context.createMarshaller();
    marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
    return marshaller;
  }
  private static Unmarshaller createUnmarshaller() throws JAXBException {
    return context.createUnmarshaller();
  }
  public static JAXBDocument parseXml(InputStream is) throws JAXBException {
    return (JAXBDocument) createUnmarshaller().unmarshal(is);
  }
  public static void serializeToXml(JAXBDocument jaxbDocument, OutputStream os) throws JAXBException {
    createMarshaller().marshal(jaxbDocument, os);
  }

  @XmlAttribute public UUID uuid;
  @XmlElementWrapper(name = "data")
  @XmlElement(name = "entry")
  public List<JAXBDocumentEntry> data;
  
  public JAXBDocument() {
    this.data = new ArrayList<>();
  }
  
  public static Document parseJAXBDocument(JAXBDocument jaxbDocument) {
    Document document = new Document();
    document.setUuid(jaxbDocument.uuid);
    for (JAXBDocumentEntry entry : jaxbDocument.data) {
      for (String value : entry.values)
        document.addString(entry.key, value);
    }
    return document;
  }

  public static JAXBDocument toJAXBDocument(Document document) {
    JAXBDocument jaxbDocument = new JAXBDocument();
    jaxbDocument.uuid = document.getUuid();
    for (Map.Entry<String, ArrayList<String>> entry : document.entrySet()) {
      JAXBDocumentEntry listEntry = new JAXBDocumentEntry();
      listEntry.key = entry.getKey();
      listEntry.values = entry.getValue();
      jaxbDocument.data.add(listEntry);
    }
    return jaxbDocument;
  }
}

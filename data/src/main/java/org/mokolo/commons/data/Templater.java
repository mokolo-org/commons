package org.mokolo.commons.data;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import freemarker.cache.ClassTemplateLoader;
import freemarker.template.DefaultObjectWrapperBuilder;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.Version;

/**
 * 
 * To keep ${b} in result, use ${r"${b}"} in template.
 * See https://stackoverflow.com/questions/43535432/can-freemarker-keep-the-original-value-if-variable-is-not-found
 *
 */
public class Templater {

  private static freemarker.template.Configuration configuration;
  private static Version FREEMARKER_CONFIGURATION_VERSION = freemarker.template.Configuration.VERSION_2_3_23;
  
  static {
    try {
      configuration = new freemarker.template.Configuration(FREEMARKER_CONFIGURATION_VERSION);
//    FileTemplateLoader fileTemplateLoader = new FileTemplateLoader(new File("src/main/resources/templates"));
      ClassTemplateLoader classTemplateLoader = new ClassTemplateLoader(Templater.class, "/templates");
//    MultiTemplateLoader multiTemplateLoader = new MultiTemplateLoader(new TemplateLoader[] { fileTemplateLoader, classTemplateLoader });
//    configuration.setTemplateLoader(multiTemplateLoader);
      configuration.setTemplateLoader(classTemplateLoader);
      configuration.setDefaultEncoding("UTF-8");
      configuration.setObjectWrapper(new DefaultObjectWrapperBuilder(FREEMARKER_CONFIGURATION_VERSION).build());
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /*
  private static Template getFreemarkerTemplate(String filename)
  throws IOException, TemplateException {
    return configuration.getTemplate(filename);
  }
  
  public static String applyTemplateFile(String filename, Map<String, Object> model)
  throws IOException, TemplateException {
    Template template = getFreemarkerTemplate(filename);
    StringWriter text = new StringWriter();
    template.process(model, text);
    return text.toString();
  }
  */
  
  public static String applyTemplateFile(File file, Map<String, Object> model)
  throws IOException, TemplateException {
    String templateString = FileUtils.readFileToString(file, Charset.forName("UTF-8"));
    return applyTemplateString(templateString, model);
  }

  public static String applyTemplateResource(String path, Map<String, Object> model)
  throws IOException, TemplateException {
    String templateString = IOUtils.resourceToString(path, Charset.forName("UTF-8"));
    return applyTemplateString(templateString, model);
  }

  public static String applyTemplateString(
      String templateString,
      Map<String, Object> model)
  throws IOException, TemplateException {
    Template template = new Template(
        UUID.randomUUID().toString(),
        new StringReader(templateString),
        configuration);
    StringWriter text = new StringWriter();
    template.process(model, text);
    return text.toString();
  }

}

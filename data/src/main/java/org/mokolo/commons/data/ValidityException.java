package org.mokolo.commons.data;

public class ValidityException extends Exception {
  private static final long serialVersionUID = 269552297594283688L;
  
  public ValidityException(String msg) {
    super(msg);
  }

}

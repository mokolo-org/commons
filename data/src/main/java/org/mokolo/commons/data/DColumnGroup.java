package org.mokolo.commons.data;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;

/**
 * Group of columns under one header
 *
 */
@XmlRootElement
public class DColumnGroup {
  @Getter @XmlElement private int startColumn; // 0..n-1
  @Getter @XmlElement private int columnCount; // >= 1
  @Getter @XmlElement private String caption;
  
  public DColumnGroup() { }
  
  public DColumnGroup(int startColumn, int columnCount, String caption) {
    this();
    this.startColumn = startColumn;
    this.columnCount = columnCount;
    this.caption = caption;
  }
}

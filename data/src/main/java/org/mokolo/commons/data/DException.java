package org.mokolo.commons.data;

public class DException extends Exception {
  private static final long serialVersionUID = 5141159718258825105L;

  public DException(String msg) {
    super(msg);
  }
}

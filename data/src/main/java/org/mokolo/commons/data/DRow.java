package org.mokolo.commons.data;

import java.util.HashMap;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class DRow extends HashMap<String, DAtom> {
  private static final long serialVersionUID = -1706262153655453655L;
}

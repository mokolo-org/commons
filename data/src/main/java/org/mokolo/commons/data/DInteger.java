package org.mokolo.commons.data;

import java.io.InputStream;

import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Setter;

@XmlRootElement
public class DInteger extends DNumber {
  @Setter Integer value;

  /* For tests */
  public static DInteger parseXml(InputStream is) throws JAXBException {
    return (DInteger) createUnmarshaller().unmarshal(is);
  }

  public DInteger() {
    super();
  }
  
  public DInteger(Integer value) {
    this();
    this.value = value;
  }
  
  public DInteger(Object o) {
    this();
    this.value = (Integer) o;
  }

  @XmlElement
  public Integer getValue() {
    return this.value;
  }
  
  @Override
  public String toString() {
    return this.value.toString();
  }
  
  @Override
  public String toString(String format) {
    return String.format(format, this.value);
  }

  @Override
  public Object getObject() {
    return this.value;
  }
}

/*
 * Copyright Ⓒ 2019 Fred Vos
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.mokolo.commons.data;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@XmlRootElement(name="document-schema")
public class JAXBDocumentSchema {
  
  private static JAXBContext context;
  static {
    try {
      context = JAXBContext.newInstance(JAXBDocumentSchema.class);
    } catch (JAXBException e) {
      log.error("Cannot create context");
    }
  }
  private static Marshaller createMarshaller() throws JAXBException {
    Marshaller marshaller = context.createMarshaller();
    marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
    return marshaller;
  }
  private static Unmarshaller createUnmarshaller() throws JAXBException {
    return context.createUnmarshaller();
  }
  public static JAXBDocumentSchema parseXml(InputStream is) throws JAXBException {
    return (JAXBDocumentSchema) createUnmarshaller().unmarshal(is);
  }
  public static void serializeToXml(JAXBDocumentSchema jaxbDocumentSchema, OutputStream os) throws JAXBException {
    createMarshaller().marshal(jaxbDocumentSchema, os);
  }

  @XmlElementWrapper(name = "descriptions")
  @XmlElement(name = "description")
  public List<JAXBDocumentKeyDescription> keyDescriptionsList;
  @XmlElementWrapper(name = "order")
  @XmlElement(name = "key")
  public List<String> preferredKeyOrderForPresentation;
  @XmlAttribute(name = "restricted-to-listed-keys") public boolean restrictedToListedKeys;
  
  public JAXBDocumentSchema() {
    this.keyDescriptionsList = new ArrayList<JAXBDocumentKeyDescription>();
    this.preferredKeyOrderForPresentation = new ArrayList<String>();
    this.restrictedToListedKeys = false;
  }
  
  public static DocumentSchema parseJAXBDocumentSchema(JAXBDocumentSchema jaxbDocumentSchema) {
    DocumentSchema documentSchema = new DocumentSchema();
    for (JAXBDocumentKeyDescription jkd : jaxbDocumentSchema.keyDescriptionsList)
      documentSchema.putKeyDescription(jkd.toDocumentKeyDescription());
    for (String key : jaxbDocumentSchema.preferredKeyOrderForPresentation)
      documentSchema.addPreferredKeyOrderForPresentation(key);
    documentSchema.setRestrictedToListedKeys(jaxbDocumentSchema.restrictedToListedKeys);
    return documentSchema;
  }

  public static JAXBDocumentSchema toJAXBDocumentSchema(DocumentSchema documentSchema) {
    JAXBDocumentSchema jaxbDocumentSchema = new JAXBDocumentSchema();
    for (DocumentKeyDescription kd : documentSchema.getKeyDescriptions())
      jaxbDocumentSchema.keyDescriptionsList.add(new JAXBDocumentKeyDescription(kd));
    for (String key : documentSchema.getPreferredKeyOrderForPresentation())
      jaxbDocumentSchema.preferredKeyOrderForPresentation.add(key);
    jaxbDocumentSchema.restrictedToListedKeys = documentSchema.isRestrictedToListedKeys();
    return jaxbDocumentSchema;
  }
}

/*
 * Copyright Ⓒ 2019 Fred Vos
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.mokolo.commons.data;

import javax.xml.bind.annotation.XmlAttribute;

import org.mokolo.commons.data.DocumentKeyDescription.Type;

public class JAXBDocumentKeyDescription {
  @XmlAttribute public String key;
  @XmlAttribute public Type type;
  @XmlAttribute public String purpose; // Default: null
  @XmlAttribute public boolean required; // Default: false
  @XmlAttribute public boolean pluralAllowed; // Default: false
  
  public JAXBDocumentKeyDescription() { }
  
  public JAXBDocumentKeyDescription(
      String key,
      Type type) {
    this();
    this.key = key;
    this.type = type;
    this.required = false;
    this.pluralAllowed = true;
  }
  
  public JAXBDocumentKeyDescription(DocumentKeyDescription kd) {
    this();
    this.key = kd.getKey();
    this.type = kd.getType();
    this.required = kd.isRequired();
    this.pluralAllowed = kd.isPluralAllowed();
  }
  
  public DocumentKeyDescription toDocumentKeyDescription() {
    DocumentKeyDescription kd = new DocumentKeyDescription();
    kd.setKey(this.key);
    kd.setType(this.type);
    kd.setRequired(this.required);
    kd.setPluralAllowed(this.pluralAllowed);
    return kd;
  }
}

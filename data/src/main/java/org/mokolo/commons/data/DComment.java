package org.mokolo.commons.data;

public class DComment extends DString {
  
  public DComment() {
    super();
  }
  
  public DComment(String text) {
    super(text);
  }
}

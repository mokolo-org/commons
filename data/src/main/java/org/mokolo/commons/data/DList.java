package org.mokolo.commons.data;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Setter;

@XmlRootElement
public class DList extends DCollection {
  @Setter private ArrayList<DSimple> list;
  
  public static DList parseXml(InputStream is) throws JAXBException {
    return (DList) createUnmarshaller().unmarshal(is);
  }

  public DList() {
    super();
    this.list = new ArrayList<>();
  }
  
  public DList(Object object) {
    this();
    @SuppressWarnings("unchecked")
    List<Object> l = (List<Object>) object;
    for (Object o : l)
      this.add((DSimple) DObject.toDObject(o));
  }

  @XmlElementWrapper(name="list")
  @XmlElement(name="item")
  public ArrayList<DSimple> getList() {
    return this.list;
  }
  
  public void add(DSimple value) {
    this.list.add(value);
  }
  
  public boolean contains(DObject o) {
    return this.list.contains(o);
  }

  @Override
  public Object getObject() {
    List<Object> object = new ArrayList<>();
    for (DSimple item : this.list) {
      object.add(item.getObject());
    }
    return object;
  }
}

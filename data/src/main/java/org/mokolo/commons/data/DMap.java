package org.mokolo.commons.data;

import java.io.ByteArrayOutputStream;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.dataformat.yaml.YAMLGenerator.Feature;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * A map of DAtom to DObject entries
 * 
 * Do not use "type" and "class" keys, for these are added during serialization to Hugo pages.
 *
 */
@XmlRootElement
@Slf4j
public class DMap extends DCollection {
  @Setter private LinkedHashMap<DAtom, DSimple> map;
  
  private static ObjectMapper mapper;
  
  // https://www.baeldung.com/jackson-yaml
  static {
    mapper = new ObjectMapper(new YAMLFactory());
    mapper.findAndRegisterModules();
    mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    mapper = new ObjectMapper(new YAMLFactory().disable(Feature.WRITE_DOC_START_MARKER));
  }
  
  public void checkValidity() throws ValidityException { }

  public static DMap parseXml(InputStream is) throws JAXBException {
    return (DMap) createUnmarshaller().unmarshal(is);
  }

  public DMap() {
    super();
    this.map = new LinkedHashMap<>();
  }
  
  public DMap(DMap map) {
    this();
    this.map.putAll(map.map);
  }
  
  public DMap(Object object) {
    this();
    @SuppressWarnings("unchecked")
    Map<String, Object> map = (Map<String, Object>) object;
    for (Map.Entry<String, Object> entry : map.entrySet())
      this.put(entry.getKey(), (DSimple) DObject.toDObject(entry.getValue()));
  }

  @XmlElement public LinkedHashMap<DAtom, DSimple> getMap() {
    return this.map;
  }

  public DSimple get(DAtom key) {
    return this.map.get(key);
  }
  
  public DSimple get(String key) {
    return this.map.get(new DString(key));
  }
  
  public Date getDate(String key) {
    DDate value = (DDate) this.get(key);
    if (value != null)
      return value.getValue();
    return null;
  }

  public String getString(String key) {
    DString value = (DString) this.get(key);
    if (value != null)
      return value.getValue();
    return null;
  }

  public List<String> getStrings(String key) {
    DList value = (DList) this.get(key);
    if (value != null) {
      List<String> list = new ArrayList<>();
      for (DObject item : value.getList())
        list.add(item.toString());
      return list;
    }
    return null;
  }

  public void put(DAtom key, DSimple value) {
    this.map.put(key, value);
  }
  
  public void put(String key, DSimple value) {
    this.put(new DString(key), value);
  }
  
  public void putDate(String key, Date value) {
    this.put(key, new DDate(value));
  }
  
  public void putString(String key, String value) {
    this.put(key, new DString(value));
  }
  
  public void putIfNotNull(String key, DSimple value) {
    if (value != null)
      this.put(key, value);
  }
  
  public boolean containsKey(String name) {
    boolean found = false;
    for (DObject o : this.map.keySet())
      if (o.toString().equals(name)) {
        found = true;
        break;
      }
    return found;
  }
  
  /**
   * Can be used in FreeMarker as a model
   */
  @Override
  public Object getObject() {
    return this.toModel();
  }
  
  public Map<String, Object> toModel() {
    Map<String, Object> soMap = new LinkedHashMap<>();
    for (Map.Entry<DAtom, DSimple> entry : this.map.entrySet()) {
      soMap.put(entry.getKey().toString(), entry.getValue().getObject());
    }
    return soMap;
  }
  
  public String toYAML() {
    String yaml = null;
    try {
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      mapper.writeValue(baos, this.getObject());
      yaml = baos.toString(Charset.forName("UTF-8"));
    } catch (IOException e) {
      log.error(e.getClass().getName()+" occurred, Msg = "+e.getMessage());
    }
    return yaml;
  }
  
  @Override
  public String toString() {
    List<String> kvs = new ArrayList<>();
    for (Map.Entry<DAtom, DSimple> entry : this.map.entrySet()) {
      StringBuffer buf = new StringBuffer();
      buf.append(entry.getKey().toString());
      buf.append(": ");
      if (entry.getValue() != null)
        buf.append(entry.getValue().toString());
      kvs.add(buf.toString());
    }
    return String.join(", ", kvs);
  }
  

}

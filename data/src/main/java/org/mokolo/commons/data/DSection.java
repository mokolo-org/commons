package org.mokolo.commons.data;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;

import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@XmlRootElement
@Slf4j
public class DSection extends DComplex {
  @Getter @XmlElement private DString title;
  @Getter @Setter @XmlElement private ArrayList<DObject> list;
  
  public DSection() {
    this.list = new ArrayList<>();
  }
  
  public void setTitle(DString title) {
    this.title = title;
  }
  
  public void setTitle(String title) {
    this.setTitle(new DString(title));
  }
  
  public void add(DObject value) throws DException {
    if (isProcessable(value))
      this.list.add(value);
    else
      throw new DException(
          "Cannot add non-processable type "+
          value.getClass().getCanonicalName()+
          " to section");
  }
  
  private static boolean isProcessable(DObject o) {
    String classnameUC = o.getClass().getSimpleName().toUpperCase();
    log.debug("Classname new object = "+classnameUC);
    Processable proc = Processable.valueOf(classnameUC);
    return (proc != null);
  }

  @Override
  public Object getObject() {
    // TODO Auto-generated method stub
    return null;
  }
}

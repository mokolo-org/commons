package org.mokolo.commons.data;

import java.io.InputStream;

import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Setter;

@XmlRootElement
public class DString extends DAtom {
  @Setter private String value;
  
  /* For tests */
  public static DString parseXml(InputStream is) throws JAXBException {
    return (DString) createUnmarshaller().unmarshal(is);
  }

  public DString() {
    super();
  }
  
  public DString(String value) {
    this();
    this.value = value;
  }
  
  public DString(Object o) {
    this();
    this.value = (String) o;
  }
  
  @XmlElement
  public String getValue() {
    return this.value;
  }
  
  @Override
  public String toString() {
    return this.value;
  }
  
  @Override
  public String toString(String format) {
    return String.format(format, this.value);
  }
  
  public int length() {
    return this.value.length();
  }

  @Override
  public Object getObject() {
    return this.value;
  }

}

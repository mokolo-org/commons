package org.mokolo.commons.data;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;

@XmlRootElement
public class DTable extends DComplex {
  
  @Getter @Setter private boolean strict;
  @Getter private LinkedHashMap<String, DColumn> columns;
  @Getter private TreeMap<Integer, DColumnGroup> columnGroups;
  @Getter private ArrayList<DRow> rows;
  
  public DTable() {
    super();
    this.strict = true;
    this.columns = new LinkedHashMap<>();
    this.columnGroups = new TreeMap<>();
    this.rows = new ArrayList<>();
  }
  
  public DTable(boolean strict) {
    this();
    this.strict = strict;
  }

  public void addColumn(DColumn column) throws DException {
    if (this.columns.containsKey(column.getName()))
      throw new DException("Duplicate column name");
    this.columns.put(column.getName(), column);
  }
  
  public void addColumnGroup(DColumnGroup columnGroup) throws DException {
    if (columnGroup.getColumnCount() < 1)
      throw new DException("Column count in column group with caption '"+columnGroup.getCaption()+"' must at least span one column");
    if (columnGroup.getStartColumn() < 0 ||
        columnGroup.getStartColumn() + columnGroup.getColumnCount() > this.columns.size())
      throw new DException("Column range in column group with caption '"+columnGroup.getCaption()+"' out of bounds");
    this.columnGroups.put(columnGroup.getStartColumn(), columnGroup);
    int last = -1;
    for (DColumnGroup cg : this.columnGroups.values()) {
      if (cg.getStartColumn() <= last)
        throw new DException(
            "Column range in column group with caption '"+
             columnGroup.getCaption()+
             "' overlaps with previous group");
      last = cg.getStartColumn() + cg.getColumnCount() - 1;
    }
  }
  
  public void addRow(DRow row) throws DException {
    // Each field:
    for (Map.Entry<String, DAtom> entry : row.entrySet()) {
      if (! this.columns.containsKey(entry.getKey())) {
        if (this.strict == true)
          throw new DException("Unknown column named '"+entry.getKey()+"'");
        this.addColumn(new DColumn(entry.getKey(), null, null, false));
      }
      // Update maxWidth for row:
      DColumn column = this.columns.get(entry.getKey()); 
      column.updateMaxWidth(column.toString(entry.getValue()).length());
    }
    // Check if row complete:
    for (DColumn column : this.columns.values()) {
      if (column.isRequired() && ! row.containsKey(column.getName()))
        throw new DException("Missing required column named '"+column.getName()+"'");
    }
    this.rows.add(row);
  }
  
  /**
   * Start position of column
   */
  public int getStartPosColumn(int i) {
    int counter = 0;
    int pos = 0;
    for (DColumn col : this.columns.values()) {
      if (counter == i)
        return pos;
      else
        pos += col.getMaxWidth() + 1;
      counter++;
    }
    return -1;
  }
  
  /**
   * End position of column; 1 past last
   */
  public int getEndPosColumn(int i) {
    int counter = 0;
    int pos = 0;
    for (DColumn col : this.columns.values()) {
      if (counter == i)
        return pos + col.getMaxWidth();
      else
        pos += col.getMaxWidth() + 1;
      counter++;
    }
    return -1;
  }

  @Override
  public Object getObject() {
    // TODO Auto-generated method stub
    return null;
  }
  
  
}

package org.mokolo.commons.data;

import java.net.URL;
import java.util.regex.Pattern;

/**
 * DAtom and DCollection
 *
 */
public abstract class DSimple extends DObject {

  /*
   * group(1) = URL
   * group(2) = link text
   */
  public static Pattern linkPattern = Pattern.compile("<a href=\"(.*?)\">(.*?)</a>");

  public static String createLink(URL url, String linkText) {
    return createLink(url.toExternalForm(), linkText);
  }
  
  public static String createLink(String url, String linkText) {
    return String.format("<a href=\"%s\">%s</a>", url, linkText);
  }
  
  /**
   * Replaces <a href="x">y</a> by y and computes length
   */
  public static int displayLenght(String s) {
    return linkPattern.matcher(s).replaceAll("$2").length();
  }
  
}

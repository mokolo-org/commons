/*
 * Copyright Ⓒ 2019 Fred Vos
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.mokolo.commons.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Getter;

public class DocumentSchema {
  private Map<String, DocumentKeyDescription> keyDescriptionsMap;
  @Getter private List<String> preferredKeyOrderForPresentation;
  @Getter private boolean restrictedToListedKeys;
  
  /**
   * Sets up the schema.
   * 
   * Initially the most relaxed constraints, so it's not restricted
   * to listed keys.
   */
  public DocumentSchema() {
    this.keyDescriptionsMap = new HashMap<String, DocumentKeyDescription>();
    this.preferredKeyOrderForPresentation = new ArrayList<String>();
    this.restrictedToListedKeys = false;
  }

  public List<DocumentKeyDescription> getKeyDescriptions() {
    List<DocumentKeyDescription> list = new ArrayList<>();
    for (String key : this.getPreferredKeyOrderForPresentationSchema())
      list.add(this.keyDescriptionsMap.get(key));
    return list;
  }
  
  public void setKeyDescriptions(List<DocumentKeyDescription> list) {
    this.keyDescriptionsMap.clear();
    for (DocumentKeyDescription description : list) {
      this.putKeyDescription(description);
    }
  }

  public void putKeyDescription(DocumentKeyDescription description) {
    this.keyDescriptionsMap.put(description.getKey(), description);
  }
  
  public void setPreferredKeyOrderForPresentation(List<String> list) {
    this.preferredKeyOrderForPresentation = list;
  }
  
  public void addPreferredKeyOrderForPresentation(String key) {
    this.preferredKeyOrderForPresentation.add(key);
  }
  
  public void setRestrictedToListedKeys(boolean b) {
    this.restrictedToListedKeys = b;
  }
  
  /**
   * Returns keys in the schema, sorted by preferredKeyOrderForPresentation.
   * 
   * Keys not present in preferredKeyOrderForPresentation are listed after
   * the keys that *are* present in that list, in the order as they appear
   * in the map.
   */
  public List<String> getPreferredKeyOrderForPresentationSchema() {
    ArrayList<String> list = new ArrayList<String>();
    for (String key : this.preferredKeyOrderForPresentation)
      if (this.keyDescriptionsMap.containsKey(key))
        list.add(key);
    for (String key : this.keyDescriptionsMap.keySet()) {
      if (! this.preferredKeyOrderForPresentation.contains(key))
        list.add(key);
    }
    return list;
  }
  
  /**
   * Returns keys in document, sorted by preferredKeyOrderForPresentation.
   * 
   * Keys not present in preferredKeyOrderForPresentation are listed after
   * the keys that *are* present in that list, in the order as they appear
   * in the map.
   * 
   * A document can have more keys than the schema.
   */
  public List<String> getPreferredKeyOrderForPresentationDocument(Document document) {
    ArrayList<String> list = new ArrayList<String>();
    for (String key : this.preferredKeyOrderForPresentation)
      if (document.containsKey(key))
        list.add(key);
    for (String key : document.keySet()) {
      if (! this.preferredKeyOrderForPresentation.contains(key))
        list.add(key);
    }
    return list;
  }
  
  public void validate(Document document) throws ValidationException {
    /*
     * For each key/value in the document, check against schema: 
     */
    for (Map.Entry<String, ArrayList<String>> entry : document.entrySet()) {
      DocumentKeyDescription description = this.keyDescriptionsMap.get(entry.getKey());
      if (description == null && this.restrictedToListedKeys == true)
        throw new ValidationException("Key '"+entry.getKey()+"' not allowed");
      if (description != null)
        description.validate(entry.getValue());
    }
    
    /*
     * All required keys from schema present in document?
     */
    for (Map.Entry<String, DocumentKeyDescription> entry : this.keyDescriptionsMap.entrySet()) {
      DocumentKeyDescription description = entry.getValue();
      if (description.isRequired() && ! document.containsKey(entry.getKey()))
        throw new ValidationException("Required key '"+entry.getKey()+"' missing in document");
    }
  }
  
  public static Document getTestDocument() {
    Document document = new Document();
    document.addString("boolean", "true");
    document.addString("date", "2012-01-19");
    document.addString("file", "/etc/app/app.conf");
    document.addString("float", "36.672");
    document.addString("integer", "-123");
    document.addString("string", "bla");
    document.addString("timestamp", "2012-01-19T14:39:00+0000");
    document.addString("url", "https://app.org/search?query=test");
    document.addString("uuid", "2c4dae8c-e591-49e0-9c5a-62b310a15788");
    return document;
  }
  
  public static DocumentSchema getTestSchema() {
    DocumentSchema schema = new DocumentSchema();
    
    DocumentKeyDescription description;
    
    description = new DocumentKeyDescription(
        "boolean",
        DocumentKeyDescription.Type.BOOLEAN);
    schema.putKeyDescription(description);
    
    description = new DocumentKeyDescription(
        "date",
        DocumentKeyDescription.Type.DATE);
    schema.putKeyDescription(description);
    
    description = new DocumentKeyDescription(
        "file",
        DocumentKeyDescription.Type.FILE);
    schema.putKeyDescription(description);
    
    description = new DocumentKeyDescription(
        "float",
        DocumentKeyDescription.Type.FLOAT);
    schema.putKeyDescription(description);
    
    description = new DocumentKeyDescription(
        "integer",
        DocumentKeyDescription.Type.INTEGER);
    schema.putKeyDescription(description);
    
    description = new DocumentKeyDescription(
        "string",
        DocumentKeyDescription.Type.STRING);
    schema.putKeyDescription(description);
    
    description = new DocumentKeyDescription(
        "timestamp",
        DocumentKeyDescription.Type.TIMESTAMP);
    schema.putKeyDescription(description);
    
    description = new DocumentKeyDescription(
        "url",
        DocumentKeyDescription.Type.URL);
    schema.putKeyDescription(description);
    
    description = new DocumentKeyDescription(
        "uuid",
        DocumentKeyDescription.Type.UUID);
    schema.putKeyDescription(description);
    
    // All but uuid and timestamp in reversed order:
    schema.addPreferredKeyOrderForPresentation("string");
    schema.addPreferredKeyOrderForPresentation("integer");
    schema.addPreferredKeyOrderForPresentation("file");
    schema.addPreferredKeyOrderForPresentation("float");
    schema.addPreferredKeyOrderForPresentation("url");
    schema.addPreferredKeyOrderForPresentation("uuid");
    schema.addPreferredKeyOrderForPresentation("date");
    schema.addPreferredKeyOrderForPresentation("boolean");
    
    return schema;
  }
}

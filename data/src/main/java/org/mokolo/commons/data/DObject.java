package org.mokolo.commons.data;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.extern.slf4j.Slf4j;

/**
 * Base class for all D* classes
 * 
 * DObject
 * |
 * +- DSimple
 * |  |
 * |  +- DAtom
 * |  |  |
 * |  |  +- DDate
 * |  |  +- DNumber
 * |  |  |  |
 * |  |  |  +- DInteger
 * |  |  +- DString
 * |  +- DCollection
 * |     |
 * |     +- DList
 * |     +- DMap
 * |     +- DSet
 * +- DComplex
 *    |
 *    +- DTable
 *    +- ...
 *    
 */
@XmlRootElement
@Slf4j
public abstract class DObject {

  private static JAXBContext context;
  static {
    try {
      context = JAXBContext.newInstance(
          DInteger.class,
          DDate.class,
          DList.class,
          DMap.class,
          DSet.class,
          DString.class
          );
/*
          DColumn.class,
          DColumnGroup.class,
          DParagraph.class,
          DRow.class,
          DSection.class,
          DTable.class
*/
      /* All derivates of DObject */
    } catch (JAXBException e) {
      log.error("Cannot create context");
    }
  }
  public static Marshaller createMarshaller() throws JAXBException {
    Marshaller marshaller = context.createMarshaller();
    marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
    return marshaller;
  }
  public static void serializeToXml(DObject object, OutputStream os) throws JAXBException {
    createMarshaller().marshal(object, os);
  }
  public static String toXml(DObject object) throws JAXBException {
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    serializeToXml(object, baos);
    return baos.toString(Charset.forName("UTF-8"));
  }
  public static Unmarshaller createUnmarshaller() throws JAXBException {
    return context.createUnmarshaller();
  }

  public abstract Object getObject();
  
  /**
   * Transformation from Object to DObject
   */
  public static DObject toDObject(Object object) {
    /*
     * DAtom types
     */
    if (object instanceof Date)
      return new DDate(object);
    else if (object instanceof Integer)
      return new DInteger(object);
    else if (object instanceof String)
      return new DString(object);

    /*
     * DCollection types
     */
    else if (object instanceof List)
      return new DList(object);
    else if (object instanceof Map)
      return new DMap(object);
    else if (object instanceof Set)
      return new DSet(object);

    return null;
  }



}

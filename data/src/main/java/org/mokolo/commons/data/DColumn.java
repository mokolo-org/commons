package org.mokolo.commons.data;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;

@XmlRootElement
public class DColumn extends DComplex {
  @Getter @Setter @XmlElement private String name;
  @XmlElement private String caption;
  @Getter @Setter @XmlElement private Class<?> clazz;
  @Getter @Setter @XmlElement private boolean required;
  @Getter @XmlElement private int maxWidth;
  @Getter @Setter @XmlElement private String format; // You can use '${maxWidth}' for width
  
  public DColumn() { }
  
  public DColumn(String name, String caption, Class<?> clazz, boolean required)
  throws DException, DException {
    this();
    if (clazz != null && ! DAtom.class.isAssignableFrom(clazz))
      throw new DException("clazz ("+clazz.getCanonicalName()+") must derive from DAtom");
    if (name == null || name.isEmpty())
      throw new DException("Name of column must contain text");
    
    this.name = name;
    this.caption = caption;
    this.updateMaxWidth(this.getCaption().length());
    this.clazz = clazz;
    this.required = required;
  }
  
  public String getCaption() {
    return (this.caption != null) ? this.caption : this.name;
  }
  
  public void setCaption(String caption) {
    this.caption = caption;
    this.updateMaxWidth(this.caption.length());
    // Setting a shorter caption than the previous one can lead to unnecessary
    // whitespace in generated tables in text
  }
  
  public void updateMaxWidth(int newWidth) {
    if (newWidth > this.maxWidth)
      this.maxWidth = newWidth;
  }
  
  public String toString(DAtom cell) {
    if (this.format != null)
      return cell.toString(this.format);
    else
      return cell.toString();
  }

  @Override
  public Object getObject() {
    // TODO Auto-generated method stub
    return null;
  }
  
}

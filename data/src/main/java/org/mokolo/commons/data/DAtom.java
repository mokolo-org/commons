package org.mokolo.commons.data;

import java.util.Objects;

/**
 * Simple values
 * 
 * Since these are used as keys in DMap, make sure that every descendant
 * overrides the toString() method.
 *
 */
public abstract class DAtom extends DSimple {
  public abstract String toString(String format);
  
  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (o == null || this.getClass() != o.getClass())
      return false;
    return this.toString().equals(o.toString());
  }
  
  @Override
  public int hashCode() {
    return Objects.hash(this.toString());
  }

}

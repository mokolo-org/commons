/*
 * Copyright Ⓒ 2019 Tilburg University
 * Copyright Ⓒ 2019 Fred Vos
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.mokolo.commons.ws.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.mokolo.commons.ws.QueryParameters;

public class QueryParametersTest {

  @Test
  public void canProduceQueryParametersStringAndList() {
    QueryParameters params = new QueryParameters();
    
    params.add("size", "big");
    params.add("color", "black");
    
    assertEquals("color=black&size=big", params.toString());
    assertEquals("- color: black\n- size: big\n", params.asList());
  }

  @Test
  public void canProduceQueryParametersStringAndListForRepeatedParamsToo() {
    QueryParameters params = new QueryParameters();
    
    params.add("size", "20");
    params.add("id", "5");
    params.add("id", "1");
    
    assertEquals("id=5&id=1&size=20", params.toString());
    assertEquals("- id:\n  - 5\n  - 1\n- size: 20\n", params.asList());
  }

}

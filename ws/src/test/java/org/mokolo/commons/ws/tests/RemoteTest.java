/*
 * Copyright Ⓒ 2019 Tilburg University
 * Copyright Ⓒ 2019 Fred Vos
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.mokolo.commons.ws.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringWriter;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;

import lombok.Cleanup;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mokolo.commons.io.HttpMethod;
import org.mokolo.commons.ws.CachingRemote;
import org.mokolo.commons.ws.DirectoryRemote;
import org.mokolo.commons.ws.HttpRemote;
import org.mokolo.commons.ws.Remote;
import org.mokolo.commons.ws.Request;
import org.mokolo.commons.ws.Response;
import org.mokolo.commons.ws.Scheme;

@Slf4j
public class RemoteTest {
  
  @Test
  public void constructsCorrectURLForRequestWithNullPath() throws Exception {
    HttpRemote httpRemote = createHttpRemote();
    Request request = Request.buildXmlGetRequest();
    request.addQueryParameter("size", "5");
    // Not a real world URL
    assertEquals("https://test.pure.uvt.nl/ws/api/514?size=5", httpRemote.getUrl(request).toExternalForm(), "URL no path correct");
  }
  
  @Test
  public void constructsCorrectURLForRequestWithPath() throws Exception {
    HttpRemote httpRemote = createHttpRemote();
    Request request = Request.buildXmlGetRequest("persons");
    request.addQueryParameter("size", "5");
    // Not a real world URL
    assertEquals("https://test.pure.uvt.nl/ws/api/514/persons?size=5", httpRemote.getUrl(request).toExternalForm(), "URL with path correct");
  }
  
  @Test
  public void constructsCorrectBaseURLs() throws Exception {
    assertEquals("http://localhost:9090/pf",
        new HttpRemote("PF", Scheme.HTTP, "localhost", Integer.valueOf(9090), "pf").getBaseUrl().toExternalForm(), "With port and path");
    assertEquals("https://pf.uvt.nl",
        new HttpRemote("PF", Scheme.HTTPS, "pf.uvt.nl", null).getBaseUrl().toExternalForm(), "No port, no path");
  }
  
  @Test
  @Disabled // Uses live system that is only accessible from development PC at work 
  public void canGetPersonFromPureWithHttpGetRequest() throws Exception {
    testRemote(createHttpRemote());
  }
  
  @Test
  @Disabled // Uses live system that is only accessible from development PC at work 
  public void canGetPersonFromPureWithCachingGetRequest() throws Exception {
    File tempDir = File.createTempFile("commons-io", null);
    tempDir.delete();
    tempDir.mkdir();
    log.debug("Temporary directory: "+tempDir.getAbsolutePath());

    /*
     * Must fail because file does not yet exist:
     */
    Remote directoryRemote = new DirectoryRemote(tempDir);
    Assertions.assertThrows(FileNotFoundException.class, () -> {
      testRemote(directoryRemote);
    });

    /*
     * Now try again, but fetch from HttpRemote if not found, and store:
     */
    testRemote(new CachingRemote(tempDir, createHttpRemote()));
    
    /*
     * Now the previously created directoryRemote must succeed:
     */
    testRemote(directoryRemote);
  }
  
  @Test
  public void canWriteAndReadRequestsAndResponsesToDirectoryRemote() throws Exception {
    File tempDir = File.createTempFile("commons-io", null);
    tempDir.delete();
    tempDir.mkdir();
    log.debug("Temp dir for testing DirectoryRemote = "+tempDir.getAbsolutePath());
    DirectoryRemote remote = new DirectoryRemote(tempDir);
    
    Request request = new Request();
    request.setMethod(HttpMethod.GET);
    request.setPath("cars");
    request.addQueryParameter("color", "red");
    request.setAccept(MediaType.TEXT_PLAIN_TYPE);
    remote.writeToFile(request);
    
    String md5Hash = request.getMd5Hash();
    
    Response response = new Response(Status.OK, MediaType.TEXT_PLAIN_TYPE, new ByteArrayInputStream("Daf".getBytes()));
    assertFalse(remote.hasFiles(md5Hash), "Only one of three files present");
    remote.writeToFiles(response, md5Hash, true);
    assertTrue(remote.hasFiles(md5Hash), "All three files present");
    
    Response retrievedResponse = remote.readFromFiles(md5Hash);
    assertEquals(Status.OK, retrievedResponse.getStatus(), "Status OK");
    assertEquals(MediaType.TEXT_PLAIN_TYPE, retrievedResponse.getContentType());
    StringWriter writer = new StringWriter();
    IOUtils.copy(retrievedResponse.getInputStream(), writer, "UTF-8");
    String content = writer.toString();
    assertEquals("Daf", content, "Content matches");
  }
  
  private static HttpRemote createHttpRemote() {
    return new HttpRemote("PURE", Scheme.HTTPS, "test.pure.uvt.nl", "ws/api/514");
  }

  private static void testRemote(Remote remote) throws IOException {
    Request request = getTestRequest();
    @Cleanup Response response = remote.call(request);
    String responseString = IOUtils.toString(response.getInputStream(), "UTF-8");
    assertTrue(responseString.contains("Peter Essers"), "Found author");
  }
  
  private static Request getTestRequest() throws IOException {
    Request request = new Request();
    request.setMethod(HttpMethod.GET);
    request.setAccept(MediaType.APPLICATION_XML_TYPE);
    request.setPath("persons/1a873e95-3bdf-4b7f-8568-cc036b4cb644/research-outputs");
    request.addQueryParameter("apiKey", "54a9ccee-249b-464d-8028-7c8eac3886db");
    request.addQueryParameter("idClassification", "uuid");
    request.validate();
    // log.debug("Request:\n"+request.toString());
    return request;
  }
}

/*
 * Copyright Ⓒ 2019 Tilburg University
 * Copyright Ⓒ 2019 Fred Vos
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.mokolo.commons.ws;

import java.io.File;
import java.io.IOException;

/**
 * Implementation of {@Link Remote} that tries to find a response in a {@Link DirectoryRemote}.
 * If not available there, it tries the provided {@Link Remote}, typically a {@Link HttpRemote}.
 * If this is successful, then the response is stored as a \<hash\>.response file in the directory
 * and the request is stored as a \<hash\>.request file.
 * This \<hash\>.request file is only for documentation purposes.
 * 
 * Subsequest calls for this request will use the cache only.
 * 
 * This class is intended for creating a mock for responses using real life data -
 * a mock that can be used for unit tests or in cases where no access to the remote system is available.
 *
 */
public class CachingRemote implements Remote {

  private File directory;
  private DirectoryRemote directoryRemote;;
  private Remote remote;
  
  public CachingRemote(File directory, Remote remote) {
    this.directory = directory;
    this.directoryRemote = new DirectoryRemote(this.directory);
    this.remote = remote;
  }

  @Override
  public Response call(Request request) throws IOException {
    String md5Hash = request.getMd5Hash();
    if (this.directoryRemote.hasFiles(md5Hash))
      return this.directoryRemote.call(request);
    else {
      this.directoryRemote.writeToFile(request);
      Response response = this.remote.call(request);
      this.directoryRemote.writeToFiles(response, md5Hash, true);
      return response;
    }
  }
}

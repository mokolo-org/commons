/*
 * Copyright Ⓒ 2019 Fred Vos
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.mokolo.commons.ws;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.io.IOUtils;

@Slf4j
public class Response {
  @Getter private Status status;
  @Getter private MediaType contentType;
  @Getter private Map<String, List<String>> responseHeaders;
  @Getter private InputStream inputStream;
  
  public Response(Status status, MediaType contentType, InputStream inputStream) {
    this.status = status;
    this.contentType = contentType;
    this.responseHeaders = new HashMap<>();
    this.inputStream = inputStream;
  }
  
  public void putResponseheader(String key, String value) {
    List<String> values = this.responseHeaders.get(key);
    if (values == null) {
      values = new ArrayList<>();
      this.responseHeaders.put(key, values);
    }
    values.add(value);
  }
  
  protected Properties getMetadataAsProperties() {
    Properties properties = new Properties();
    if (this.status != null)
      properties.put("Status", this.status.toString());
    if (this.contentType != null)
      properties.put("Content-Type", this.contentType.toString());
    return properties;
  }
  
  public void close() throws IOException {
    if (this.inputStream != null)
      this.inputStream.close();
  }
  
  public void checkStatusAndThrowExceptionIfNotOK(Request request) throws IOException {
    if (this.status == null) {
      String msg = "Status of response is null";
      log.error(msg);
      throw new IOException(msg);
    }
    if (this.status == Status.OK)
      return;
    else if (this.status == Status.NO_CONTENT) {
      String msg = "201 No Content for "+request.toString();
      log.warn(msg);
      throw new NotFoundException(msg);
    }
    else if (this.status == Status.BAD_REQUEST) {
      String msg = "400 Bad Request Exception for "+request.toString();
      log.error(msg);
      throw new BadRequestException(msg);
    }
    else if (this.status == Status.NOT_FOUND) {
      String msg = "404 Not Found Exception for "+request.toString();
      log.error(msg);
      throw new NotFoundException(msg);
    }
    else if (this.status == Status.INTERNAL_SERVER_ERROR) {
      String msg = "500 Internal Server Error for "+request.toString();
      log.error(msg);
      throw new InternalServerErrorException(msg);
    }
    else {
      String msg = "Status "+this.status.toString()+" returned for "+request.toString();
      log.error(msg);
      throw new IOException(msg);
    }
  }
  
  /**
   * For debugging only.
   * it reads InputStream that is unusable afterwards!
   */
  public String toLongString() {
    StringBuffer buf = new StringBuffer();
    buf.append("Status: "+this.status.getStatusCode()+" "+this.status.toString()+"\n");
    if (this.contentType != null)
      buf.append("Content-Type: "+this.contentType+"\n");
    if (! this.responseHeaders.isEmpty()) {
      buf.append("Response-Headers:\n");
      for (Map.Entry<String, List<String>> entry : this.responseHeaders.entrySet()) {
        if (entry.getKey() != null && ! entry.getKey().equals("Content-Type")) {
          buf.append("- "+entry.getKey()+":\n");
          for (String value : entry.getValue()) {
            buf.append("  - "+value+"\n");
          }
        }
      }
    }
    if (this.inputStream != null) {
      try {
        buf.append("\n"+this.getString());
      } catch (IOException e) {
        buf.append("\n"+e.getClass().getName()+" occurred. Msg = "+e.getMessage());
      }
    }
    return buf.toString();
  }
  /**
   * Converts InputStream to String and returns it
   * @throws IOException 
   */
  public String getString() throws IOException {
    return IOUtils.toString(this.inputStream, StandardCharsets.UTF_8.name());
  }
}

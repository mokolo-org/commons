/*
 * Copyright Ⓒ 2019 Tilburg University
 * Copyright Ⓒ 2019 Fred Vos
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.mokolo.commons.ws;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import javax.ws.rs.core.MediaType;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.codec.digest.DigestUtils;
import org.mokolo.commons.io.HttpMethod;
import org.mokolo.commons.xml.Wellformer;
import org.mokolo.commons.xml.XMLValidator;
import org.xml.sax.SAXException;

/**
 * A request sent to a {@link Remote}
 */
@Slf4j
public class Request {

  @Getter @Setter private HttpMethod method;
  @Getter @Setter private MediaType contentType;
  @Getter @Setter private MediaType accept;
  @Getter private Map<String, String> extraHttpRequestHeaders; 
  @Getter @Setter private String path;
  @Getter private QueryParameters queryParameters;
  @Getter @Setter private String document;
  @Setter private XMLValidator xmlValidator;
  
  public Request() {
    this.extraHttpRequestHeaders = new HashMap<>();
    this.queryParameters = new QueryParameters();
  }
  
  /**
   * Service method to build a GET request without path and without query parameters, that expects XML
   */
  public static Request buildXmlGetRequest() {
    return buildXmlGetRequest(null, new QueryParameters());
  }
  
  /**
   * Service method to build a GET request without query parameters, that expects XML
   */
  public static Request buildXmlGetRequest(String path) {
    return buildXmlGetRequest(path, new QueryParameters());
  }

  /**
   * Service method to build a GET request that expects XML
   */
  public static Request buildXmlGetRequest(String path, QueryParameters queryParameters) {
    Request request = new Request();
    request.method = HttpMethod.GET;
    request.accept = MediaType.APPLICATION_XML_TYPE;
    request.path = path;
    request.queryParameters = queryParameters;
    return request;
  }
  
  /**
   * Service method to build a POST request without query parameters, that expects XML
   */
  public static Request buildXmlPostRequest(String path, String document) {
    Request request = new Request();
    request.method = HttpMethod.POST;
    request.contentType = MediaType.APPLICATION_XML_TYPE;
    request.accept = MediaType.APPLICATION_XML_TYPE;
    request.path = path;
    request.document = document;
    return request;
  }

  /**
   * Adds extra request header to request
   * 
   * Some request headers are added automatically using members of the class like Accept and Content-Type.
   * This method makes it possible to add extra request headers to the request,
   * for instance for authentication.
   */
  public void putExtraHttpRequestHeader(String key, String value) {
    this.extraHttpRequestHeaders.put(key, value);
  }
  
  /**
   * Adds query parameter
   */
  public void addQueryParameter(String key, String value) {
    this.queryParameters.add(key, value);
  }

  /**
   * Validates the request and throws an {@IOException} if validation fails
   */
  public void validate() throws IOException {
    if (this.method == null)
      throw new IOException("Method not set in Request");
    if (this.accept == null)
      throw new IOException("Accept not set in Request");
    if (this.method == HttpMethod.GET) {
      if (this.contentType != null)
        throw new IOException("Content-Type set in GET Request");
      if (this.document != null)
        throw new IOException("Document set in GET Request");
    }
    else if (this.method == HttpMethod.POST || this.method == HttpMethod.PUT) {
      if (this.contentType == null)
        throw new IOException("Content-Type not set in POST or PUT Request");
      if (this.document == null)
        throw new IOException("Document not set in POST or PUT Request");
      try {
        if (this.contentType == MediaType.APPLICATION_XML_TYPE && this.xmlValidator != null)
          this.xmlValidator.validate(this.document);
      } catch (SAXException e) {
        throw new IOException(e);
      }
    }
  }

  /**
   * Computes an MD5 hash for the request
   * 
   * Can be used to cache request responses.
   * It first converts the request into human readable text and the computes the MD5 hash for that text.
   */
  public String getMd5Hash() {
    return DigestUtils.md5Hex(this.toLongString());
  }
  
  /**
   * Generates the full path of the URL (for logging only)
   * 
   * Generates a string like:
   *  <code>
   *  cars?color=black&size=big
   * </code>
   */
  @Override
  public String toString() {
    StringBuffer buf = new StringBuffer();
    if (this.path != null)
      buf.append(this.path);
    if (this.queryParameters != null && ! this.queryParameters.isEmpty())
      buf.append("?"+this.queryParameters.toString());
    return buf.toString();
  }

  /**
   * Generates a human readable (and printable) description of the request
   * 
   * Generates a string like:
   *  <code>
   *  Method: GET
   *  Accept: application/xml
   *  Path: cars
   *  Query-Params:
   *  - color=black
   * </code>
   */
  public String toLongString() {
    StringBuffer buf = new StringBuffer();
    if (this.method != null)
      buf.append("Method: "+this.method+"\n");
    if (this.contentType != null)
      buf.append("Content-Type: "+this.contentType+"\n");
    if (this.accept != null)
      buf.append("Accept: "+this.accept+"\n");
    if (this.extraHttpRequestHeaders != null && ! this.extraHttpRequestHeaders.isEmpty()) {
      buf.append("Extra-HTTP-Request-Headers:\n");
      buf.append(this.getExtraHttpRequestHeadersAsString());
    }
    if (this.path != null)
      buf.append("Path: "+this.path+"\n");
    if (this.queryParameters != null && ! this.queryParameters.isEmpty())
      buf.append("Query-Params:\n"+this.queryParameters.asList());
    if (this.document != null) {
      if (this.contentType == MediaType.APPLICATION_XML_TYPE) {
        try {
          buf.append("\n"+Wellformer.wellformXml(this.document, true));
        } catch (IOException e) {
          log.error(e.getClass().getName()+" occurred. Msg="+e.getMessage());
        }
      }
      else
        buf.append("\n"+this.document+"\n");
    }
    return buf.toString();
  }

  private String getExtraHttpRequestHeadersAsString() {
    StringBuffer buf = new StringBuffer();
    if (this.extraHttpRequestHeaders != null) {
      TreeMap<String, String> treeMap = new TreeMap<>();
      treeMap.putAll(this.extraHttpRequestHeaders);
      for (Map.Entry<String, String> entry : treeMap.entrySet()) {
        buf.append("- "+entry.getKey()+": "+entry.getValue()+"\n");
      }
    }
    return buf.toString();
  }

}

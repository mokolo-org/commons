/*
 * Copyright Ⓒ 2019 Tilburg University
 * Copyright Ⓒ 2019 Fred Vos
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.mokolo.commons.ws;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;

import lombok.Cleanup;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.io.IOUtils;
import org.mokolo.commons.io.HttpMethod;

/**
 * Implementation of {@Link Remote} that accesses a remote system using HTTP methods
 */
@Slf4j
public class HttpRemote implements Remote {
  private String application;
  private Scheme scheme;
  private String host;
  private Integer port;
  private String basePath;
  
  public HttpRemote() { }

  /**
   * Setup for default port
   * 
   * @param application Used for logging
   * @param scheme Scheme.HTTP or Scheme.HTTPS
   * @param host Name or IP addres of host
   * @param basePath Base path for all requests
   */
  public HttpRemote(String application, Scheme scheme, String host, String basePath) {
    this();
    this.application = application;
    this.scheme = scheme;
    this.host = host;
    this.basePath = basePath;
  }
  
  /**
   * Setup for specific port
   * 
   * @param application Used for logging
   * @param scheme Scheme.HTTP or Scheme.HTTPS
   * @param host Name or IP addres of host
   * @param port Port
   * @param basePath Base path for all requests
   */
  public HttpRemote(String application, Scheme scheme, String host, Integer port, String basePath) {
    this(application, scheme, host, basePath);
    this.port = port;
  }
  
  /**
   * Sends request to remote system and returns Response
   */
  @Override
  public Response call(Request request) throws IOException {
    URL url = this.getUrl(request);
    log.info(this.application+" "+request.getMethod()+" "+url.toExternalForm());
    if (request.getDocument() != null &&
        request.getContentType() != null &&
        ( request.getContentType().toString().startsWith("text") || request.getContentType().toString().contains("xml")))
      log.debug(request.getDocument());
    HttpURLConnection connection =
        (HttpURLConnection) url.openConnection();
    connection.setRequestMethod(request.getMethod().toString());
    connection.setRequestProperty("Accept", request.getAccept().toString());
    for (Map.Entry<String, String> entry : request.getExtraHttpRequestHeaders().entrySet())
      connection.setRequestProperty(entry.getKey(), entry.getValue());
    if (request.getMethod() == HttpMethod.POST) {
      connection.setRequestProperty("Content-Type", "application/xml");
      connection.setDoOutput(true);
      @Cleanup OutputStream os = connection.getOutputStream();
      IOUtils.write(request.getDocument(), os, Charset.forName("UTF-8"));
      os.flush();
    }
    MediaType contentType = null;
    String contentTypeString = connection.getContentType();
    if (contentTypeString != null)
      contentType = MediaType.valueOf(contentTypeString); 
    Response response = new Response(
        Status.fromStatusCode(connection.getResponseCode()),
        contentType,
        connection.getInputStream());
    for (Map.Entry<String, List<String>> entry : connection.getHeaderFields().entrySet()) {
      for (String value : entry.getValue()) {
        response.putResponseheader(entry.getKey(), value);
      }
    }
    return response;
  }

  /**
   * Constructs URL
   */
  public URL getUrl(Request request) throws IOException {
    StringBuffer pathBuffer = new StringBuffer();
    if (this.basePath != null && ! this.basePath.isEmpty())
      pathBuffer.append(this.basePath);
    if (request.getPath() != null && ! request.getPath().isEmpty()) {
      if (pathBuffer.length() > 0)
        pathBuffer.append('/');
      pathBuffer.append(request.getPath());
    }
    String path = null;
    if (pathBuffer.length() > 0)
      path = pathBuffer.toString();
    
    UrlBuilder urlBuilder = new UrlBuilder()
        .withScheme(this.scheme)
        .withHost(this.host)
        .withPort(this.port)
        .withPath(path);
    for (Map.Entry<String, ArrayList<String>> entry : request.getQueryParameters().entrySet())
      for (String value : entry.getValue())
        urlBuilder = urlBuilder.withRequestParameter(entry.getKey(), value);
    return urlBuilder.build();
  }
  
  /**
   * Constructs base URL
   */
  public URL getBaseUrl() throws IOException {
    UrlBuilder urlBuilder = new UrlBuilder()
        .withScheme(this.scheme)
        .withHost(this.host)
        .withPort(this.port)
        .withPath(this.basePath);
    return urlBuilder.build();
  }
  
  
}

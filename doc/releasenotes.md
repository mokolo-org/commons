# Mokolo Commons - Release Notes

## Version 11.4

### Version 11.4.0 (2022-09-19)

- commons-cmd

  - Moved CommandRunner from commons-pckg into its own module
  
- commons-data

  - Improvements to support commons-text

- commons-parent

  - Removed joda-time from BOM - use java.time instead
  
- commons-text

  - Transform links as <a href="...">...</a> in text into proper
    markup
  - Convenience methods added

## Version 11.3

### Version 11.3.5 (2022-07-27)

- commons-data

  - In Templater class version of Freemarker templates from
    2.3.28 to 2.3.23 to match Freemarker package version in
    Debian bullseye and Ubuntu 20.04 LTS 

- commons-pckg

  - In DependencyMerger class include all main dirs with lower case names
    and include all WEB-INF/services/* files
    
### Version 11.3.4 (2022-07-26)

- commons-pckg

  - Added Classpath class for listing all classes in classpath

### Version 11.3.3 (2022-07-14)

- commons-io

  - logback

    - New method LogbackConfiguration.addRollingFileAppender()
    - Added test for LogbackConfiguration
    - First version of LogbackConfiguration.printSetup()


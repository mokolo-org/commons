package org.mokolo.commons.lang;

import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.TimeZone;

public enum DateFormat {
  APACHE("dd/MMM/yyyy:hh:mm:ss Z"),
  DATETIME("yyyy-MM-dd HH:mm:ss"),
  DATETIME_MS_TZ("yyyy-MM-dd HH:mm:ss.SSSZ"),
  DATETIME_TZ("yyyy-MM-dd HH:mm:ssZ"),
  DEFAULT("yyyy-MM-dd HH:mm:ss"),
  ISO_TZ("yyyy-MM-dd'T'HH:mm:ssZ"),
  ISO_MS_TZ("yyyy-MM-dd'T'HH:mm:ss.SSSZ"),
  YYYY_MM_DD("yyyy-MM-dd");

  private String formatString;

  private DateFormat(String formatString) {
    this.formatString = formatString;
  }

  public SimpleDateFormat getSimpleDateFormat() {
    return getSimpleDateFormat(TimeZone.getDefault(), Locale.getDefault());
  }

  public SimpleDateFormat getSimpleDateFormat(TimeZone timeZone) {
    return getSimpleDateFormat(timeZone, Locale.getDefault());
  }

  public SimpleDateFormat getSimpleDateFormat(Locale locale) {
    return getSimpleDateFormat(TimeZone.getDefault(), locale);
  }

  public SimpleDateFormat getSimpleDateFormat(TimeZone timeZone, Locale locale) {
    SimpleDateFormat sdf = new SimpleDateFormat(formatString, locale);
    sdf.setTimeZone(timeZone);
    return sdf;
  }

}

<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <groupId>org.mokolo.commons</groupId>
  <artifactId>commons-parent</artifactId>
  <!-- Version maintained by scripts; see commons/bin/README.md -->
  <version>11.4.0</version>
  <packaging>pom</packaging>
  <name>Mokolo Commons Parent</name>
  <description>Collection of shared libraries</description>

  <modules>
    <module>lang</module>
    <module>xml</module>
    <module>data</module>
    <module>io</module>
    <module>cmd</module>
    <module>text</module>
    <module>jaxrs</module>
    <module>ws</module>
    <module>http</module>
    <module>sql</module>
    <module>system</module>
    <module>cli</module>
    <module>pckg</module>
  </modules>

  <organization>
    <name>Mokolo Software Development and Tilburg University</name>
  </organization>

  <developers>
    <developer>
      <id>vosf</id>
      <name>Fred Vos</name>
      <email>fred.vos@mokolo.org</email>
      <organization>Mokolo Org</organization>
    </developer>
    <developer>
      <id>fvos</id>
      <name>Fred Vos</name>
      <email>fred.vos@tilburguniversity.edu</email>
      <organization>Tilburg University</organization>
    </developer>
  </developers>

  <!-- To fix problem with warning on Google Guice and illegal reflection things.
       Until Maven has fixed this, probably in 3.6.2.
       Add this option to mvn command: DASHDASHadd-opens java.base/java.lang=ALL-UNNAMED
       Or set this in file /etc/mavenrc on Linux Linux-systems:
       MAVEN_OPTS="$MAVEN_OPTS DASHDASHadd-opens java.base/java.lang=ALL-UNNAMED"
       Where 'DASHDASH' is two dashes :)
       -->

  <properties>
    <maven.compiler.release>11</maven.compiler.release>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>

    <!--+
        | POM version maintained by build system.
        |
        +-->
    <major.version>11</major.version>
    <minor.version>4</minor.version>
    <incremental.version>0</incremental.version>

    <ch.qos.logback.version>1.2.10</ch.qos.logback.version>
    <com.googlecode.json-simple.version>1.1.1</com.googlecode.json-simple.version>
    <com.jcraft.jsch.version>0.1.54</com.jcraft.jsch.version>
    <com.rometools.rome.version>1.16.0</com.rometools.rome.version>
    <com.vladsch.flexmark.version>0.64.0</com.vladsch.flexmark.version>
    <commons-codec.version>1.15</commons-codec.version>
    <commons-io.version>2.11.0</commons-io.version>
    <javax.activation.version>1.2.0</javax.activation.version>
    <javax.xml.bind.version>2.4.0-b180830.0359</javax.xml.bind.version>
    <net.sf.jopt-simple.version>5.0.4</net.sf.jopt-simple.version>
    <org.apache.commons.commons-csv.version>1.7</org.apache.commons.commons-csv.version>
    <org.apache.commons.commons-text.version>1.9</org.apache.commons.commons-text.version>
    <org.apache.maven.maven-core.version>3.8.5</org.apache.maven.maven-core.version>
    <org.apache.maven.plugins.assembly.version>3.1.0</org.apache.maven.plugins.assembly.version>
    <org.apache.maven.plugins.checkstyle.version>3.0.0</org.apache.maven.plugins.checkstyle.version>
    <org.apache.maven.plugins.compiler.version>3.8.0</org.apache.maven.plugins.compiler.version>
    <org.apache.maven.plugins.dependency.version>2.9</org.apache.maven.plugins.dependency.version>
    <org.apache.maven.plugins.findbugs.version>3.0.5</org.apache.maven.plugins.findbugs.version>
    <org.apache.maven.plugins.javadoc.version>3.0.1</org.apache.maven.plugins.javadoc.version>
    <org.apache.maven.plugins.pmd.version>3.11.0</org.apache.maven.plugins.pmd.version>
    <org.apache.maven.plugins.project-info-reports.version>3.0.0</org.apache.maven.plugins.project-info-reports.version>
    <org.apache.maven.plugins.site.version>3.7.1</org.apache.maven.plugins.site.version>
    <org.apache.maven.plugins.shade.version>3.2.1</org.apache.maven.plugins.shade.version>
    <org.apache.maven.plugins.surefire.version>2.22.0</org.apache.maven.plugins.surefire.version>
    <org.apache.maven.plugins.war.version>2.6</org.apache.maven.plugins.war.version>
    <org.apache.solr.version>8.8.2</org.apache.solr.version>
    <org.eclipse.jetty.version>9.4.13.v20181111</org.eclipse.jetty.version>
    <org.eclipse.persistence.version>2.7.5</org.eclipse.persistence.version>
    <org.freemarker.version>2.3.31</org.freemarker.version>
    <org.glassfish.jaxb.version>2.4.0-b180830.0438</org.glassfish.jaxb.version>
    <org.glassfish.jersey.version>2.29.1</org.glassfish.jersey.version>
    <org.hsqldb.version>2.5.0</org.hsqldb.version>
    <org.jsoup.version>1.14.3</org.jsoup.version>
    <org.junit.version>5.8.1</org.junit.version>
    <org.marc4j.version>2.9.2</org.marc4j.version>
    <org.mybatis.version>3.5.3</org.mybatis.version>
    <org.projectlombok.version>1.18.10</org.projectlombok.version>
    <org.quartz-scheduler.version>2.3.2</org.quartz-scheduler.version>
    <org.slf4j.version>2.0.0-alpha5</org.slf4j.version>
    <xerces.version>2.12.1</xerces.version>
  </properties>

  <licenses>
    <license>
      <name>Apache 2.0</name>
      <url>http://www.opensource.org/licenses/Apache-2.0</url>
      <distribution>repo</distribution>
    </license>
  </licenses>

  <scm>
    <connection>scm:git:https://github.com/fredvos/commons</connection>
    <developerConnection>scm:git:https://github.com/fredvos/commons</developerConnection>
  </scm>

  <dependencies>
    <dependency>
      <groupId>ch.qos.logback</groupId>
      <artifactId>logback-core</artifactId>
      <version>${ch.qos.logback.version}</version>
    </dependency>
    <dependency>
      <groupId>ch.qos.logback</groupId>
      <artifactId>logback-classic</artifactId>
      <version>${ch.qos.logback.version}</version>
    </dependency>
    <dependency>
      <groupId>org.junit.jupiter</groupId>
      <artifactId>junit-jupiter-api</artifactId>
      <version>${org.junit.version}</version>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>org.junit.jupiter</groupId>
      <artifactId>junit-jupiter-engine</artifactId>
      <version>${org.junit.version}</version>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>org.projectlombok</groupId>
      <artifactId>lombok</artifactId>
      <version>${org.projectlombok.version}</version>
      <scope>provided</scope>
    </dependency>
  </dependencies>

  <repositories>
    <repository>
      <id>mokolo-org</id>
      <url>https://gitlab.com/api/v4/groups/13041011/-/packages/maven</url>
    </repository>
  </repositories>

  <distributionManagement>
    <repository>
      <id>mokolo-org</id>
      <url>https://gitlab.com/api/v4/projects/13573600/packages/maven</url>
    </repository>
    <snapshotRepository>
      <id>mokolo-org</id>
      <url>https://gitlab.com/api/v4/projects/13573600/packages/maven</url>
    </snapshotRepository>
  </distributionManagement>

  <build>
    <directory>${project.basedir}/target.v${major.version}.${minor.version}</directory>
    <finalName>${project.artifactId}-${project.version}</finalName>

    <pluginManagement>
      <plugins>

        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-compiler-plugin</artifactId>
          <version>${org.apache.maven.plugins.compiler.version}</version>
          <executions>
            <!-- Replacing default-compile as it is treated specially by maven -->
            <execution>
              <id>default-compile</id>
              <phase>none</phase>
            </execution>
            <!-- Replacing default-testCompile as it is treated specially by maven -->
            <execution>
              <id>default-testCompile</id>
              <phase>none</phase>
            </execution>
            <execution>
              <id>java-compile</id>
              <phase>compile</phase>
              <goals> <goal>compile</goal> </goals>
            </execution>
            <execution>
              <id>java-test-compile</id>
              <phase>test-compile</phase>
              <goals> <goal>testCompile</goal> </goals>
            </execution>
          </executions>
        </plugin>
      </plugins>
    </pluginManagement>

    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-site-plugin</artifactId>
        <version>${org.apache.maven.plugins.site.version}</version>
      </plugin>

      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-surefire-plugin</artifactId>
        <version>${org.apache.maven.plugins.surefire.version}</version>
        <configuration>
          <!-- forkCount 0 prevents problems introduced with Surefire 2.22 -->
         <forkCount>1</forkCount>
        </configuration>
      </plugin>
    </plugins>
  </build>

  <reporting>
    <plugins>

      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-project-info-reports-plugin</artifactId>
        <version>${org.apache.maven.plugins.project-info-reports.version}</version>
        <configuration>
          <!--dependencyDetailsEnabled>false</dependencyDetailsEnabled-->
          <dependencyLocationsEnabled>false</dependencyLocationsEnabled>
        </configuration>
      </plugin>

      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-pmd-plugin</artifactId>
        <version>${org.apache.maven.plugins.pmd.version}</version>
        <configuration>
          <includeXmlInSite>true</includeXmlInSite>
        </configuration>
      </plugin>

      <plugin>
        <groupId>org.codehaus.mojo</groupId>
        <artifactId>findbugs-maven-plugin</artifactId>
        <version>${org.apache.maven.plugins.findbugs.version}</version>
        <configuration>
          <effort>Max</effort>
          <!-- Reports all bugs (other values are medium and max) -->
          <threshold>Low</threshold>
          <!-- Produces XML report -->
          <xmlOutput>true</xmlOutput>
        </configuration>
      </plugin>

      <!--plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-checkstyle-plugin</artifactId>
        <version>${org.apache.maven.plugins.checkstyle.version}</version>
        <configuration>
          <configLocation>checkstyle.xml</configLocation>
        </configuration>
      </plugin-->

      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-javadoc-plugin</artifactId>
        <version>${org.apache.maven.plugins.javadoc.version}</version>
        <reportSets>
          <reportSet><!-- by default, id = "default" -->
            <reports><!-- select non-aggregate reports -->
              <report>javadoc</report>
              <report>test-javadoc</report>
            </reports>
          </reportSet>
          <reportSet><!-- aggregate reportSet, to define in poms having modules -->
            <id>aggregate</id>
            <inherited>false</inherited><!-- don't run aggregate in child modules -->
            <reports>
              <report>aggregate</report>
            </reports>
          </reportSet>
        </reportSets>
      </plugin>
    </plugins>
  </reporting>

</project>

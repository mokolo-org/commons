/*
 * Copyright Ⓒ 2018 Tilburg University
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.mokolo.commons.xml.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.mokolo.commons.xml.Wellformer;

public class WellformerTest {

  @Test
  public void canCleanupNonWellformedHTMLFragment() {
    String part = "<div><p>Lorem ipsum...</p>";
    assertEquals("<div><p>Lorem ipsum...</p></div>", Wellformer.wellformHtmlPart(part), "Tidy");
  }

  @Test
  public void canCleanupHTMLFragmentWithoutSingleParentNode() {
    String part = "<p>Lorem ipsum...</p><p>Aenean commodo...</p>";
    assertEquals(part, Wellformer.wellformHtmlPart(part), "Tidy"); // No change
  }

  @Test
  public void canPrettyPrintFragment() {
    String part = "<div><p>Lorem ipsum...</p></div>";
    assertEquals("<div>\n  <p>Lorem ipsum...</p>\n</div>", Wellformer.wellformHtmlPart(part, true), "Pretty printed");
  }
  
  @Test
  public void canPrettyPrintXml() throws Exception {
    String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<doc><text>Lorem ipsum...</text></doc>\n";
    assertEquals("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<doc>\n  <text>Lorem ipsum...</text>\n</doc>\n", Wellformer.wellformXml(xml, true), "Pretty printed XML");
  }

}

/*
 * Copyright Ⓒ 2018 Tilburg University
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.mokolo.commons.xml;

import java.io.OutputStream;
import java.io.StringWriter;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import lombok.extern.slf4j.Slf4j;

import org.w3c.dom.Node;

@Slf4j
public class Serializer {
  private Transformer transformer;
  private boolean doOmitXMLDeclaration;
  
  public Serializer() {
    try {
      this.transformer = TransformerFactory.newInstance().newTransformer();
      this.doOmitXMLDeclaration = false;
      this.transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
      this.transformer.setOutputProperty(OutputKeys.METHOD, "xml");
      this.transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
      this.transformer.setOutputProperty(OutputKeys.DOCTYPE_PUBLIC, "yes");
    } catch (TransformerConfigurationException|TransformerFactoryConfigurationError e) {
      log.error(e.getClass().getName()+" occurred. Msg="+e.getMessage());
    }
  }
  
  public void omitXMLDeclaration() {
    this.transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
    this.doOmitXMLDeclaration = true;
  }
  
  public void setIndentation(int indentation) {
    if (this.transformer != null) {
      if (indentation > 0) {
        this.transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        this.transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", Integer.toString(indentation));
      }
      else {
        this.transformer.setOutputProperty(OutputKeys.INDENT, "no");
      }
    }
  }
  
  public static Serializer getNodeSerializer(int indentation) {
    Serializer serializer = new Serializer();
    serializer.omitXMLDeclaration();
    serializer.setIndentation(indentation);
    return serializer;
  }

  public String toXML(Node node) {
    if (this.nodeIsSerializable(node))
      return this.serializeToString(node);
    else
      return null;
  }
  
  private String serializeToString(Node node) {
    String result = null;
    
    DOMSource domSource = new DOMSource(node);
    StringWriter sw = new StringWriter();
    StreamResult sr = new StreamResult(sw);
    try {
      this.transformer.transform(domSource, sr);
      result = sw.toString();
    } catch (TransformerException e) {
      log.error("Serializer cannot transform node. Msg="+e.getMessage());
      return null;
    }
    return result;
  }

  public void serialize(Node node, OutputStream os) {
    if (this.nodeIsSerializable(node))
      this.serializeToOutputStream(node, os);
  }
  
  private void serializeToOutputStream(Node node, OutputStream os) {
    DOMSource domSource = new DOMSource(node);
    StreamResult sr = new StreamResult(os);
    try {
      this.transformer.transform(domSource, sr);
    } catch (TransformerException e) {
      log.error("Serializer cannot transform node. Msg="+e.getMessage());
    }
  }

  public boolean nodeIsSerializable(Node node) {
    if (node.getNodeType() == Node.DOCUMENT_NODE) {
      return true;
    }
    else if (node.getNodeType() == Node.ELEMENT_NODE) {
      if (this.doOmitXMLDeclaration == false) {
        log.error("Serializer can only serialize elements when XML declaration omitted");
        return false;
      }
      else
        return true;
    }
    else {
      log.error("Serializer can only serialize documents or elements");
      return false;
    }
  }

}

/*
 * Copyright Ⓒ 2019 Fred Vos
 * Copyright Ⓒ 2019 Tilburg University
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.mokolo.commons.xml;

import java.io.InputStream;

import lombok.extern.slf4j.Slf4j;

import org.w3c.dom.bootstrap.DOMImplementationRegistry;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSInput;
import org.w3c.dom.ls.LSResourceResolver;

/**
 * Implementation of LSResourceResolver that can resolve XML schemas from the classpath.
 * 
 * Based on: http://www.java.net/node/666263
 */
@Slf4j
public class ClasspathResourceResolver implements LSResourceResolver {
  private DOMImplementationLS domImplementationLS;

  public ClasspathResourceResolver()
  throws ClassNotFoundException, IllegalAccessException, InstantiationException {
    System.setProperty(DOMImplementationRegistry.PROPERTY, "org.apache.xerces.dom.DOMImplementationSourceImpl");
    DOMImplementationRegistry registry = DOMImplementationRegistry.newInstance();
    this.domImplementationLS = (DOMImplementationLS) registry.getDOMImplementation("LS");
  }

  @Override
  public LSInput resolveResource(
      String type,
      String namespaceURI,
      String publicId,
      String systemId,
      String baseURI) {
    
    log.info(
        "Resolving: type='" + type +
        "', namespaceURI='" + namespaceURI +
        "', publicId='" + publicId +
        "', systemId='" + systemId +
        "', baseURI='" + baseURI + "'");

    LSInput lsInput = domImplementationLS.createLSInput();
    InputStream is = getClass().getResourceAsStream("/" + systemId); // FindBugs complains, but cannot close is here.
    lsInput.setByteStream(is);
    lsInput.setSystemId(systemId);
    
    return lsInput;
  }
}

/*
 * Copyright Ⓒ 2019 Tilburg University
 * Copyright Ⓒ 2019 Fred Vos
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.mokolo.commons.xml;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.apache.commons.io.IOUtils;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import lombok.Cleanup;

/**
 * Service to validate XML documents against XSD schemas.
 * 
 * Based on http://www.ibm.com/developerworks/xml/library/x-javaxmlvalidapi.html
 */
public class XMLValidator {
  
  private SchemaFactory schemaFactory;
  private Validator validator;

  private XMLValidator()
  throws ClassNotFoundException,
         IllegalAccessException,
         InstantiationException {
    this.schemaFactory = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");

    /*
     * If a schema contains include-statements referencing other schemas,
     * we us a resource resolver that uses the systemId (name of the file
     * without path or URI) to fetch the XSD schema from the classpath
     * instead of fetching it from the web. This means that everything needs
     * to be in the classpath. All imported schemas must be in classpath and
     * references in import statements must be changed into simple file names
     * like 'dc.xsd' (no URLs):
     */
    schemaFactory.setResourceResolver(new ClasspathResourceResolver());
  }

  public XMLValidator(File file)
  throws FileNotFoundException,
         SAXException,
         ClassNotFoundException,
         IllegalAccessException,
         InstantiationException {
    this();
    if (! file.exists())
      throw new FileNotFoundException("XSD file "+file.getAbsolutePath()+" not found in validate()");
    Schema schema = schemaFactory.newSchema(file);
    this.validator = schema.newValidator();
  }
  
  public XMLValidator(InputStream is)
  throws FileNotFoundException,
         SAXException,
         ClassNotFoundException,
         IllegalAccessException,
         InstantiationException {
    this();
    Source source = new StreamSource(is);
    Schema schema = schemaFactory.newSchema(source);
    this.validator = schema.newValidator();
  }
  
  public void validate(Document document)
  throws SAXException, IOException {
    Serializer serializer = new Serializer();
    @Cleanup InputStream is = IOUtils.toInputStream(serializer.toXML(document), "UTF-8"); 
    this.validate(is);
  }
  
  public void validate(String s)
  throws SAXException, IOException {
    @Cleanup InputStream is = new ByteArrayInputStream(s.getBytes());
    this.validate(is);
  }

  public void validate(InputStream is)
  throws FileNotFoundException, IOException, SAXException  {
    Source source = new StreamSource(is);
    this.validator.validate(source);
  }
  
}

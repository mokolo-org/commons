/*
cd  * Copyright Ⓒ 2018 Tilburg University
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.mokolo.commons.xml;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.xml.sax.SAXException;

import lombok.Cleanup;

public class Wellformer {

  /**
   * Wellform HTML fragment without pretty printing
   */
  public static String wellformHtmlPart(String part) {
    return wellformHtmlPart(part, false);
  }

  /**
   * Wellform HTML fragment with or without pretty printing
   */
  public static String wellformHtmlPart(String part, boolean prettyPrint) {
    Document doc = Jsoup.parseBodyFragment(part);
    doc.outputSettings().prettyPrint(prettyPrint);
    doc.outputSettings().indentAmount(2);
    Element body = doc.body();
    return body.html();
	}

  /**
   * Wellform XML document with or without pretty printing
   * 
   * Does not really wellform an XML document that is not welllformed.
   * Only pretty prints it on request and creates exception if invalid.
   */
  public static String wellformXml(String xml, boolean prettyPrint) throws IOException {
    String retval = null;
    try {
      DocumentBuilder builder = DocumentBuilderFactory.getDocumentBuilder();
      @Cleanup InputStream is = new ByteArrayInputStream(xml.getBytes());
      org.w3c.dom.Document doc = builder.parse(is);
      doc.setXmlStandalone(true);
      Serializer serializer = new Serializer();
      if (prettyPrint)
        serializer.setIndentation(2);
      retval = serializer.toXML(doc);
    } catch (ParserConfigurationException | SAXException e) {
      throw new IOException(e);
    }
    return retval;
  }
}

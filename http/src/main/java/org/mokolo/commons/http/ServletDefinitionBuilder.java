package org.mokolo.commons.http;

public interface ServletDefinitionBuilder extends Cloneable {
  ServletDefinition build(String applicationName);
}

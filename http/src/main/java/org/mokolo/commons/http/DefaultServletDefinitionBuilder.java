/*
 * Copyright Ⓒ 2019 Fred Vos
 * Copyright Ⓒ 2019 Tilburg University
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.mokolo.commons.http;

import java.io.File;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DefaultServletDefinitionBuilder implements ServletDefinitionBuilder {
  private String pathSpec;
  private File resourceBase;
  private boolean dirAllowed;
  
  public DefaultServletDefinitionBuilder() {
    this.pathSpec = "/*";
    this.resourceBase = new File("MUST SET RESOURCEBASE");
    this.dirAllowed = false;
  }
  
  @Override
  public DefaultServletDefinitionBuilder clone() {
    DefaultServletDefinitionBuilder builder = new DefaultServletDefinitionBuilder();
    builder.pathSpec = this.pathSpec;
    builder.resourceBase = this.resourceBase;
    builder.dirAllowed = this.dirAllowed;
    return builder;
  }
  
  public DefaultServletDefinitionBuilder withPathSpec(String pathSpec) {
    DefaultServletDefinitionBuilder builder = this.clone();
    builder.pathSpec = pathSpec;
    return builder;
  }
  
  public DefaultServletDefinitionBuilder withResourceBase(File resourceBase) {
    DefaultServletDefinitionBuilder builder = this.clone();
    builder.resourceBase = resourceBase;
    log.info("ResourceBase = "+builder.resourceBase.getAbsolutePath());
    return builder;
  }

  public DefaultServletDefinitionBuilder withDirAllowed() {
    DefaultServletDefinitionBuilder builder = this.clone();
    builder.dirAllowed = true;
    return builder;
  }
  
  @Override
  public ServletDefinition build(String applicationName) {
    return new ServletDefinition(
        new LoggingDefaultServlet(applicationName, this.getPrefix()), this.pathSpec)
            .withInitParameter("resourceBase", this.resourceBase.getAbsolutePath())
            .withInitParameter("pathInfoOnly", "true")
            .withInitParameter("dirAllowed", Boolean.toString(this.dirAllowed));
  }
  
  private String getPrefix() {
    return this.pathSpec.replaceAll("\\/\\*", "");
  }
}

/*
 * Copyright Ⓒ 2018 Tilburg University
 * Copyright Ⓒ 2018 Fred Vos
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.mokolo.commons.http;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import lombok.extern.slf4j.Slf4j;

/*
 * https://www.eclipse.org/jetty/documentation/current/embedding-jetty.html#_embedding_servletcontexts
 */

@Slf4j
public class ServerBuilder implements Cloneable {

  private String applicationName;
  private int port;
  private String contextPath;
  private boolean sessions;
  private File resourceBase;
  private List<ServletDefinitionBuilder> servletDefinitionBuilders;
  
  public static final int DEFAULT_PORT = 8080;
  
  public ServerBuilder() {
    this.applicationName = "Server";
    this.port = DEFAULT_PORT;
    this.contextPath = "/";
    this.sessions = false;
    this.resourceBase = new File("MUST SET RESOURCEBASE");
    this.servletDefinitionBuilders = new ArrayList<>();
  }
  
  @Override
  public ServerBuilder clone() {
    ServerBuilder builder = new ServerBuilder();
    builder.applicationName = this.applicationName;
    builder.port = this.port;
    builder.contextPath = this.contextPath;
    builder.sessions = this.sessions;
    builder.resourceBase = this.resourceBase;
    builder.servletDefinitionBuilders.addAll(this.servletDefinitionBuilders);
    return builder;
  }
  
  public ServerBuilder withApplicationName(String applicationName) {
    ServerBuilder builder = this.clone();
    builder.applicationName = applicationName;
    return builder;
  }
  
  public ServerBuilder withPort(int port) {
    ServerBuilder builder = this.clone();
    builder.port = port;
    return builder;
  }
  
  public ServerBuilder withContextPath(String contextPath) {
    ServerBuilder builder = this.clone();
    builder.contextPath = contextPath;
    return builder;
  }
  
  public ServerBuilder withSessions() {
    ServerBuilder builder = this.clone();
    builder.sessions = this.sessions;
    return builder;
  }

  public ServerBuilder withResourceBase(File resourceBase) {
    ServerBuilder builder = this.clone();
    builder.resourceBase = this.resourceBase;
    return builder;
  }

  public ServerBuilder withServletDefinitionBuilder(ServletDefinitionBuilder servletDefinitionBuilder) {
    ServerBuilder builder = this.clone();
    builder.servletDefinitionBuilders.add(servletDefinitionBuilder);
    return builder;    
  }
  
  public Server build() {
    Server server = new Server(this.port);
    int options = ServletContextHandler.NO_SESSIONS;
    if (this.sessions == true)
      options = ServletContextHandler.SESSIONS;
    ServletContextHandler context = new ServletContextHandler(options);
    context.setContextPath(this.contextPath);
    context.setResourceBase(this.resourceBase.getAbsolutePath());
    server.setHandler(context);

    for (ServletDefinitionBuilder servletDefinitionBuilder : this.servletDefinitionBuilders) {
      ServletDefinition servletDefinition = servletDefinitionBuilder.build(this.applicationName);
      context.addServlet(
          servletDefinition.buildServletHolder(),
          servletDefinition.getPathSpec());
    }
    
    // Add DefaultServlet (required, must be last)
    ServletHolder defaultServletHolder = new ServletHolder("default", DefaultServlet.class);
    defaultServletHolder.setInitParameter("dirAllowed", "true");
    context.addServlet(defaultServletHolder, "/");
    
    return server;
  }
  
  public void buildAndRun() {
    Server server = this.build();
    try {
      server.start();
      server.join();
      log.info(String.format("%s (HTTP) started", this.applicationName));
    } catch (Exception e) {
      log.error(e.getClass().getName()+" occurred. Msg = "+e.getMessage());
      try {
        server.stop();
        server.destroy();
      } catch (Exception e2) {
        log.error(e.getClass().getName()+" occurred. Msg = "+e.getMessage());
      }
    }
    log.info(String.format("%s (HTTP) stopped", this.applicationName));
  }
}

/*
 * Copyright Ⓒ 2019 Fred Vos
 * Copyright Ⓒ 2019 Tilburg University
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.mokolo.commons.http;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.glassfish.jersey.servlet.ServletContainer;

public class JAXRSServletDefinitionBuilder implements ServletDefinitionBuilder {
  private String pathSpec;
  private boolean formattedOutput;
  private List<String> providerPackages;

  public JAXRSServletDefinitionBuilder() {
    this.pathSpec = "/*";
    this.providerPackages = new ArrayList<>();
  }
  
  @Override
  public JAXRSServletDefinitionBuilder clone() {
    JAXRSServletDefinitionBuilder builder = new JAXRSServletDefinitionBuilder();
    builder.pathSpec = this.pathSpec;
    builder.formattedOutput = this.formattedOutput;
    builder.providerPackages.addAll(this.providerPackages);
    return builder;
  }
  
  public JAXRSServletDefinitionBuilder withPathSpec(String pathSpec) {
    JAXRSServletDefinitionBuilder builder = this.clone();
    builder.pathSpec = pathSpec;
    return builder;
  }
  
  public JAXRSServletDefinitionBuilder withFormattedOutput(boolean formattedOutput) {
    JAXRSServletDefinitionBuilder builder = this.clone();
    builder.formattedOutput = formattedOutput;
    return builder;
  }
  
  public JAXRSServletDefinitionBuilder withProviderPackages(String... providerPackages) {
    JAXRSServletDefinitionBuilder builder = this.clone();
    builder.providerPackages.addAll(Arrays.asList(providerPackages));
    return builder;
  }
  
  @Override
  public ServletDefinition build(String applicationName) {
    return new ServletDefinition(
        new ServletContainer(), this.pathSpec)
            .withInitParameter("jersey.config.xml.formatOutput", Boolean.toString(this.formattedOutput))
            .withInitParameter("jersey.config.server.provider.packages", String.join(",", this.providerPackages))
            .withInitParameter("jersey.config.server.provider.classnames", "org.glassfish.jersey.media.multipart.MultiPartFeature");
  }
}

package org.mokolo.commons.http;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.servlet.DefaultServlet;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class LoggingDefaultServlet extends DefaultServlet {
  private static final long serialVersionUID = 2962005263027429679L;
  
  private String applicationName;
  private String prefix;
  
  public LoggingDefaultServlet(String applicationName, String prefix) {
    this.applicationName = applicationName;
    this.prefix = prefix;
  }
  
  @Override
  protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
  throws ServletException, IOException {
    this.logRequest(req);
    super.doDelete(req, resp);
  }
  
  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp)
  throws ServletException, IOException {
    this.logRequest(req);
    super.doGet(req, resp);
  }
  
  @Override
  protected void doHead(HttpServletRequest req, HttpServletResponse resp)
  throws ServletException, IOException {
    this.logRequest(req);
    super.doHead(req, resp);
  }
  
  @Override
  protected void doOptions(HttpServletRequest req, HttpServletResponse resp)
  throws ServletException, IOException {
    this.logRequest(req);
    super.doOptions(req, resp);
  }
  
  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp)
  throws ServletException, IOException {
    this.logRequest(req);
    super.doPost(req, resp);
  }
  
  @Override
  protected void doPut(HttpServletRequest req, HttpServletResponse resp)
  throws ServletException, IOException {
    this.logRequest(req);
    super.doPut(req, resp);
  }
  
  @Override
  protected void doTrace(HttpServletRequest req, HttpServletResponse resp)
  throws ServletException, IOException {
    this.logRequest(req);
    super.doTrace(req, resp);
  }
  
  private void logRequest(HttpServletRequest req) {
    log.info(
        this.applicationName+" "+
        req.getMethod()+" "+
        (this.prefix != null ? this.prefix : "")+
        req.getPathInfo()+
        (req.getQueryString() != null ? "?"+req.getQueryString() : ""));
  }
}

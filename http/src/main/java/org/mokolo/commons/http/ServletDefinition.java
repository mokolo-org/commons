/*
 * Copyright Ⓒ 2019 Fred Vos
 * Copyright Ⓒ 2019 Tilburg University
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.mokolo.commons.http;

import java.util.HashMap;

import java.util.Map;

import javax.servlet.Servlet;

import org.eclipse.jetty.servlet.ServletHolder;

import lombok.Getter;

public class ServletDefinition implements Cloneable {
  private Servlet servlet;
  @Getter private String pathSpec;
  private Map<String, String> initParameters;

  public ServletDefinition(Servlet servlet, String pathSpec) {
    this.servlet = servlet;
    this.pathSpec = pathSpec;
    this.initParameters = new HashMap<>();
  }
  
  @Override
  public ServletDefinition clone() {
    ServletDefinition def = new ServletDefinition(this.servlet, this.pathSpec);
    def.initParameters.putAll(this.initParameters);
    return def;
  }

  public ServletDefinition withInitParameter(String key, String value) {
    ServletDefinition def = this.clone();
    def.initParameters.put(key,  value);
    return def;
  }
  
  public ServletHolder buildServletHolder() {
    ServletHolder servletHolder = new ServletHolder(this.servlet);
    for (String key : this.initParameters.keySet())
      servletHolder.setInitParameter(key, this.initParameters.get(key));
    return servletHolder;
  }
}

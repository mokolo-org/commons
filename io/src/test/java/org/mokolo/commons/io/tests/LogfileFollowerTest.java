/*
 * Copyright Ⓒ 2019 Fred Vos
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.mokolo.commons.io.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mokolo.commons.io.LogfileFollower;

public class LogfileFollowerTest {

  @Test
  public void canFollowLogfile()
  throws IOException {
    File tempFile = File.createTempFile(
        "commons-io", "", new File(System.getProperty("java.io.tmpdir")));
    tempFile.deleteOnExit();
    System.out.println("Temp file = "+tempFile.getAbsolutePath());
    PrintStream log = new PrintStream(tempFile);
    LogfileFollower follower = new LogfileFollower(tempFile);
    List<String> lines;
    
    // Empty file:
    lines = follower.newLines();
    assertEquals(0, lines.size());
    
    // Write two lines:
    log.println("Line 1");
    log.println("Line 2");
    lines = follower.newLines();
    assertEquals(2, lines.size());
    assertEquals("Line 2", lines.get(1));
    
    // Write one more line:
    log.println("Line 3");
    lines = follower.newLines();
    assertEquals(1, lines.size());
    assertEquals("Line 3", lines.get(0));
    
    // Write one and a half line and finish later:
    log.println("Line 4");
    log.print("Line 5 begin");
    lines = follower.newLines();
    assertEquals(1, lines.size());
    
    // End last line and start a new one:
    log.println(" end");
    log.print("Line 6 begin");
    lines = follower.newLines();
    assertEquals(1, lines.size());
    assertEquals("Line 5 begin end", lines.get(0));
    
    // End last line:
    log.println(" end");
    lines = follower.newLines();
    assertEquals(1, lines.size());
    assertEquals("Line 6 begin end", lines.get(0));
    
    // A line only missing a newline:
    log.print("Line 7");
    lines = follower.newLines();
    assertEquals(0, lines.size());
    log.println();
    lines = follower.newLines();
    assertEquals(1, lines.size());
    assertEquals("Line 7", lines.get(0));
    
    // Delete:
    log.close();
    lines = follower.newLines();
    assertEquals(0, lines.size());
  }

}

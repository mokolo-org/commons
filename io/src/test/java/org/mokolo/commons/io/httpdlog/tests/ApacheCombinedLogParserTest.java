/*
 * Copyright Ⓒ 2019 Tilburg University
 * Copyright Ⓒ 2019 Fred Vos
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.mokolo.commons.io.httpdlog.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Map;
import java.util.TimeZone;

import org.junit.jupiter.api.Test;
import org.mokolo.commons.io.httpdlog.ApacheCombinedLogParser;
import org.mokolo.commons.io.httpdlog.HttpdLogEntry;
import org.mokolo.commons.lang.DateFormat;
import org.mokolo.commons.io.HttpMethod;

public class ApacheCombinedLogParserTest {

  private static final String testEntry1 =
      "11.12.13.14 - - [12/Jun/2019:07:25:26 +0200] " +
      "\"GET /products/bicicles?color=red HTTP/2.0\" 200 11229 \"https://www.google.nl/\" " +
      "\"Mozilla/5.0 (iPhone; CPU iPhone OS 12_1_4 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0 Mobile/15E148 Safari/604.1\""; 
  private static final String testEntry2 =
      "2001:ee0:4140:65be:2d37:d340:8c98:4ba9 - - [26/Jun/2019:19:25:36 +0200] " +
      "\"GET /students HTTP/2.0\" 200 8970 \"-\" " +
      "\"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/79.0.108 Chrome/73.0.3683.108 Safari/537.36\"";
  private static final String testEntry3 =
      "101.102.103.104 - - [03/Jul/2019:08:55:41 +0200] " +
      "\"GET /news/rss.xml HTTP/1.1\" 200 7261 \"\" " +
      "\"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36; +collection@infegy.com\"";
  private static final String testEntry4 =
      "101.102.103.104 - - [04/Jul/2019:05:26:22 +0200] " +
      "\"GET /nl HTTP/1.1\" 200 10439 \"-\" " +
      "\"\"";
  private static final String testEntry5 =
      "141.98.102.235 - - [03/Jul/2019:23:14:00 +0200] " +
      "\"HEAD /nl/ HTTP/1.1\" 301 4698 \"-\" " +
      "\"\\\"Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36\\\"\"";
  private static final String testEntry6 =
      "2a02:a456:77db:1:e8ba:fa9a:5f55:f252 16847 - [03/Mar/2020:12:45:48 +0100] " +
      "\"GET /education/languages/dutch-iii HTTP/2.0\" 200 7600 \"https://www.tilburguniversity.edu/education/bla/\" " +
      "\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36\" .fabri";
  
  @Test
  public void canParseRequestWithAllFields() throws Exception {
    Map<String, String> request = ApacheCombinedLogParser.parseRequest("GET /static/uvtpresentation/js/jquery-ui-1.8.2.custom.min.js HTTP/2.0");
    assertEquals("GET", request.get("httpMethod"));
    assertEquals("/static/uvtpresentation/js/jquery-ui-1.8.2.custom.min.js", request.get("path"));
    assertEquals("HTTP/2.0", request.get("protocol"));
  }
  
  @Test
  public void canParseRequestWithNoProtocol() throws Exception {
    Map<String, String> request = ApacheCombinedLogParser.parseRequest("GET /contact/tel:+31 (0)13");
    assertEquals("GET", request.get("httpMethod"));
    assertEquals("/contact/tel:+31 (0)13", request.get("path"));
    assertNull(request.get("protocol"));
  }
  
  @Test
  public void canParseEmptyRequest() throws Exception {
    Map<String, String> request = ApacheCombinedLogParser.parseRequest("-");
    assertNull(request.get("httpMethod"));
    assertNull(request.get("path"));
    assertNull(request.get("protocol"));
  }
  
  @Test
  public void canParseLogEntryWithIPv4() throws Exception {
    HttpdLogEntry logEntry = getEntry1();
    assertEquals("11.12.13.14", logEntry.getInetAddress().getHostAddress(), "IPv4 address matches");
    assertEquals("2019-06-12 05:25:26+0000", DateFormat.DATETIME_TZ.getSimpleDateFormat(TimeZone.getTimeZone("GMT")).format(logEntry.getTimestamp()), "Timestamp matches");
    assertEquals(HttpMethod.GET, logEntry.getHttpMethod(), "GET matches");
    assertEquals("/products/bicicles?color=red", logEntry.getPath(), "Request matches");
    assertEquals(200, logEntry.getStatus().intValue(), "Success");
    assertEquals("https://www.google.nl/", logEntry.getReferer(), "Google");
    assertEquals(
        "Mozilla/5.0 (iPhone; CPU iPhone OS 12_1_4 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0 Mobile/15E148 Safari/604.1",
        logEntry.getUserAgent());
  }
  
  @Test
  public void canParseLogEntryWithIPv6() throws Exception {
    HttpdLogEntry logEntry = getEntry2();
    assertEquals("2001:ee0:4140:65be:2d37:d340:8c98:4ba9", logEntry.getInetAddress().getHostAddress(), "IPv6 address matches");
    assertNull(logEntry.getReferer(), "Referer is null");
  }
  
  @Test
  public void canParseLogEntryWithEmptyReferer() throws Exception {
    HttpdLogEntry logEntry = getEntry3();
    assertTrue((logEntry.getUserAgent().startsWith("Mozilla")), "UserAgent without escaped double quotes");
    assertNull(logEntry.getReferer(), "Referer is null");
  }

  @Test
  public void canParseLogEntryWithEmptyUserAgent() throws Exception {
    HttpdLogEntry logEntry = getEntry4();
    assertNull(logEntry.getUserAgent(), "UserAgent is null");
  }

  @Test
  public void canParseLogEntryWithUserAgentEscapedInDoubleQuotes() throws Exception {
    HttpdLogEntry logEntry = getEntry5();
    assertNotNull(logEntry.getUserAgent(), "has UserAgent");
    assertTrue((logEntry.getUserAgent().contains("AppleWebKit")), "UserAgent contains AppleWebKit");
    assertTrue((logEntry.getUserAgent().startsWith("Mozilla")), "UserAgent without escaped double quotes (start)");
    assertTrue((logEntry.getUserAgent().endsWith("36")), "UserAgent without escaped double quotes (end)");
  }

  @Test
  public void canParseLogEntryWithAdditionalDotfabri() throws Exception {
    HttpdLogEntry logEntry = getEntry6();
    assertNotNull(logEntry.getUserAgent(), "has UserAgent");
  }

  protected static HttpdLogEntry getEntry1() throws Exception {
    return new ApacheCombinedLogParser().parseLine(testEntry1);
  }

  protected static HttpdLogEntry getEntry2() throws Exception {
    return new ApacheCombinedLogParser().parseLine(testEntry2);
  }

  protected static HttpdLogEntry getEntry3() throws Exception {
    return new ApacheCombinedLogParser().parseLine(testEntry3);
  }

  protected static HttpdLogEntry getEntry4() throws Exception {
    return new ApacheCombinedLogParser().parseLine(testEntry4);
  }

  protected static HttpdLogEntry getEntry5() throws Exception {
    return new ApacheCombinedLogParser().parseLine(testEntry5);
  }

  protected static HttpdLogEntry getEntry6() throws Exception {
	return new ApacheCombinedLogParser().parseLine(testEntry6);
  }
}

/*
 * Copyright Ⓒ 2018 Fred Vos
 * Copyright Ⓒ 2018 Tilburg University
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.mokolo.commons.io.logback.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.mokolo.commons.io.logback.LayoutConfiguration;

public class LogbackAppenderConfigurationTest {

  @Test
  public void canGeneratePatternLayoutString() {
    LayoutConfiguration configuration = new LayoutConfiguration();
    assertEquals("%m%n", configuration.getPatternLayout(), "Only message");
    configuration.showLevel();
    assertEquals("%-5p - %m%n", configuration.getPatternLayout(), "With level");
    configuration.showClass();
    assertEquals("%-20.30c{1} %-5p - %m%n", configuration.getPatternLayout(), "With class");
  }

}

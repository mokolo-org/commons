/*
 * Copyright Ⓒ 2019 Fred Vos
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.mokolo.commons.io.conf.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.mokolo.commons.io.conf.Configuration;
import org.mokolo.commons.io.conf.Mode;

public class ConfigurationTest {

  @Test
  public void generateSingleExpandedKeyIfPrefixNorModeSet() {
    Configuration configuration = new Configuration();
    this.checkOne(configuration);
    configuration.setPrefix("");
    this.checkOne(configuration);
  }

  private void checkOne(Configuration configuration) {
    List<String> expandedKeys = configuration.generateKeys("base-url", Configuration.WITH_ALL_ONOFF);
    assertNotNull(expandedKeys, "Generated list");
    assertEquals(1, expandedKeys.size());
    assertEquals("base-url", expandedKeys.get(0));
  }
  
  @Test
  public void generateTwoExpandedKeysIfPrefixSet() {
    Configuration configuration = new Configuration();
    configuration.setPrefix("pf");
    this.checkTwoPF(configuration);
  }

  private void checkTwoPF(Configuration configuration) {
    List<String> expandedKeys = configuration.generateKeys("base-url", Configuration.WITH_ALL_ONOFF);
    assertNotNull(expandedKeys, "Generated list");
    assertEquals(2, expandedKeys.size());
    assertEquals("pf.base-url", expandedKeys.get(0));
    assertEquals("base-url", expandedKeys.get(1));
  }
  
  @Test
  public void generateTwoExpandedKeysIfModeSet() {
    Configuration configuration = new Configuration();
    configuration.setMode(Mode.PRODUCTION);
    this.checkTwoModeProduction(configuration);
  }

  private void checkTwoModeProduction(Configuration configuration) {
    List<String> expandedKeys = configuration.generateKeys("base-url", Configuration.WITH_ALL_ONOFF);
    assertNotNull(expandedKeys, "Generated list");
    assertEquals(2, expandedKeys.size());
    assertEquals("production.base-url", expandedKeys.get(0));
    assertEquals("base-url", expandedKeys.get(1));
  }
  
  @Test
  public void generateFourExpandedKeysIfPrefixAnsModeSet() {
    Configuration configuration = new Configuration();
    configuration.setPrefix("pf");
    configuration.setMode(Mode.PRODUCTION);
    this.checkFourProduction(configuration);
  }

  private void checkFourProduction(Configuration configuration) {
    List<String> expandedKeys = configuration.generateKeys("base-url", Configuration.WITH_ALL_ONOFF);
    assertNotNull(expandedKeys, "Generated list");
    assertEquals(4, expandedKeys.size());
    assertEquals("pf.production.base-url", expandedKeys.get(0));
    assertEquals("pf.base-url", expandedKeys.get(1));
    assertEquals("production.base-url", expandedKeys.get(2));
    assertEquals("base-url", expandedKeys.get(3));
  }
  
  @Test
  public void canFindRightValueBase() {
    Configuration configuration = getTestConfiguration();
    assertEquals("https://base.example.com", configuration.getString("base-url"), "None");
  }
  
  @Test
  public void canFindRightValueBaseModeTest() {
    Configuration configuration = getTestConfiguration();
    configuration.setMode(Mode.TEST);
    assertEquals("https://test.example.com", configuration.getString("base-url"), "No prefix Test");
  }
  
  @Test
  public void canFindRightValueBaseModeTestPrefix() {
    Configuration configuration = getTestConfiguration();
    configuration.setMode(Mode.TEST);
    configuration.setPrefix("pf");
    assertEquals("https://test.example.com", configuration.getString("base-url"), "Prefix Test");
  }
  
  @Test
  public void canFindRightValueBaseModeProductionPrefix() {
    Configuration configuration = getTestConfiguration();
    configuration.setMode(Mode.PRODUCTION);
    configuration.setPrefix("pf");
    assertEquals("https://www.example.com", configuration.getString("base-url"), "Prefix Production");
  }
  
  @Test
  public void canFindRightValueBaseModeTestSettingPrefix() {
    Configuration configuration = getTestConfiguration();
    configuration.setPrefix("pf");
    configuration.addString("pf.mode", "TEST");
    configuration.updateMode();
    assertEquals("https://test.example.com", configuration.getString("base-url"), "Prefix Test");
  }
  
  private static Configuration getTestConfiguration() {
    Configuration configuration = new Configuration();
    configuration.addString("base-url", "https://base.example.com");
    configuration.addString("test.base-url", "https://test.example.com");
    configuration.addString("pf.production.base-url", "https://www.example.com");
    return configuration;    
  }
  
}

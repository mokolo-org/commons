/*
 * Copyright Ⓒ 2019 Tilburg University
 * Copyright Ⓒ 2019 Fred Vos
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.mokolo.commons.io.httpdlog.tests;

import static org.junit.jupiter.api.Assertions.assertFalse;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.regex.Pattern;

import org.junit.jupiter.api.Test;
import org.mokolo.commons.io.httpdlog.HttpdLogEntry;
import org.mokolo.commons.io.httpdlog.HttpdLogEntryMatcher;
import org.mokolo.commons.io.httpdlog.HttpdlogEntryMatcherBuilder;
import org.mokolo.commons.io.HttpMethod;

public class HttpdLogEntryMatchertest {

  @Test
  public void canCheckIfMatches() throws Exception {
    HttpdLogEntry logEntry = ApacheCombinedLogParserTest.getEntry1();

    HttpdLogEntryMatcher matcher;
    
    matcher = new HttpdlogEntryMatcherBuilder()
        .build();
    assertTrue(matcher.matches(logEntry), "Matches empty matcher");    
    
    matcher = new HttpdlogEntryMatcherBuilder()
        .withInetAddress(Pattern.compile("^11\\.12\\.13\\.14$"))
        .build();
    assertTrue(matcher.matches(logEntry), "Matches IP-address");
    
    matcher = new HttpdlogEntryMatcherBuilder()
        .withInetAddress(Pattern.compile("^11\\.12\\.13\\.99$"))
        .build();
    assertFalse(matcher.matches(logEntry), "Does not match IP-address");
    
    matcher = new HttpdlogEntryMatcherBuilder()
        .withInetAddress(Pattern.compile("^11\\.12\\.13\\.14$"))
        .withInetAddress(Pattern.compile("^11\\.12\\.13\\.99$"))
        .build();
    assertTrue(matcher.matches(logEntry), "Matches any of two IP-addresses");

    matcher = new HttpdlogEntryMatcherBuilder()
        .withHttpMethod(HttpMethod.GET)
        .build();
    assertTrue(matcher.matches(logEntry), "Matches GET method");    
    
    matcher = new HttpdlogEntryMatcherBuilder()
        .withHttpMethod(HttpMethod.PUT)
        .build();
    assertFalse(matcher.matches(logEntry), "Does not match PUT method");    
    
    matcher = new HttpdlogEntryMatcherBuilder()
        .withPath(Pattern.compile("\\/products\\/"))
        .build();
    assertTrue(matcher.matches(logEntry), "Matches path");

    matcher = new HttpdlogEntryMatcherBuilder()
        .withPath(Pattern.compile("\\/studorp\\/"))
        .build();
    assertFalse(matcher.matches(logEntry), "Does not match path");

  }

}

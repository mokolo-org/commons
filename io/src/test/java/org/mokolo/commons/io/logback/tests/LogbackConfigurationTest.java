package org.mokolo.commons.io.logback.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Test;
import org.mokolo.commons.io.logback.LogbackConfiguration;
import org.mokolo.commons.lang.DateFormat;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class LogbackConfigurationTest {

  private static String TEST_MSG = "This is a test";
  
  @Test
  public void canAppendToFileAndRollingFile() throws Exception {
    LogbackConfiguration logb = new LogbackConfiguration();
    File logFile = File.createTempFile("logfile", ".log");
    // System.out.println("Logfile = "+logFile.getAbsolutePath());
    File rollingLogFile = File.createTempFile("rollinglogfile", ".log");
    // System.out.println("Rollinglogfile = "+rollingLogFile.getAbsolutePath());
    logb.reset();
    int logbOptions = LogbackConfiguration.WITH_DATETIMESTAMP;
    logbOptions += LogbackConfiguration.WITH_MILLISECONDS;
    
    logb.addFileAppender(
        LogbackConfiguration.Level.NORMAL,
        logbOptions,
        logFile.getAbsolutePath());
    logb.addRollingFileAppender(
          LogbackConfiguration.Level.NORMAL,
          logbOptions,
          rollingLogFile.getAbsolutePath()+".%d",
          10);
    
    logb.addLoggers(LogbackConfiguration.Level.NORMAL, "org.mokolo.commons");
    // logb.printSetup();

    log.info(TEST_MSG);
    
    checkLogFile(logFile.getAbsolutePath());
    checkLogFile(rollingLogFile.getAbsolutePath()+"."+getYyyyMMdd());
    // Can theoretically fail if test is done exactly at midnight
  }
  
  private static void checkLogFile(String path) throws IOException {
    File file = new File(path);
    assertTrue(file.exists());
    InputStream is = new FileInputStream(file);
    List<String> lines = IOUtils.readLines(is, Charset.forName("UTF-8"));
    assertEquals(1, lines.size());
    assertTrue(lines.get(0).contains(TEST_MSG));
  }
  
  private static String getYyyyMMdd() {
    Date date = new Date();
    return DateFormat.YYYY_MM_DD.getSimpleDateFormat().format(date);
  }
}

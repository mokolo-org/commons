/*
 * Copyright Ⓒ 2019 Fred Vos
 * Copyright Ⓒ 2019 Tilburg University
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.mokolo.commons.io.conf;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;

import lombok.Cleanup;
import lombok.extern.slf4j.Slf4j;

/**
 * Reads configuration files.
 * 
 * Loads files in this order if these exist:
 * <ul>
 *   <li>File /etc/${applicationName}.conf</li>
 *   <li>File .${applicationName}/$applicationName}.conf in user home directory</li>
 *   <li>File provided with -D${applicationName}.conf system property (must exist if provided)</li>
 *   <li>File ${applicationName}.conf in current directory</li>
 * </ul>
 * Values set in a file can be overridden in another file.
 */
@Slf4j
public class ConfigurationLoader {
  
  public static final int USE_CLASSPATH = 1;
  public static final int USE_ETC = 2;
  public static final int USE_USER_HOME = 4;
  public static final int USE_SYSTEM_PROPERTY = 8;
  public static final int USE_CURRENT_DIR = 16;
  public static final int ALLOW_INCLUDES = 32;
  public static final int USE_ALL = 31;

  public static Configuration getConfiguration(String applicationName) throws IOException {
    Configuration configuration = new Configuration();
    Properties properties = new Properties();
    properties.put("application-name", applicationName);
    loadConfiguration(properties, applicationName, USE_ALL); // Do not allow includes
    for (Map.Entry<Object, Object> entry : properties.entrySet())
      configuration.addString(entry.getKey().toString(), entry.getValue().toString());
    return configuration;
  }
  
  private static void loadConfiguration(Properties properties, String applicationName, int flags) throws IOException {
    if ((flags & USE_CLASSPATH) > 0)
      parseConfigurationFileFromClasspath(properties, applicationName);
    if ((flags & USE_ETC) > 0)
      parseConfigurationFileFromEtc(properties, applicationName);
    if ((flags & USE_USER_HOME) > 0)
      parseConfigurationFileFromUserHome(properties, applicationName);
    if ((flags & USE_SYSTEM_PROPERTY) > 0) 
      parseConfigurationFileFromSystemProperty(properties, applicationName);
    if ((flags & USE_CURRENT_DIR) > 0)
      parseConfigurationFileFromCurrentWorkingDirectory(properties, applicationName);
    if ((flags & ALLOW_INCLUDES) > 0) {
      String includes = properties.getProperty(applicationName+".include");
      if (includes != null && includes.trim().length() > 0) {
        for (String path : includes.trim().split("\\s+")) {
          log.debug("Include "+path);
          File file = new File(path);
          parseFile(properties, file, true);
        }
      }
    }
  }
  
  /**
   * Load file 'applicationName'.conf from classpath if it exists
   * 
   * Settings override settings if present.
   */
  private static void parseConfigurationFileFromClasspath(Properties properties, String applicationName) throws IOException {
    @Cleanup InputStream is = ConfigurationLoader.class.getResourceAsStream("/"+applicationName+".conf");
    if (is != null) {
      properties.load(is);
      log.info("Properties file loaded from classpath");
    }
  }
  
  private static void parseConfigurationFileFromEtc(Properties properties, String applicationName) throws IOException {
    File file = new File("/etc/"+applicationName+".conf");
    parseFile(properties, file, false);
  }
  
  private static void parseConfigurationFileFromUserHome(Properties properties, String applicationName) throws FileNotFoundException, IOException {
    File file = new File(
        System.getProperty("user.home")+File.separator+
        "."+applicationName+File.separator+
        applicationName+".conf");
    parseFile(properties, file, false);
  }

  private static void parseConfigurationFileFromSystemProperty(Properties properties, String applicationName) throws IOException {
    String conf = System.getProperty(applicationName+".conf");
    if (conf != null && conf.length() > 0) {
      File file = new File(conf);
      parseFile(properties, file, true);
    }
  }

  private static void parseConfigurationFileFromCurrentWorkingDirectory(Properties properties, String applicationName) throws IOException {
    File file = new File(applicationName+".conf");
    parseFile(properties, file, false);
  }
  
  public static void parseFile(Properties properties, File file, boolean mustExist) throws IOException {
    if (! file.exists() && mustExist == true)
      throw new FileNotFoundException("File '"+file.getAbsolutePath()+"' does not exist");
    if (file.exists()) {
      @Cleanup InputStream is = new FileInputStream(file);
      properties.load(is);
      log.info("Properties file '" + file.getAbsolutePath() + "' loaded");
    }
  }

}

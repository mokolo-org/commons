/*
 * Copyright Ⓒ 2019 Fred Vos
 * Copyright Ⓒ 2019 Tilburg University
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.mokolo.commons.io.httpdlog;

import java.net.InetAddress;
import java.util.Date;

import org.mokolo.commons.io.HttpMethod;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

public class HttpdLogEntryImpl implements HttpdLogEntry {
  @Getter @Setter(AccessLevel.PROTECTED) private InetAddress inetAddress;
  @Getter @Setter(AccessLevel.PROTECTED) private Date timestamp;
  @Getter @Setter(AccessLevel.PROTECTED) private HttpMethod httpMethod;
  @Getter @Setter(AccessLevel.PROTECTED) private String path;
  @Getter @Setter(AccessLevel.PROTECTED) private String protocol;
  @Getter @Setter(AccessLevel.PROTECTED) private Integer status;
  @Getter @Setter(AccessLevel.PROTECTED) private Integer size;
  @Getter @Setter(AccessLevel.PROTECTED) private String referer;
  @Getter @Setter(AccessLevel.PROTECTED) private String userAgent;
}

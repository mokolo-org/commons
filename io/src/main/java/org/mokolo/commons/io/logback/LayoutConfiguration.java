/*
 * Copyright Ⓒ 2018 Fred Vos
 * Copyright Ⓒ 2018 Tilburg University
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.mokolo.commons.io.logback;

public class LayoutConfiguration {
  private boolean doShowMilliseconds;
  private boolean doShowDateTimestamp;
  private boolean doShowClass;
  private boolean doShowLevel;

  private static String MILLISECONDS = "%10r";
  private static String DATETIMESTAMP = "%d{ISO8601}";
  private static String CLASS = "%-20.30c{1}"; // (*)
  private static String LEVEL = "%-5p";
  private static String MESSAGE = "%m";
  private static String SEPARATOR = "-";
  private static String NEWLINE = "%n";

  public LayoutConfiguration() {
    this.doShowMilliseconds = false;
    this.doShowDateTimestamp = false;
    this.doShowClass = false;
    this.doShowLevel = false;
  }

  public void showMilliseconds() { this.doShowMilliseconds = true; }
  public void showDateTimestamp() { this.doShowDateTimestamp = true; }
  public void showClass() { this.doShowClass = true; }
  public void showLevel() { this.doShowLevel = true; }

  public String getPatternLayout() {
    StringBuilder patternLayout = new StringBuilder();

    if (this.doShowMilliseconds == true)
      patternLayout.append(MILLISECONDS);

    if (this.doShowDateTimestamp == true) {
      addSpaceIf(patternLayout);
      patternLayout.append(DATETIMESTAMP);
    }

    if (this.doShowClass == true) {
      addSpaceIf(patternLayout);
      patternLayout.append(CLASS);
    }

    if (this.doShowLevel == true) {
      addSpaceIf(patternLayout);
      patternLayout.append(LEVEL);
    }

    addSeparatorIf(patternLayout);
    patternLayout.append(MESSAGE+NEWLINE);

    return patternLayout.toString();
  }

  private static void addSpaceIf(StringBuilder s) {
    if (s.length() > 0 && s.charAt(s.length()-1) != ' ')
      s.append(' ');
  }

  private static void addSeparatorIf(StringBuilder s) {
    if (s.length() > 0 && s.charAt(s.length()-1) != ' ') {
      s.append(' ');
      s.append(SEPARATOR);
      s.append(' ');
    }
  }
}

/*
 * Copyright Ⓒ 2019 Tilburg University
 * Copyright Ⓒ 2019 Fred Vos
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.mokolo.commons.io.httpdlog;

import java.net.InetAddress;

import java.net.UnknownHostException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.mokolo.commons.lang.DateFormat;
import org.mokolo.commons.io.HttpMethod;

public class ApacheCombinedLogParser implements HttpdLogParser {

  // Source: http://www.java2s.com/Code/Java/Development-Class/ParseanApachelogfilewithRegularExpressions.htm
  // Modified to match IPv6 addresses too
  private static final Pattern logEntryPattern = Pattern.compile(
      "^([0-9a-f:.]+) (\\S+) (\\S+) \\[([\\w:/]+\\s[+\\-]\\d{4})\\] \"(.+?)\" (\\d{3}) (\\d+) \"([^\"]*)\" \"(.*)\".*");
  //    (1)           (2)    (3)       (4)                            (5)     (6)      (7)      (8)          (9)
  //    InetAddress                    Timestamp                      Request Status   Size     Referer      UserAgent
  public static final int NUM_FIELDS = 9;

  private static final Pattern requestPattern2 = Pattern.compile("^([A-Z]+) (.*)$");
  //                                                               (1)      (2)
  //                                                               HttpMethod
  //                                                                        Path

  private static final Pattern requestPattern3 = Pattern.compile("^([A-Z]+) (.*) (HTTP/\\d\\.\\d)$");
  //                                                               (1)      (2)  (3)
  //                                                               HttpMethod
  //                                                                        Path Protocol
  
  @Override
  public HttpdLogEntry parseLine(String s) throws ParseException {
    Matcher logEntryMatcher = logEntryPattern.matcher(s);
    if (! logEntryMatcher.matches() || NUM_FIELDS != logEntryMatcher.groupCount())
      throw new ParseException("Bad log entry "+s, 0);
    HttpdLogEntryImpl logEntry = new HttpdLogEntryImpl();
    
    try {
      logEntry.setInetAddress(InetAddress.getByName(logEntryMatcher.group(1)));
    } catch (UnknownHostException e) {
      throw new ParseException(e.getMessage(), 0);
    }
    SimpleDateFormat timestampFormat = DateFormat.APACHE.getSimpleDateFormat(Locale.US);
    logEntry.setTimestamp(timestampFormat.parse(logEntryMatcher.group(4)));
    
    String requestString = logEntryMatcher.group(5);
    Map<String, String> request = parseRequest(requestString);
    if (request.containsKey("httpMethod"))
      logEntry.setHttpMethod(HttpMethod.valueOf(request.get("httpMethod")));
    if (request.containsKey("path"))
      logEntry.setPath(request.get("path"));
    if (request.containsKey("protocol"))
      logEntry.setProtocol(request.get("protocol"));

    logEntry.setStatus(Integer.valueOf(logEntryMatcher.group(6)));
    
    logEntry.setSize(Integer.valueOf(logEntryMatcher.group(7)));

    String referer = logEntryMatcher.group(8);
    if (! referer.isEmpty() && ! referer.equals("-"))
      logEntry.setReferer(referer);
    
    String userAgent = logEntryMatcher.group(9);
    if (! userAgent.isEmpty()) {
      logEntry.setUserAgent(unQuote(userAgent));
    }

    return logEntry;
  }
  
  public static Map<String, String> parseRequest(String requestString)
  throws ParseException {
    Map<String, String> map = new HashMap<>();
    
    if (requestString.equals("-")) // Empty request
      return map;

    Matcher requestMatcher = requestPattern3.matcher(requestString);
    if (! requestMatcher.matches()) {
      requestMatcher = requestPattern2.matcher(requestString);
      if (! requestMatcher.matches())
        throw new ParseException("Bad request pattern "+requestString, 0);
    }
      
    map.put("httpMethod", requestMatcher.group(1));
    map.put("path", requestMatcher.group(2));
    if (requestMatcher.groupCount() == 3)
      map.put("protocol", requestMatcher.group(3));

    return map;
  }
  
  private static String unQuote(String s) {
    if (s.startsWith("\\\"") && s.endsWith("\\\""))
      return s.substring(2, s.length()-2);
    else
      return s;
  }

}

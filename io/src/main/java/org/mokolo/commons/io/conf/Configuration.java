/*
 * Copyright Ⓒ 2019 Fred Vos
 * Copyright Ⓒ 2019 Tilburg University
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.mokolo.commons.io.conf;

import java.util.ArrayList;
import java.util.List;

import org.mokolo.commons.data.Document;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Configuration extends Document {
  private static final long serialVersionUID = 3095012011539078854L;

  @Setter private String prefix;
  @Getter @Setter private Mode mode;
  
  public static final int WITH_ALL_OFF = 0;
  public static final int WITH_PREFIX_ONOFF = 1;
  public static final int WITH_MODE_ONOFF = 2;
  public static final int WITH_ALL_ONOFF = 3;
  
  /**
   * If setting 'mode' found
   * 
   * Only checks prefix.mode and mode.
   */
  public void updateMode() {
    Mode oldMode = this.mode;
    String modeString = this.getString("mode", null, WITH_PREFIX_ONOFF);
    if (modeString != null) {
      this.mode = Mode.valueOf(modeString.toUpperCase());
      if (oldMode == null && this.mode != null ||
          oldMode != null && this.mode == null ||
          this.mode != oldMode)
        log.info("Mode = "+this.mode);
    }
  }
  
  @Override
  public String getString(String key) {
    return this.getString(key, null);
  }
  
  @Override
  public String getString(String key, String defaultValue) {
    return this.getString(key, defaultValue, WITH_ALL_ONOFF);
  }
  
  private String getString(String key, String defaultValue, int options) {
    KeyValue kv = this.getKeyValue(key, options);
    return kv != null ? kv.value : defaultValue;
  }
  
  /**
   * Returns real key and value or null if not found
   *  
   * @param key
   * @param options
   */
  public KeyValue getKeyValue(String key, int options) {
    KeyValue kv = null;
    for (String expandedKey : this.generateKeys(key, options)) {
      String foundValue = super.getStringValue(expandedKey, null);
      if (foundValue != null) {
        kv = new KeyValue();
        kv.key = expandedKey;
        kv.value = foundValue;
        break;
      }
    }
    return kv;
  }
  
  /**
   * Generates candidate keys from string
   * 
   * It adds the prefix and mode to the given key if set.
   * If prefix = 'pf' and mode = PRODUCTION, and all options ONOFF set,
   * key 'base-url' is returned as:
   * - 'pf.production.base-url'
   * - 'pf.base-url'
   * - 'production.base_url'
   * - 'base-url'
   */
  public List<String> generateKeys(String key, int options) {
    List<String> keys = new ArrayList<>();
    if ((options & WITH_PREFIX_ONOFF) > 0 && this.prefix != null && this.prefix.length() > 0) {
      if ((options & WITH_MODE_ONOFF) > 0 && this.mode != null)
        keys.add(this.prefix+"."+this.mode.toString().toLowerCase()+"."+key);
      keys.add(this.prefix+"."+key);
    }
    if ((options & WITH_MODE_ONOFF) > 0 && this.mode != null)
      keys.add(this.mode.toString().toLowerCase()+"."+key);
    keys.add(key);
    return keys;
  }
}

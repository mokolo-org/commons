/*
 * Copyright Ⓒ 2018 Fred Vos
 * Copyright Ⓒ 2018 Tilburg University
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.mokolo.commons.io.logback;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.filter.ThresholdFilter;
import ch.qos.logback.classic.net.SyslogAppender;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.Appender;
import ch.qos.logback.core.ConsoleAppender;
import ch.qos.logback.core.FileAppender;
import ch.qos.logback.core.filter.Filter;
import ch.qos.logback.core.rolling.RollingFileAppender;
import ch.qos.logback.core.rolling.TimeBasedRollingPolicy;

/**
 * Easy runtime setup of Logback configuration.
 *
 * <p>Sane defaults and conventions make it easy to setup
 * Logback runtime instead of using a .properties file.</p>
 *
 * <p>You can add loggers, ConsoleAppenders and FileAppenders.
 * Every appender is automatically attached to every logger.
 * The Level, as specified in this class, determines the
 * level used by the logger, the threshold level for an appender
 * and the layout of messages.</p>
 *
 * <p>Sample code:</p>
 * <pre>
    Log4jConfiguration log4j = new Log4jConfiguration();
    log4j.addConsoleAppender(Log4jConfiguration.Level.NORMAL, Log4jConfiguration.WITH_MILLISECONDS);
    log4j.addLogger("nl.uvt.commons", Log4jConfiguration.Level.QUIET);
    log4j.addLogger("nl.tilburguniversity.dodoco", Log4jConfiguration.Level.NORMAL);

    ...
    logger.info("Bla");

   </pre>
 * <p>results in <code>         0 INFO  - Bla</code></p>
 *
 * @author fvos
 */
  
public class LogbackConfiguration {

  public enum Level {
    NONE,
    QUIET,
    NORMAL,
    VERBOSE;
    
    public static Level parse(String s) {
      for (Level l : values()) {
        if (l.name().equals(s))
          return l;
      }
      return NORMAL;
    }
  }

  public static final int NONE = 0;
  public static final int WITH_DATETIMESTAMP = 1;
  public static final int WITH_MILLISECONDS = 2;

  private LoggerContext loggerContext;
  private Map<String, Logger> loggers;
  private Map<String, Appender<ILoggingEvent>> appenders;
  private Map<Level, PatternLayoutEncoder> patternLayouts;
  private int consoleAppenderCount;
  private int fileAppenderCount;
  private int syslogAppenderCount;

  public LogbackConfiguration() {
    this.loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
    this.setupThisObjectOrSetItUpAgain();
  }

  /**
   * Removes all loggers and appenders in Logback and resets this object
   */
  public void reset() {
    this.loggerContext.reset();
    this.setupThisObjectOrSetItUpAgain();
  }

  private void setupThisObjectOrSetItUpAgain() {
    this.loggers = new HashMap<>();
    this.appenders = new HashMap<>();
    this.patternLayouts = new HashMap<>();
    this.consoleAppenderCount = 0;
    this.fileAppenderCount = 0;
    this.syslogAppenderCount = 0;
  }
  
  /**
   * Adds Loggers
   *
   * @param names names of classes or packages
   * @param level the logging level expressed as NONE, QUIET, et cetera; not Logback Level!
   */
  public void addLoggers(Level level, String... names) {
    ch.qos.logback.classic.Level logbackLevel = level2level(level);
    for (String name : names) {

      Logger logger = (Logger) LoggerFactory.getLogger(name);
      logger.setLevel(logbackLevel);
      logger.setAdditive(false); /* set to true if root should log too */

      // Add all appenders:
      for (Appender<ILoggingEvent> appender : this.appenders.values())
        logger.addAppender(appender);

      // Store:
      this.loggers.put(name, logger);
    }
  }
  
  /**
   * Adds ConsoleAppender that writes to STDOUT.
   *
   * <p>Appender is automatically attached to already added loggers.</p>
s   *
   * @param level the logging level expressed as NONE, QUIET, et cetera; not Logback Level!
   * @param extras additional fields in output
   * 
   * @return name of appender
   */
  public String addConsoleAppender(Level level, int extras) {
    ConsoleAppender<ILoggingEvent> appender = new ConsoleAppender<ILoggingEvent>();
    this.consoleAppenderCount++;
    String name = "Stdout"+this.consoleAppenderCount;
    appender.setName(name);
    appender.setTarget("System.err");
    setAppenderLevel(appender, level);
    appender.setContext(this.loggerContext);
    appender.setEncoder(this.getPatternLayoutEncoder(level, extras));
    appender.start();
    // Add to all loggers:
    for (Logger logger : this.loggers.values())
      logger.addAppender(appender);
    this.appenders.put(name, appender);
    return name;
  }

  /**
   * Adds FileAppender that appends to an existing file if it already exists
   * and uses buffered IO.
   *
   * <p>Appender is automatically attached to already added loggers.</p>
   *
   * @param level the logging level expressed as NONE, QUIET, et cetera; not Logback Level!
   * @param extras additional fields in output
   * @param fileName setting for the appender
   *
   * @return name of appender
   */
  public String addFileAppender(
      Level level,
      int extras,
      String fileName) {
    FileAppender<ILoggingEvent> appender = new FileAppender<ILoggingEvent>();
    this.fileAppenderCount++;
    String name = "File"+this.fileAppenderCount;
    appender.setName(name);
    appender.setFile(fileName);
    setAppenderLevel(appender, level);
    appender.setContext(this.loggerContext);
    appender.setEncoder(this.getPatternLayoutEncoder(level, extras));
    appender.start();
    // Add to all loggers:
    for (Logger logger : this.loggers.values())
      logger.addAppender(appender);
    this.appenders.put(name, appender);
    return name;
  }
  
  /**
   * Adds RollingFileAppender that appends to an existing file if it already exists
   * and uses buffered IO.
   *
   * <p>Appender is automatically attached to already added loggers.</p>
   *
   * @param level the logging level expressed as NONE, QUIET, et cetera; not Logback Level!
   * @param extras additional fields in output
   * @param fileName setting for the appender
   * @param fileNamePattern path to the logfile - must include date/time,
   *        like %d for yyyy-MM-dd
   *
   * @return name of appender
   */
  public String addRollingFileAppender(
      Level level,
      int extras,
      String fileNamePattern,
      int maxHistory) {
    RollingFileAppender<ILoggingEvent> appender = new RollingFileAppender<ILoggingEvent>();
    this.fileAppenderCount++;
    String name = "File"+this.fileAppenderCount;
    appender.setName(name);
    TimeBasedRollingPolicy<ILoggingEvent> rollingPolicy = new TimeBasedRollingPolicy<ILoggingEvent>();
    rollingPolicy.setParent(appender);
    rollingPolicy.setContext(this.loggerContext);
    rollingPolicy.setFileNamePattern(fileNamePattern);
    rollingPolicy.setMaxHistory(maxHistory);
    rollingPolicy.start();
    appender.setRollingPolicy(rollingPolicy);
    setAppenderLevel(appender, level);
    appender.setContext(this.loggerContext);
    appender.setEncoder(this.getPatternLayoutEncoder(level, extras));
    appender.start();
    // Add to all loggers:
    for (Logger logger : this.loggers.values())
      logger.addAppender(appender);
    this.appenders.put(name, appender);
    return name;
  }
  
  /**
   * Adds SyslogAppender that appends to syslog
   *
   * <p>Appender is automatically attached to already added loggers.</p>
   *
   * @param level the logging level expressed as NONE, QUIET, et cetera; not Logback Level!
   * @param syslogHost see SyslogAppender for options
   *
   * @return name of appender
   */
  public String addSyslogAppender(Level level, String syslogHost) {
    SyslogAppender appender = new SyslogAppender();
    this.syslogAppenderCount++;
    String name = "Syslog"+this.syslogAppenderCount;
    appender.setName(name);
    appender.setSyslogHost(syslogHost);
    appender.setFacility("DAEMON");
    setAppenderLevel(appender, level);
    appender.setContext(this.loggerContext);
    appender.start();
    // Add to all loggers:
    for (Logger logger : this.loggers.values())
      logger.addAppender(appender);
    this.appenders.put(name, appender);
    return name;
  }
  
  public void printSetup() {
    System.out.println("Logback setup");
    System.out.println("");
    
    System.out.println("Appenders:");
    for (Map.Entry<String, Appender<ILoggingEvent>> entry : this.appenders.entrySet())
      System.out.println("- "+entry.getKey()+" : "+entry.getValue().getClass().getName());
    System.out.println("");
    
    System.out.println("Loggers + appenders:");
    for (Map.Entry<String, Logger> entry : this.loggers.entrySet()) {
      System.out.println("- "+entry.getKey());
      for (String appenderName : this.appenders.keySet()) {
        System.out.print("  - "+appenderName+" : ");
        Appender<ILoggingEvent> appender = entry.getValue().getAppender(appenderName);
        if (appender == null)
          System.out.println("NOT FOUND");
        else
          System.out.println(appender.getClass().getName());
      }
    }
  }
  
  private static void setAppenderLevel(Appender<ILoggingEvent> appender, Level level) {
    List<Filter<ILoggingEvent>>filters = appender.getCopyOfAttachedFiltersList();
    for (Filter<ILoggingEvent> filter : filters) {
      if (filter instanceof ThresholdFilter)
        ((ThresholdFilter) filter).setLevel(level2level(level).toString());
    }
  }
  
  private PatternLayoutEncoder getPatternLayoutEncoder(Level level, int extras) {
    PatternLayoutEncoder encoder = this.patternLayouts.get(level);
    if (encoder == null) {
      encoder = new PatternLayoutEncoder();
      encoder.setPattern(getAppenderLayout(level, extras));
      encoder.setContext(this.loggerContext);
      encoder.start();
      this.patternLayouts.put(level, encoder);
    }
    return encoder;
  }
  
  private static ch.qos.logback.classic.Level level2level(Level level) {
    switch (level) {
      case NONE :   return ch.qos.logback.classic.Level.OFF;
      case QUIET :  return ch.qos.logback.classic.Level.WARN;
      case NORMAL : return ch.qos.logback.classic.Level.INFO;
      default:      return ch.qos.logback.classic.Level.DEBUG;
    }
  }

  private static String getAppenderLayout(Level level, int extras) {
    LayoutConfiguration configuration = new LayoutConfiguration();
    if (level == Level.NORMAL) {
      configuration.showLevel();
    }
    else if (level == Level.VERBOSE) {
      configuration.showLevel();
      configuration.showClass();
    }
    if ((extras & WITH_DATETIMESTAMP) != 0) {
      configuration.showDateTimestamp();
    }
    if ((extras & WITH_MILLISECONDS) != 0) {
      configuration.showMilliseconds();
    }
    return configuration.getPatternLayout();
  }

}

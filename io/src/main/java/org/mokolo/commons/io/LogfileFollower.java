/*
 * Copyright Ⓒ 2019 Fred Vos
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.mokolo.commons.io;

import java.io.*;
import java.util.*;

/**
 * Reads logfile and returns new lines of text when asked.
 *
 */
public class LogfileFollower {
  private File logfile;
  private long lastPosition;
  
  public LogfileFollower(File logfile) {
    this.logfile = logfile;
    this.lastPosition = 0;
  }
  
  public List<String> newLines()
  throws IOException, FileNotFoundException {
    List<String> lines = new ArrayList<String>();
    long length = this.logfile.length();
    if (length > this.lastPosition) {
      RandomAccessFile raf = new RandomAccessFile(this.logfile, "r");
      raf.seek(this.lastPosition);
      String line = null;
      while ((line = raf.readLine()) != null) {
        // Check last char:
        raf.seek(raf.getFilePointer()-1);
        Byte b = raf.readByte();
        if (b == 0x0d || b == 0x0a) {
          // Read did not stop because of EOF:
          lines.add(line);
          this.lastPosition = raf.getFilePointer();
        }
      }
      raf.close();
    }

    return lines;
  }

}

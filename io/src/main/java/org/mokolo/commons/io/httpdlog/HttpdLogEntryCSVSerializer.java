/*
 * Copyright Ⓒ 2019 Fred Vos
 * Copyright Ⓒ 2019 Tilburg University
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.mokolo.commons.io.httpdlog;

import java.io.IOException;

import java.io.OutputStream;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.mokolo.commons.lang.DateFormat;

import lombok.Getter;

public class HttpdLogEntryCSVSerializer implements HttpdLogEntrySerializer {

  public enum Column {
    INET_ADDRESS("InetAddress"),
    TIMESTAMP("Timestamp"),
    HTTP_METHOD("HttpMethod"),
    PATH("Path"),
    PROTOCOL("Protocol"),
    STATUS("Status"),
    SIZE("Size"),
    REFERER("Referer"),
    USER_AGENT("UserAgent");

    @Getter private String name;
    private Column(String name) { this.name = name; }
    public static Column fromName(String name) {
      for (Column column : Column.values())
        if (column.name.equalsIgnoreCase(name))
          return column;
      return null;
    }
  }

  public enum CsvFormat {
    DEFAULT(CSVFormat.DEFAULT),
    EXCEL(CSVFormat.EXCEL);

    @Getter private CSVFormat format;
    private CsvFormat(CSVFormat format) { this.format = format; }
  }

  private OutputStream outputStream;
  private CSVPrinter printer;
  private List<Column> columns;
  private CSVFormat format = CSVFormat.DEFAULT;
  private SimpleDateFormat dateFormat;

  public HttpdLogEntryCSVSerializer(OutputStream outputStream) {
    this.dateFormat = DateFormat.DEFAULT.getSimpleDateFormat(Locale.US);
    this.outputStream = outputStream;
    this.columns = new ArrayList<>();
  }

  public void selectAll() {
    this.columns.clear();
    try {
      this.selectColumn(Column.INET_ADDRESS);
      this.selectColumn(Column.TIMESTAMP);
      this.selectColumn(Column.HTTP_METHOD);
      this.selectColumn(Column.PATH);
      this.selectColumn(Column.PROTOCOL);
      this.selectColumn(Column.STATUS);
      this.selectColumn(Column.SIZE);
      this.selectColumn(Column.REFERER);
      this.selectColumn(Column.USER_AGENT);
    } catch (AlreadyExistsException e) { }
  }

  public void selectColumn(Column column) throws AlreadyExistsException {
    if (this.columns.contains(column))
      throw new AlreadyExistsException("Column "+column+" already added");
    this.columns.add(column);
  }

  public void setTimestampFormat(DateFormat format) {
    this.dateFormat = format.getSimpleDateFormat(Locale.US);
  }
  
  public void setTimeZone(TimeZone timeZone) {
    this.dateFormat.setTimeZone(timeZone);
  }

  public void setCsvFormat(CsvFormat format) {
    this.format = format.getFormat();
  }

  @Override
  public void open() throws IOException {
    List<String> headers = this.getHeaders();
    PrintWriter writer = new PrintWriter(this.outputStream);
    this.printer = new CSVPrinter(
        writer,
        this.format
        .withHeader(headers.toArray(new String[headers.size()])));
  }

  @Override
  public void printHeader() throws IOException {
  }

  @Override
  public void printLogEntry(HttpdLogEntry logEntry) throws IOException {
    List<Object> values = this.getValues(logEntry);
    this.printer.printRecord(values.toArray(new Object[values.size()]));
  }

  @Override
  public void printFooter() throws IOException {
  }

  @Override
  public void close() throws IOException {
    this.printer.close();
  }

  private List<String> getHeaders() {
    List<String> headers = new ArrayList<>();
    for (Column column : this.columns)
      headers.add(column.getName());
    return headers;
  }

  private List<Object> getValues(HttpdLogEntry logEntry) {
    List<Object> values = new ArrayList<>();
    for (Column column : this.columns) {
      if (column == Column.INET_ADDRESS)
        values.add(logEntry.getInetAddress().getHostAddress());
      if (column == Column.TIMESTAMP)
        if (this.dateFormat != null)
          values.add(this.dateFormat.format(logEntry.getTimestamp()));
        else
          values.add(logEntry.getTimestamp());
      if (column == Column.HTTP_METHOD)
        values.add(logEntry.getHttpMethod());
      if (column == Column.PATH)
        values.add(logEntry.getPath());
      if (column == Column.PROTOCOL)
        values.add(logEntry.getProtocol());
      if (column == Column.STATUS)
        values.add(logEntry.getStatus());
      if (column == Column.SIZE)
        values.add(logEntry.getSize());
      if (column == Column.REFERER)
        values.add(logEntry.getReferer());
      if (column == Column.USER_AGENT)
        values.add(logEntry.getUserAgent());
    }
    return values;
  }
}

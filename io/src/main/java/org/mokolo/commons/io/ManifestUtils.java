/*
 * Copyright Ⓒ 2019 Tilburg University
 * Copyright Ⓒ 2019 Fred Vos
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.mokolo.commons.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.jar.JarFile;
import java.util.jar.Manifest;

import lombok.Cleanup;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ManifestUtils {

  public static final String[] FIELDS_SUMMARY = {
      "Implementation-Title",
      "Implementation-Vendor",
      "Implementation-Version",
      "Built-By",
      "Created-By",
      "Build-Jdk",
      "Build-Time"
  };

  /**
   * Finds and loads first WEB-INF/MANIFEST.MF it can find in series of paths and loads it
   * 
   * @param paths List of directories or paths to jars containing WEB-INF/MANIFEST.MF separated by path separator
   * @return found Manifest of null if not found 
   */
  public static Manifest getManifest(String paths) throws IOException {
    Manifest manifest = null;
    try {
      String[] pathStrings = paths.split(File.pathSeparator);
      for (String path : pathStrings) {
        if (path.endsWith(".jar") || path.endsWith(".war")) {
          // Try this jar or war file:
          File file = new File(path);
          if (file.exists()) {
            @Cleanup JarFile jarFile = new JarFile(file);
            manifest = jarFile.getManifest();
            break; // Found it. Stop.
          }
        }
        else {
          // Try this directory:
          File directory = new File(path);
          File file = new File(directory, "META-INF/MANIFEST.MF".replaceAll("/", File.separator));
          if (file.exists()) {
            @Cleanup InputStream is = new FileInputStream(file);
            manifest = new Manifest();
            manifest.read(is);
            break; // Found it. Stop.
          }
        }
      }
    } catch (IOException e) {
      log.error(e.getClass()+" occurred. Msg = "+e.getMessage());
    }
    return manifest;
  }

  public static void printManifest(Manifest manifest) throws IOException {
    if (manifest == null)
      log.error("No Manifest found");
    
    for (String fieldName : FIELDS_SUMMARY) {
      String value = manifest.getMainAttributes().getValue(fieldName);
      if (value != null) {
        System.out.println(String.format("%-22s : %s", fieldName, value));
      }
    }
  }


}

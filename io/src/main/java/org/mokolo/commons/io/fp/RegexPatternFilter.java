/*
 * Copyright Ⓒ 2019 Fred Vos
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.mokolo.commons.io.fp;

import java.io.File;

public class RegexPatternFilter  implements java.io.FilenameFilter {

  private java.util.regex.Pattern pattern;

  public RegexPatternFilter(String regex) {
    this.pattern = java.util.regex.Pattern.compile(regex);
  }

  public boolean accept(File dir, String name) {
    return this.pattern.matcher(name).matches();
  }

}

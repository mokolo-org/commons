/*
 * Copyright Ⓒ 2019 Fred Vos
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.mokolo.commons.io.fp;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.TreeSet;

/**
 * Processes files in a directory.
 * 
 * <p>Can be used like find &lt;dir&gt; -name &lt;pattern&gt; | xargs &lt;command&gt;
 * in the Linux/Unix world.</p>
 *
 */
public abstract class FileProcessor {
  
  private File baseDirectory;
  private String filePattern;
  private int level;
  
  public FileProcessor() {
    this.level = 0;
  }

  public FileProcessor(File baseDirectory) {
    this();
    this.setBaseDirectory(baseDirectory);
  }
  
  public void setFilePattern(String filePattern) {
    this.filePattern = filePattern;
  }
  
  public void process(Properties properties) throws InterruptedException {
    this.beforeProcess();
    this.processDirectory(baseDirectory, properties);
    this.afterProcess();
  }
  
  public void processDirectory(File directory, Properties properties) throws InterruptedException {
    // Create sorted set of all files (matching pattern) and directories
    TreeSet<File> files = new TreeSet<>();
    if (this.filePattern != null) {
      for (File file : asList(directory.listFiles(new RegexPatternFilter(this.filePattern))))
        if (file.isFile())
          files.add(file);
      for (File file : asList(directory.listFiles()))
        if (file.isDirectory())
          files.add(file);
    }
    else
      for (File file : asList(directory.listFiles()))
        if (file.isFile() || file.isDirectory())
          files.add(file);
    
    // Now process:
    for (File file : files) {
      if (file.isFile())
        processFile(file, properties);
      else {
        this.level++;
        processDirectory(file, properties);
        this.level--;
      }
    }
    this.afterDirectory();
  }
  
  private static List<File> asList(File[] files) {
    List<File> list = new ArrayList<>();
    if (files != null)
      for (File file : files)
        list.add(file);
    return list;
  }
  
  public abstract void processFile(File file, Properties properties) throws InterruptedException;

  public void setBaseDirectory(File baseDirectory) {
    this.baseDirectory = baseDirectory;
  }

  public File getBaseDirectory() {
    return this.baseDirectory;
  }
  
  public int getLevel() { return this.level; }
  
  // Hooks:
  public void beforeProcess() throws InterruptedException { }
  public void afterProcess() throws InterruptedException { }
  public void afterDirectory() throws InterruptedException { }
}

/*
 * Copyright Ⓒ 2018 Fred Vos
 * Copyright Ⓒ 2018 Tilburg University
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.mokolo.commons.cli;

import java.util.*;

import org.mokolo.commons.data.Document;

import joptsimple.OptionParser;
import joptsimple.OptionSet;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CommandParser {

  @Getter private OptionParser optionParser;
  private String commandKey;
  private List<CommandDefinition> commandDefinitions;
  private Map<String, CommandDefinition> commandsMap;
  private Set<String> argumentsSet;

  public CommandParser(OptionParser optionParser, String commandKey) {
    this.optionParser = optionParser;
    this.commandKey = commandKey;
    this.commandDefinitions = new ArrayList<CommandDefinition>();
    this.commandsMap = new HashMap<String, CommandDefinition>();
    this.argumentsSet = new HashSet<String>();
  }

  public void addCommandDefinition(CommandDefinition commandDefinition) {
    this.commandDefinitions.add(commandDefinition);
    this.commandsMap.put(commandDefinition.longCommand, commandDefinition);
    if (commandDefinition.shortCommand != null)
      this.commandsMap.put(commandDefinition.shortCommand, commandDefinition);
    for (String argument : commandDefinition.requiredArguments) {
      if (this.argumentsSet.add(argument))
        this.optionParser.accepts(argument).withRequiredArg();
    }
    for (String argument : commandDefinition.optionalArguments)
      if (this.argumentsSet.add(argument))
        this.optionParser.accepts(argument).withRequiredArg();
  }

  /**
   * Parses commands like 'command [arg1 [arg2 ... [argn]]]'
   * and assigns argument values to argument names in definition.
   * 
   * <p>Command is assigned to commandKey.</p>
   * 
   * @param args
   * @return document containing command and argument names and values
   * @throws ParsingException if no argument given, command not known
   *         or length of list of arguments not equal to number of arguments
   *         in definition 
   */
  public Document parseCommandNoSwitches(String[] args)
      throws ParsingException {
    Document document = new Document();
    if (args.length == 0)
      throw new ParsingException("No command");
    CommandDefinition commandDefinition = this.commandsMap.get(args[0]);
    if (commandDefinition == null)
      throw new ParsingException("Unknown command '"+args[0]+"'");
    String commandString = commandDefinition.longCommand;
    log.debug("Command string = '"+commandString+"'");
    int req = commandDefinition.requiredArguments.size();
    int opt = commandDefinition.optionalArguments.size();
    if (args.length < req+1 ||
        args.length > req+opt+1)
      throw new ParsingException(
          "Command '"+commandDefinition.longCommand+"' has "+
              req+" required arguments and "+
              opt+" optional arguments");
    document.addString(this.commandKey, commandString);
    for (int i=0; i<req; i++)
      document.addString(commandDefinition.requiredArguments.get(i), args[i+1]);
    for (int i=req+1; i<args.length; i++)
      document.addString(commandDefinition.optionalArguments.get(i-req-1), args[i]);

    return document;
  }

  /**
   * Parses commands like 'command --foo=5 --bar=yellow'
   * and assigns argument values to argument names in definition.
   * 
   * <p>Command is assigned to commandKey. It ignores arguments
   * that are not allowed for any command.</p>
   * 
   * @param args
   * @return document containing command and argument names and values
   * @throws ParsingException if no argument given, command not known
   *         or length of list of arguments not equal to number of arguments
   *         in definition 
   */
  public Document parseCommandWithSwitches(OptionSet options)
      throws ParsingException {
    if (options.nonOptionArguments().isEmpty())
      throw new ParsingException("No command");
    if (options.nonOptionArguments().size() > 1)
      throw new ParsingException("Only one command allowed");
    CommandDefinition commandDefinition = this.commandsMap.get(options.nonOptionArguments().get(0)); 
    if (commandDefinition == null)
      throw new ParsingException("Unknown command '"+options.nonOptionArguments().get(0)+"'");
    String commandString = commandDefinition.longCommand;
    log.debug("Command string = '"+commandString+"'");
    Document document = new Document();
    document.addString(this.commandKey, commandString);
    Iterator<String> it = this.argumentsSet.iterator();
    while (it.hasNext()) {
      String argument = it.next();
      if (options.has(argument)) {
        List<?> values = options.valuesOf(argument);
        for (Object value : values)
          document.addString(argument, (String) value);
      }
    }
    return document;
  }

  /**
   * Verifies if document is valid.
   * 
   * <p>Applies the following tests to the document:</p>
   * <ol>
   *   <li>Command provided</li>
   *   <li>Known command</li>
   *   <li>All required arguments provided</li>
   *   <li>All provided arguments allowed</li>
   * </ol> 
   * @param document
   * @throws ParsingException if document does not validate
   */
  public void validateDocument(Document document)
      throws ParsingException {
    String commandString = document.getString(this.commandKey);
    if (commandString == null)
      throw new ParsingException("No command");
    CommandDefinition commandDefinition = this.commandsMap.get(commandString);
    if (commandDefinition == null)
      throw new ParsingException("Unknown command '"+commandString+"'");
    for (String argumentName : commandDefinition.requiredArguments)
      if (document.getString(argumentName) == null)
        throw new ParsingException("Missing required argument '"+argumentName+"'");
    for (String argumentName : document.keySet())
      if (! argumentName.equals(this.commandKey) &&
          ! (commandDefinition.requiredArguments.contains(argumentName) ||
              commandDefinition.optionalArguments.contains(argumentName)))
        throw new ParsingException("Argument '"+argumentName+"' not allowed");
  }

  public void printUsage() {
    System.out.println("Usage:");
    System.out.println("java -jar <program-name> <command> [arg1 [arg2 ...]]");
    System.out.println("where <command> is one of");
    for (CommandDefinition command : this.commandDefinitions) {
      System.out.println(String.format("  %s (%s)", command.longCommand, command.shortCommand));
    }
  }

}

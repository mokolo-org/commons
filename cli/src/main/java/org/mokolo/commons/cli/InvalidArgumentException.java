package org.mokolo.commons.cli;

public class InvalidArgumentException extends Exception {
  private static final long serialVersionUID = -8198511165023251457L;

  public InvalidArgumentException(String msg) {
    super(msg);
  }
}

# Commons

Collection of libraries used in other Java projects.

## Libraries

### parent

Only POM with general settings and properties.
BOM for artifact versions.

### cli

Helper classes for parsing command line parameters with JOpt Simple and validating command line parameters.

### data

- Wrappers for data types and data structures.
- JAXB support for serializing to XML and deserializing from XML.
- Converters for generating YAML.
- Conversion to Freemarker template model.
- Support for Freemarker.
- The Document class (``Map<String, List<String>>``), used in different modules.

### http

HTTP server for servlets plus builder.

### io

- Classes for easy LogBack setup
- Support for reading and parsing configuration
- Support classes for http access to remote systems
- Traverse directory trees
- Parse and filter Httpd logfiles and serialize as CSV

### jaxrs

- @Logged annotation to log requests in LogBack

### lang

- DateFormat support class.

### pckg

- Classes for packaging projects, used bij jpackager/pommade.
- [Versioning](pckg/src/mail/resources/versions.md)

### sql

Support for generating DDL scripts.

### system

Support classes for the Quartz schedular.

### text

- Differ class that reports differences between two texts.
- TextBuffer class.
- Generating markup documents (Markdown, reStructuredtext) from data structures in text module. 

### ws

Support for calling external web services via HTTP.

### xml

XML support classes.


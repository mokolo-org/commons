package org.mokolo.commons.cmd;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;

/**
 * (1) Run command, (2) Handle Output
 *
 */
@Slf4j
public class CommandRunner {

  public enum LogLevel { QUIET, NORMAL, VERBOSE }

  public static Output runCommand(String command, File workingDirectory)
  throws IOException {
    List<String> commandList = new ArrayList<>();
    commandList.add("/bin/sh");
    commandList.add("-c");
    commandList.add(command);
    return runCommand(commandList, workingDirectory);
  }
  
  public static Output runCommand(List<String> command, File workingDirectory)
  throws IOException {
    ProcessBuilder processBuilder = new ProcessBuilder();
    processBuilder.directory(workingDirectory);
    processBuilder.command(command);
    return startProcessBuilder(processBuilder);
  }
  
  public static Output runCommand(String command)
  throws IOException {
    List<String> commandList = new ArrayList<>();
    commandList.add("/bin/sh");
    commandList.add("-c");
    commandList.add(command);
    return runCommand(commandList);
  }
  
  public static Output runCommand(List<String> command)
  throws IOException {
    ProcessBuilder processBuilder = new ProcessBuilder();
    processBuilder.command(command);
    return startProcessBuilder(processBuilder);
  }
  
  public static Output startProcessBuilder(ProcessBuilder processBuilder)
  throws IOException {
    Process process = processBuilder.start();

    StringBuffer outBuf = new StringBuffer();
    StringBuffer errBuf = new StringBuffer();
    
    try {
      BufferedReader stdInput = new BufferedReader(new 
          InputStreamReader(process.getInputStream()));
      BufferedReader stdError = new BufferedReader(new 
          InputStreamReader(process.getErrorStream()));

      String line;
      while ((line = stdInput.readLine()) != null)
        outBuf.append(line+"\n");
      while ((line = stdError.readLine()) != null) {
        if (line.trim().length() > 0)
          errBuf.append(line+"\n");
      }
      stdInput.close();
      stdError.close();

      int exitVal = process.waitFor();
      if (exitVal != 0)
        errBuf.append("Process exited with value "+exitVal+"\n");
    } catch (InterruptedException e) {
      errBuf.append("InterruptedException occurred while running command. Msg="+e.getMessage());
    } catch (IOException e) {
      errBuf.append("IOException occurred while running command. Msg="+e.getMessage());
    }
    Output output = new Output();
    output.out = outBuf.toString();
    output.err = errBuf.toString();
    return output;
  }
  
  /**
   * Turns Output into list of strings
   * 
   * If a line from stderr contains a string listed in ignoredErrors,
   * it's output is not logged as an error.
   */
  public static List<String> outputToListOfStrings(Output output, String[] ignoredErrors) {
    List<String> list = new ArrayList<>();
    String[] lines = output.err.split("\\n");
    for (String line : lines) {
      if (line.length() > 0) {
        boolean mustIgnore = false;
        for (String s : ignoredErrors) {
          if (line.contains(s)) {
            mustIgnore = true;
            break;
          }
        }
        if (! mustIgnore)
          log.error(line);
      }
    }
    lines = output.out.split("\\n");
    for (String line : lines)
      list.add(line);
    return list;
  }

  public static void logOutput(Output output, LogLevel level) {
    String[] lines = output.out.split("\\n");
    for (String line : lines)
      if (level != LogLevel.QUIET)
        log.info(line);

    lines = output.err.split("\\n");
    for (String line : lines)
      if (line.length() > 0)
        log.error(line);
  }

}

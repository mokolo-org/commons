package org.mokolo.commons.text;

public class TextBuffer {
  private StringBuffer buf;
  
  public TextBuffer() {
    this.buf = new StringBuffer();
  }
  
  public void appendBlock(String s) {
    this.appendString(true, 0, s, false);
  }
  
  public void appendString(boolean afterEmptyLine, int indentation, String s, boolean endingWithNewLine) {
    if (afterEmptyLine == true && ! this.isEmptyOrHasEmptyLineAtEnd())
      this.appendNewLine();
    if (indentation > 0)
      this.buf.append(" ".repeat(indentation));
    this.buf.append(s);
    if (endingWithNewLine == true)
      this.appendNewLine();
  }
  
  public void appendSpace() {
    this.buf.append(" ");
  }
  
  public void appendNewLine() {
    this.buf.append("\n");
  }
  
  private boolean isEmptyOrHasEmptyLineAtEnd() {
    if (this.buf.length() == 0)
      return true;
    if (this.buf.charAt(buf.length()-2) == '\n' &&
        this.buf.charAt(buf.length()-1) == '\n')
      return true;
    return false;
  }
  
  @Override
  public String toString() {
    return this.buf.toString();
  }
}

package org.mokolo.commons.text;

import org.mokolo.commons.data.DList;
import org.mokolo.commons.data.DMap;
import org.mokolo.commons.data.DObject;
import org.mokolo.commons.data.DParagraph;
import org.mokolo.commons.data.DSection;
import org.mokolo.commons.data.DTable;
import org.mokolo.commons.data.Processable;

/**
 * Transforms content structured as D* objects into MarkupText
 * using a markup API, i.e. a transformer that helps to
 * generate MarkDown or reStructuredText.
 */
public class MarkupTextGenerator {

  public enum Level {
    NONE(0), H1(1), H2(2), H3(3), H4(4), H5(5), H6(6);

    public final int value;
    static public final Level[] values = values();

    public Level next() {
      return values[(ordinal() + 1) % values.length];
    }

    private Level(int value) {
      this.value = value;
    }
  }
  
  public static MarkupText generate(
      FrontMatter frontMatter,
      DSection mainSection,
      MarkupAPI api) {
    MarkupText markupText = new MarkupText();
    markupText.setMarkupFormat(api.getMarkupFormat());
    markupText.setFrontMatter(frontMatter);
    StringBuffer buf = new StringBuffer();
    Level startLevel = Level.H1;
    if (api.increaseContentStartLevelByOneIfFrontMatter())
      startLevel = startLevel.next();
    buf.append(processSection(startLevel, mainSection, api));
    markupText.setText(buf.toString());
    return markupText;
  }

  private static String processSection(
      Level level,
      DSection section,
      MarkupAPI api) {
    TextBuffer buf = new TextBuffer();
    if (section.getTitle() != null) {
      buf.appendBlock(api.titleToString(level, section.getTitle()));
    }

    for (DObject o : section.getList()) {
      Processable proc = Processable.valueOf(o.getClass().getSimpleName().toUpperCase());

      switch (proc) {
      case DSECTION: // DSection IS A DList
        buf.appendBlock(processSection(level.next(), (DSection)o, api));
        break;
      case DLIST:
        buf.appendBlock(api.listToString((DList)o));
        break;
      case DMAP:
        buf.appendBlock(api.mapToString((DMap)o));
        break;
      case DPARAGRAPH: // DParagraph IS A DString
        buf.appendBlock(api.paragraphToString((DParagraph)o));
        break;
      case DTABLE:
        buf.appendBlock(api.tableToString((DTable)o));
        break;
      }
    }
    return buf.toString();
  }

}

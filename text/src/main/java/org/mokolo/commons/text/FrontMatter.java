package org.mokolo.commons.text;

import org.mokolo.commons.data.DMap;

import lombok.Getter;
import lombok.Setter;

public class FrontMatter {
  @Getter @Setter private DMap dMap;

  public FrontMatter() {
    this.dMap = new DMap();
  }
  
  public FrontMatter(DMap map) {
    this.dMap = new DMap(map);
  }

}

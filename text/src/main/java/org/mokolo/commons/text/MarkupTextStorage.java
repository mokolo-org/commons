package org.mokolo.commons.text;

import java.io.IOException;

import org.mokolo.commons.data.DException;

/**
 * Data Accessor Object for CRUD operations on markup documents
 * 
 * The path uses slashes as separators and includes the
 * target object and excludes an extension.
 */
abstract public class MarkupTextStorage {

  /**
   * Returns the MarkupText given the path
   * 
   * If the object does not exist, an IOException is thrown.
   */
  abstract public MarkupText get(String path)
      throws DException, TextException, IOException;
  
  /**
   * Creates or replaces a MarkupText in storage
   * 
   */
  abstract public void put(String path, MarkupText markupText)
      throws DException, TextException, IOException;

}

package org.mokolo.commons.text;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.StringUtils;
import org.mokolo.commons.cmd.CommandRunner;
import org.mokolo.commons.data.DAtom;
import org.mokolo.commons.data.DColumn;
import org.mokolo.commons.data.DColumnGroup;
import org.mokolo.commons.data.DComment;
import org.mokolo.commons.data.DException;
import org.mokolo.commons.data.DLine;
import org.mokolo.commons.data.DList;
import org.mokolo.commons.data.DMap;
import org.mokolo.commons.data.DObject;
import org.mokolo.commons.data.DParagraph;
import org.mokolo.commons.data.DRow;
import org.mokolo.commons.data.DSimple;
import org.mokolo.commons.data.DString;
import org.mokolo.commons.data.DTable;
import org.mokolo.commons.text.MarkupTextGenerator.Level;

import lombok.Cleanup;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class RestructuredTextAPI extends MarkupAPI {

  @Setter private String rst2htmlOptions;

  private static final String[] adornments = { "=", "+", "-", "~", "^", "#" };

  public RestructuredTextAPI() {
    super();
  }
  
  @Override
  public MarkupFormat getMarkupFormat() {
    return MarkupFormat.RESTRUCTUREDTEXT;
  }

  @Override
  public void frontMatterPreSave(MarkupText markupText) {
  }

  /**
   * Adds DAtom types and DList types as :key: value
   * 
   * Ignores tables and other complicated metadata.
   * Data types that cannot be serialized produce WARN log messages.
   * @throws DException 
   */
  @Override
  public String frontMatterToString(FrontMatter frontMatter) throws DException {
    if (frontMatter == null)
      return "";
    
    TextBuffer buf = new TextBuffer();
    for (Map.Entry<DAtom, DSimple> entry : frontMatter.getDMap().getMap().entrySet()) {
      String key = ":"+entry.getKey().toString()+":";
      if (entry.getValue() instanceof DAtom)
        buf.appendString(
            false,
            0,
            key+" "+entry.getValue().toString().replaceAll("\n", "\n   "),
            true);
      else if (entry.getValue() instanceof DList) {
        DList list = (DList) entry.getValue();
        if (list.getList().isEmpty())
          throw new DException("Cannot convert empty list to metadata");
        buf.appendString(
            false,
            0,
            key+" - "+list.getList().get(0).toString(),
            true);
        for (int i=1; i<list.getList().size(); i++)
          buf.appendString(
              false,
              key.length() + 1,
              "- "+list.getList().get(i).toString(),
              true);
      }
      else {
        log.warn(
            "Ignoring map entry with key '"+entry.getKey().toString()+"'. " +
            "Value is a "+entry.getValue().getClass().getName());
      }
    }
    return buf.toString();
  }

  @Override
  public boolean increaseContentStartLevelByOneIfFrontMatter() {
    return false;
  }

  @Override
  public String commentToString(DComment comment) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public String lineToString(DLine line) {
    return "\n---\n\n";
  }

  @Override
  public String listToString(DList list) {
    TextBuffer buf = new TextBuffer();
    buf.appendNewLine();
    for (DObject o : list.getList())
      buf.appendString(false, 0, "- "+this.replaceLinks(o.toString()), true);
    return buf.toString();
  }

  @Override
  public String mapToString(DMap map) {
    if (map == null)
      return "";

    TextBuffer buf = new TextBuffer();
    for (Map.Entry<DAtom, DSimple> entry : map.getMap().entrySet()) {
      String key = "- "+entry.getKey().toString()+":";
      if (entry.getValue() instanceof DAtom) {
        String value = entry.getValue().toString();
        if (value == null)
          value = "";
        else
          value = this.replaceLinks(value);
        buf.appendString(
            false,
            0,
            key+" "+value.replaceAll("\n", "\n   "),
            true);
      }
      else if (entry.getValue() instanceof DList) {
        buf.appendString(
            false,
            0,
            key,
            true);
        buf.appendNewLine();
        DList list = (DList) entry.getValue();
        for (DObject o : list.getList())
          buf.appendString(
              false,
              2,
              "- "+this.replaceLinks(o.toString()),
              true);
        buf.appendNewLine();
      }
      else {
        log.warn(
            "Ignoring map entry with key '"+entry.getKey().toString()+"'. " +
            "Value is a "+entry.getValue().getClass().getName());
      }
    }
    return buf.toString();
  }

  @Override
  public String paragraphToString(DParagraph paragraph) {
    TextBuffer buf = new TextBuffer();
    buf.appendString(false, 0, this.replaceLinks(paragraph.getValue()), true);
    buf.appendNewLine();
    return buf.toString();
  }

  @Override
  public String tableToString(DTable table) {
    TextBuffer buf = new TextBuffer();
    StringBuffer lines = new StringBuffer();
    StringBuffer captions = new StringBuffer();
    List<Integer> firstPositionsOfColumns = new ArrayList<>();
    firstPositionsOfColumns.add(0);
    for (DColumn col : table.getColumns().values()) {
      int width = col.getMaxWidth();
      if (captions.length() > 0) {
        captions.append(" ");
        lines.append(" ");
      }
      captions.append(String.format("%-"+col.getMaxWidth()+"s", col.getCaption()));
      lines.append("=".repeat(width));
      firstPositionsOfColumns.add(lines.length()+1);
    }
    buf.appendString(true, 0, lines.toString(), true);
    if (table.getColumnGroups().size() > 0) {
      String cgCaptions = " ".repeat(lines.length());
      String cgLines = " ".repeat(lines.length());
      for (DColumnGroup cg : table.getColumnGroups().values()) {
        int startPos = table.getStartPosColumn(cg.getStartColumn());
        int endPos = table.getEndPosColumn(cg.getStartColumn()+cg.getColumnCount()-1);
        cgCaptions = StringUtils.overlay(cgCaptions, cg.getCaption(), startPos, startPos+cg.getCaption().length());
        cgLines = StringUtils.overlay(cgLines, "-".repeat(endPos-startPos), startPos, endPos);
      }
      buf.appendString(false, 0, cgCaptions, true);
      buf.appendString(false, 0, cgLines, true);
    }
    buf.appendString(false, 0, captions.toString(), true);
    buf.appendString(false, 0, lines.toString(), true);
    for (DRow row : table.getRows()) {
      StringBuffer rowBuf = new StringBuffer();
      for (Entry<String, DColumn> colEntry : table.getColumns().entrySet()) {
        if (rowBuf.length() > 0)
          rowBuf.append(" ");
        DAtom cell = row.get(colEntry.getKey());
        if (cell != null) {
          String cellString = this.replaceLinks(colEntry.getValue().toString(cell));
          rowBuf.append(String.format("%-"+colEntry.getValue().getMaxWidth()+"s", cellString));
        }
        else
          rowBuf.append(" ".repeat(colEntry.getValue().getMaxWidth()));
      }
      buf.appendString(false, 0, rowBuf.toString(), true);
    }
    buf.appendString(false, 0, lines.toString(), true);
    return buf.toString();
  }

  @Override
  public String titleToString(Level level, DString text) {
    TextBuffer buf = new TextBuffer();
    String lines = adornments[level.value-1].repeat(text.length());
    if (level == Level.H1)
      buf.appendString(true, 0, lines, true);
    boolean nl = level == Level.H1 ? false : true;
    buf.appendString(nl, 0, text.getValue(), true);
    buf.appendString(false, 0, lines, true);
    return buf.toString();
  }

  public String replaceLinks(String s) {
    return DSimple.linkPattern.matcher(s).replaceAll(MarkupFormat.RESTRUCTUREDTEXT.getLinkPattern());
  }

  @Override
  public void generateHTML(MarkupText markupText, File targetFile)
  throws NotImplementedException, TextException {
    try {
      File tempRst = File.createTempFile("frommarkup", ".rst");
      @Cleanup OutputStream os = new FileOutputStream(tempRst);
      IOUtils.write(this.serialize(markupText), os, Charset.forName("UTF-8"));
      StringBuffer command = new StringBuffer();
      command.append("rst2html ");
      command.append("--language="+this.getLocale().toString()+" ");
      if (this.rst2htmlOptions != null)
        command.append(this.rst2htmlOptions+" ");
      command.append(tempRst.getAbsolutePath()+" ");
      command.append(targetFile.getAbsolutePath());
      CommandRunner.runCommand(command.toString());
      tempRst.delete();
    } catch (DException | IOException e) {
      throw new TextException(e);
    }
  }

  @Override
  public String generateHTML(MarkupText markupText)
  throws NotImplementedException, TextException {
    try {
      File tempHtml = File.createTempFile("fromrst", ".html");
      this.generateHTML(markupText, tempHtml);
      @Cleanup InputStream is = new FileInputStream(tempHtml);
      String html = IOUtils.toString(is, "UTF-8");
      tempHtml.delete();
      return html;
    } catch (TextException | IOException e) {
      throw new TextException(e);
    }
  }

}

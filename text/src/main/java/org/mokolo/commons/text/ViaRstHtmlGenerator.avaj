package org.mokolo.commons.text;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;

import org.apache.commons.io.IOUtils;
import org.mokolo.commons.cmd.CommandRunner;

import lombok.Cleanup;
import lombok.Setter;

/**
 * Generates reStructuredText and the uses rst2html-command to generate HTML
 *
 */
public class ViaRstHtmlGenerator implements HtmlGenerator {

  @Setter private Locale locale;
  @Setter private String rst2htmlOptions;

  public ViaRstHtmlGenerator() {
    this.locale = Locale.getDefault();
    this.rst2htmlOptions = null;
  }

  @Override
  public void generate(MarkupDocument document, File targetFile) throws TextException {
    try {
      File tempRst = File.createTempFile("generator", ".rst");
      RstGenerator rstGenerator = new RstGenerator();
      rstGenerator.generate(document, tempRst);

      StringBuffer command = new StringBuffer();
      command.append("rst2html ");
      command.append("--language="+this.locale.toString()+" ");
      if (this.rst2htmlOptions != null)
        command.append(this.rst2htmlOptions+" ");
      command.append(tempRst.getAbsolutePath()+" ");
      command.append(targetFile.getAbsolutePath());
      CommandRunner.runCommand(command.toString());
      tempRst.delete();
    } catch (IOException e) {
      throw new TextException(e);
    }
  }

  @Override
  public String generate(MarkupDocument document) throws TextException {
    try {
      File tempHtml = File.createTempFile("generator", ".html");
      this.generate(document, tempHtml);
      @Cleanup InputStream is = new FileInputStream(tempHtml);
      String html = IOUtils.toString(is, "UTF-8");
      tempHtml.delete();
      return html;
    } catch (TextException | IOException e) {
      throw new TextException(e);
    }
  }

}

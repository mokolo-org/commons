package org.mokolo.commons.text;

import java.util.Date;

import org.mokolo.commons.data.DSimple;

import lombok.Getter;
import lombok.Setter;

public class MarkupText {
  @Getter @Setter private MarkupFormat markupFormat;
  @Getter @Setter private FrontMatter frontMatter;
  @Getter @Setter private String text;
  
  public void createEmptyFrontMatter() {
    this.frontMatter = new FrontMatter();
  }
  
  public Date getDate(String key) {
    return this.getFrontMatter().getDMap().getDate(key);
  }

  public String getString(String key) {
    return this.getFrontMatter().getDMap().getString(key);
  }

  public DSimple get(String key) {
    return this.getFrontMatter().getDMap().get(key);
  }
  
  public void putDate(String key, Date value) {
    this.frontMatter.getDMap().putDate(key, value);
  }
  
  public void putString(String key, String value) {
    this.frontMatter.getDMap().putString(key, value);
  }
  
  public void put(String key, DSimple value) {
    this.getFrontMatter().getDMap().put(key, value);
  }
  
  public void putClassType() {
    this.putString("class", this.getClass().getName());
    this.putString("type", this.getType());    
  }

  public String getType() {
    return deCamelCap(this.getClass().getSimpleName());    
  }
  
  /**
   * Splits string into parts based on camel caps and returns parts separated by dashes
   * 
   * If no pattern found, the full string is returned in lower case
   * 
   * "DMap" -> "d-map"
   * "OneTwo" -> "one-two"
   * "ABCFooDBar" -> "abc-foo-d-bar"
   * "abcDef5" -> "abc-def5"
   * "s10" -> "s10"
   */
  public static String deCamelCap(String s) {
    StringBuffer buf = new StringBuffer();
    for (String part : s.split("(?<!(^|[A-Z]))(?=[A-Z])|(?<!^)(?=[A-Z][a-z])")) {
      if (buf.length() > 0)
        buf.append('-');
      buf.append(part.toLowerCase());
    }
    return buf.toString();
  }

}

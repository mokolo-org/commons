package org.mokolo.commons.text;

import java.util.List;

import org.incava.util.diff.Diff;
import org.incava.util.diff.Difference;

/**
 * Lists differences in texts.
 *
 */
public class Differ {
  
  private List<String> before;
  private String beforeLabel;
  private List<String> after;
  private String afterLabel;
  private String label;
  private List<Difference> diffs;
  
  /**
   * @param before Before text
   * @param beforelabel Label for before text (null allowed)
   * @param after After text
   * @param afterLabel Label for after text (null allowed)
   * @param label Label for diff (null allowed)
   */
  public Differ(
      List<String> before,
      String beforeLabel,
      List<String> after,
      String afterLabel,
      String label) {
    this.before = before;
    this.beforeLabel = beforeLabel;
    this.after = after;
    this.afterLabel = afterLabel;
    this.label = label;

    this.diffs = (new Diff<String>(this.before, this.after)).diff();
  }
  
  /**
   * Detects if before and after are different or not.
   */
  public boolean hasDifferences() {
    return (! this.diffs.isEmpty());
  }
  
  /**
   * Generates a listing of changed blocks with line numbers (1..)
   * 
   * Example output:
   * 
   * src -> target (test)
   * ===== [1] d:5,a:5
   * < FN:Peter Johnson
   * -----
   * > FN:Peter J. Johnson
   * ===== [2] d:10-11,a:10
   * < TEL;TYPE=HOME:+31 73 6412345
   * < TEL;TYPE=WORK:+31 13 4654321
   * -----
   * > TEL;TYPE=WORK:+31 13 4123456
   * =====
   * 
   * Header shows beforeLabel and afterLabel only if both not null.
   * header shows label only if not null.
   * 
   * Blocks are numbered (1..) and separated by "=====".
   * Each block header shows block number between square brackets,
   * followed by replaced (deleted) lines range in before text
   * and new (added) lines range in after text.
   * 
   * Each block then shows deleted lines marked with '<',
   * a separator '-----'
   * and added lines marked with '>'.
   * 
   * @return Text listing all changes
   */
  public String diffsAsText() {
    
    StringBuilder buf = new StringBuilder();

    /*
     * Main header
     */
    if (this.beforeLabel != null && this.afterLabel != null)
      buf.append(this.beforeLabel+" -> "+this.afterLabel);
    if (this.label != null) {
      if (buf.length() > 0)
        buf.append(" ");
      buf.append("("+this.label+")");
    }
    if (buf.length() > 0)
      buf.append("\n");

    int blockNr = 0;
    for (Difference difference : diffs) {
      
      blockNr++;
      
      /*
       * Block header
       */
      buf.append("===== ["+blockNr+"] ");
      buf.append("d:"+(difference.getDeletedStart()+1));
      if (difference.getDeletedEnd() > difference.getDeletedStart())
        buf.append("-"+(difference.getDeletedEnd()+1));
      buf.append(",a:"+(difference.getAddedStart()+1));
      if (difference.getAddedEnd() > difference.getAddedStart())
        buf.append("-"+(difference.getAddedEnd()+1));
      buf.append("\n");

      /*
       * Block changes
       */
      for (int i=difference.getDeletedStart(); i<=difference.getDeletedEnd(); i++)
        buf.append("< "+this.before.get(i)+"\n");
      buf.append("----\n");
      for (int i=difference.getAddedStart(); i<=difference.getAddedEnd(); i++)
        buf.append("> "+this.after.get(i)+"\n");
    }
    
    /*
     * Main footer
     */
    if (blockNr > 0)
      buf.append("=====\n");
    
    return buf.toString();  
  }
}

package org.mokolo.commons.text;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;

public enum MarkupFormat {
  MARKDOWN("text/markdown", "[$2]($1)", ".md"),
  RESTRUCTUREDTEXT("text/x-rst", "`$2 <$1>`__", ".rst");
  
  @Getter private String mimeType;
  /**
   * String like "`$2 <$1/>`_" where $1=url and $2=link text
   * 
   * Formatting tables with links in the cells can fail
   * if this markup pattern is longer than the
   * <a href="$1">$2</a> in the source,
   * since the column width is computed for the longest
   * untransformed string.
   */
  @Getter private String linkPattern;
  @Getter private List<String> extensions;
  
  MarkupFormat(String mimeType, String linkPattern, String... extensionsarray) {
    this.mimeType = mimeType;
    this.linkPattern = linkPattern;
    this.extensions = new ArrayList<>();
    for (String extension : extensionsarray)
      this.extensions.add(extension);
  }
  
  public static MarkupFormat getMarkupFormatFilename(String filename) {
    String lcfilename = filename.toLowerCase();
    for (MarkupFormat fmt : MarkupFormat.values()) {
      for (String extension : fmt.extensions) {
        if (lcfilename.endsWith(extension))
          return fmt;
      }
    }
    return null;
  }
  

}

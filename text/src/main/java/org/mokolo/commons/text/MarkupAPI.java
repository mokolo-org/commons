package org.mokolo.commons.text;

import java.io.File;
import java.util.Locale;

import org.apache.commons.lang3.NotImplementedException;
import org.mokolo.commons.data.DComment;
import org.mokolo.commons.data.DException;
import org.mokolo.commons.data.DLine;
import org.mokolo.commons.data.DList;
import org.mokolo.commons.data.DMap;
import org.mokolo.commons.data.DParagraph;
import org.mokolo.commons.data.DString;
import org.mokolo.commons.data.DTable;
import org.mokolo.commons.text.MarkupTextGenerator.Level;

import lombok.Getter;
import lombok.Setter;

/**
 * Transformations to and from a specific markup language
 *
 * Used to generate markup like Markdown or reStructuredText
 * from D*-objects (mokolo.commons.data), but also to parse
 * such markups.
 */
public abstract class MarkupAPI {
  
  @Getter @Setter private Locale locale;
  
  public MarkupAPI() {
    this.locale = Locale.getDefault();
  }
  
  abstract public MarkupFormat getMarkupFormat();
  abstract public void frontMatterPreSave(MarkupText markupText);
  abstract public String frontMatterToString(FrontMatter frontMatter) throws DException;
  abstract public boolean increaseContentStartLevelByOneIfFrontMatter();
  abstract public String commentToString(DComment comment);
  abstract public String lineToString(DLine line);
  abstract public String listToString(DList list);
  abstract public String mapToString(DMap map);
  abstract public String paragraphToString(DParagraph paragraph);
  abstract public String tableToString(DTable table);
  abstract public String titleToString(Level level, DString text);
  
  abstract public void generateHTML(MarkupText markupText, File targetFile)
      throws NotImplementedException, TextException;
  abstract public String generateHTML(MarkupText markupText)
      throws NotImplementedException, TextException;
  
  public String serialize(MarkupText markupText) throws DException {
    this.frontMatterPreSave(markupText);
    StringBuffer buf = new StringBuffer();
    if (markupText.getFrontMatter() != null) {
      buf.append(this.frontMatterToString(markupText.getFrontMatter()));
      buf.append("\n");
    }
    buf.append(markupText.getText());
    return buf.toString();
  }

}

package org.mokolo.commons.text;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.mokolo.commons.data.DException;
import org.mokolo.commons.data.DInteger;
import org.mokolo.commons.data.DList;
import org.mokolo.commons.data.DMap;
import org.mokolo.commons.data.DParagraph;
import org.mokolo.commons.data.DSection;
import org.mokolo.commons.data.DString;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MarkupAPITest {

  @Test
  public void canGenerateSimpleHugoPage() throws Exception {
    try {
      DMap map = new DMap();
      map.putString("title", "Test Hugo page");
      map.put("weight", new DInteger(5));
      DList tags = new DList();
      tags.add(new DString("hugo"));
      tags.add(new DString("yaml"));
      map.put("tags", tags);
      FrontMatter frontMatter = new FrontMatter(map);

      DSection content = new DSection();
      content.setTitle("Content");
      DParagraph li = new DParagraph();
      li.setValue("Lorem ipsum");
      content.add(li);

      MarkupText t = MarkupTextGenerator.generate(frontMatter, content, new HugoPageAPI());
      assertEquals(MarkupFormat.MARKDOWN, t.getMarkupFormat());

      MarkupAPI api = new HugoPageAPI();
      String page = api.serialize(t);
      log.debug("\n" + page);

      String expectedText =
          "---\n" +
          "title: \"Test Hugo page\"\n" +
          "weight: 5\n" +
          "tags:\n" +
          "- \"hugo\"\n" +
          "- \"yaml\"\n" +
          "class: \"org.mokolo.commons.text.MarkupText\"\n" +
          "type: \"markup-text\"\n" +
          "---\n\n" +
          "## Content\n\n" +
          "Lorem ipsum\n\n";
      assertEquals(expectedText, page);

    } catch (Exception e) {
      e.printStackTrace();
      throw new Exception(e);
    }
  }
  
  @Test
  public void canSerializeFullDocumentToRST() throws DException {
    MarkupText markupText = RestructuredTextAPITest.generateTest();
    MarkupAPI api = new RestructuredTextAPI();
    String page = api.serialize(markupText);
    log.debug("\n" + page);
    assertEquals(MarkupFormat.RESTRUCTUREDTEXT, markupText.getMarkupFormat());
    assertEquals(RestructuredTextAPITest.EXPECTED_FULL, page);
  }
  

 
}

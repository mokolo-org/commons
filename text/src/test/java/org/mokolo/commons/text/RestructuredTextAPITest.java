package org.mokolo.commons.text;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.mokolo.commons.data.DException;
import org.mokolo.commons.data.DInteger;
import org.mokolo.commons.data.DList;
import org.mokolo.commons.data.DMap;
import org.mokolo.commons.data.DParagraph;
import org.mokolo.commons.data.DSection;
import org.mokolo.commons.data.DString;

public class RestructuredTextAPITest {
  
  private static final String EXPECTED_VERSION =
      ":Version: 5\n";
  private static final String EXPECTED_STATUS =
      ":Status: Concept\n";
  private static final String EXPECTED_ML =
      ":multi-line string: Very long string that is split into several lines,\n" +
      "   and this is the second line.\n";
  private static final String EXPECTED_AUTHORS =
      ":Authors: - Fred Vos\n" +
      "          - Pietje Puk\n";
  
  private static final String EXPECTED_MAIN_SECTION =
      "============\n" +
      "Introduction\n" +
      "============\n" +
      "\n" +
      "Current situation\n" +
      "+++++++++++++++++\n";
  
  public static final String EXPECTED_FULL =
      EXPECTED_VERSION +
      EXPECTED_STATUS +
      EXPECTED_ML +
      EXPECTED_AUTHORS +
      "\n" +
      EXPECTED_MAIN_SECTION;

  @Test
  public void canSerializeAtomMetadataToRST() throws Exception {
    RestructuredTextAPI rst = new RestructuredTextAPI();
    DMap map;
    
    map = new DMap();
    addVersion(map);
    FrontMatter fm = new FrontMatter(map);
    assertEquals(EXPECTED_VERSION, rst.frontMatterToString(fm));
    
    map = new DMap();
    addStatus(map);
    fm = new FrontMatter(map);
    assertEquals(EXPECTED_STATUS, rst.frontMatterToString(fm));
    
    map = new DMap();
    addML(map);
    fm = new FrontMatter(map);
    assertEquals(EXPECTED_ML, rst.frontMatterToString(fm));
  }
  
  @Test
  public void canSerializeListMetadataToRST() throws Exception {
    RestructuredTextAPI rst = new RestructuredTextAPI();
    DMap map = new DMap();
    addAuthors(map);
    FrontMatter fm = new FrontMatter(map);
    assertEquals(EXPECTED_AUTHORS, rst.frontMatterToString(fm));
  }
  
  @Test
  public void canSerializeSectionsToRST() throws Exception {
    RestructuredTextAPI rst = new RestructuredTextAPI();
    MarkupText t = MarkupTextGenerator.generate(null, generateMainSection(), rst);
    assertEquals(EXPECTED_MAIN_SECTION, t.getText());
  }

  public static MarkupText generateTest() throws DException {
    RestructuredTextAPI rst = new RestructuredTextAPI();

    DMap map = new DMap();
    addVersion(map);
    addStatus(map);
    addML(map);
    addAuthors(map);
    FrontMatter fm = new FrontMatter(map);

    return MarkupTextGenerator.generate(fm, generateMainSection(), rst);
  }
  
  private static void addVersion(DMap map) {
    map.put(new DString("Version"), new DInteger(5));
  }

  private static void addStatus(DMap map) {
    map.put(new DString("Status"), new DString("Concept"));
  }

  private static void addML(DMap map) {
    map.put(
        new DString("multi-line string"),
        new DString(
            "Very long string that is split into several lines,\n" +
            "and this is the second line."));
  }

  private static void addAuthors(DMap map) throws DException {
    DList authors = new DList();
    authors.add(new DString("Fred Vos"));
    authors.add(new DString("Pietje Puk"));
    map.put(new DString("Authors"), authors);
  }

  private static DSection generateMainSection() throws DException {
    DSection main = new DSection();
    main.setTitle(new DString("Introduction"));
    
    DSection l2dot1 = new DSection();
    l2dot1.setTitle(new DString("Current situation"));
    main.add(l2dot1);
    
    return main;
  }
  
  @Test
  public void canReplaceSingleLink() {
    DParagraph p = new DParagraph();
    p.setValue("This is a text with a <a href=\"https://test.org/\">link</a> to a site");
    RestructuredTextAPI ser = new RestructuredTextAPI();
    String expected = "This is a text with a `link <https://test.org/>`__ to a site\n\n";
    assertEquals(expected, ser.paragraphToString(p));
  }

  @Test
  public void canReplaceMultipleLinks() {
    DParagraph p = new DParagraph();
    p.setValue(
        "<a href=\"https://test.org/\">link</a> " +
        "<a href=\"https://test.org/\">link</a>");
    RestructuredTextAPI ser = new RestructuredTextAPI();
    String expected = "`link <https://test.org/>`__ `link <https://test.org/>`__\n\n";
    assertEquals(expected, ser.paragraphToString(p));
  }

}

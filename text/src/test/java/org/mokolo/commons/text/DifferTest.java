package org.mokolo.commons.text;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Test;

import lombok.Cleanup;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DifferTest {

  @Test
  public void canDetectDifferences() throws Exception {
    List<String> before = generateBeforeText();
    List<String> after = new ArrayList<>();
    after.addAll(before);
    Differ differ1 = new Differ(before, "one", after, "two", "differ1");
    assertFalse(differ1.hasDifferences());
    after.remove(4);
    after.add(4, "FN:Peter my Friend");
    Differ differ2 = new Differ(before, "one", after, "two", "difer2");
    assertTrue(differ2.hasDifferences());
    log.debug("As text\n"+differ2.diffsAsText());
  }
  
  private static List<String> generateBeforeText() throws Exception {
    @Cleanup InputStream is = DifferTest.class.getResourceAsStream("/peter.vcf");
    String text = IOUtils.toString(is, "UTF-8");
    List<String> list = new ArrayList<>();
    Collections.addAll(list, text.split("\\r?\\n"));
    return list;
  }
}

package org.mokolo.commons.text;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class MarkupTextTest extends MarkupText {

  @Test
  public void getTypeReturnsStringBasedOnThisClassName() {
    assertEquals("markup-text-test", this.getType());
  }
  
  @Test
  public void deCamelCapWorksForManyStrings() {
    doTest("DMap", "d-map");
    doTest("OneTwo", "one-two");
    doTest("ABCFooDBar", "abc-foo-d-bar");
    doTest("abcDef5", "abc-def5");
    doTest("s10", "s10");
  }
  
  private static void doTest(String src, String expected) {
    assertEquals(expected, MarkupText.deCamelCap(src));
  }

}

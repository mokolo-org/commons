package org.mokolo.commons.text;

import static org.junit.jupiter.api.Assertions.assertEquals;


import org.junit.jupiter.api.Test;
import org.mokolo.commons.data.DException;
import org.mokolo.commons.data.DSection;
import org.mokolo.commons.data.DString;

public class MarkupTextGeneratorTest {
  
  private static final String EXPECTED_MAIN_SECTION =
      "============\n" +
      "Introduction\n" +
      "============\n" +
      "\n" +
      "Current situation\n" +
      "+++++++++++++++++\n";
  
  @Test
  public void canSerializeSectionsToRST() throws Exception {
    RestructuredTextAPI rst = new RestructuredTextAPI();
    MarkupText t = MarkupTextGenerator.generate(null, generateMainSection(), rst);
    assertEquals(EXPECTED_MAIN_SECTION, t.getText());
  }

  private static DSection generateMainSection() throws DException {
    DSection main = new DSection();
    main.setTitle(new DString("Introduction"));
    
    DSection l2dot1 = new DSection();
    l2dot1.setTitle(new DString("Current situation"));
    main.add(l2dot1);
    
    return main;
  }

}

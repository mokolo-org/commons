package org.mokolo.commons.jaxrs;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class LoggingFilterTest {

  @Test
  public void canCleanupRequestBeforeLogging() {
    
    /*
     * All allowed characters
     */
    doTest("abcdefghijklmonopqrstuvwxyz", "abcdefghijklmonopqrstuvwxyz");
    doTest("ABCDEFGHIJKLMNOPQRSTUVWXYZ", "ABCDEFGHIJKLMNOPQRSTUVWXYZ");
    doTest("0123456789", "0123456789");
    doTest(";-._=/", ";-._=/");
    
    /*
     * Some forbidden characters
     */
    doTest("{}[]\"'", "XXXXXX");
    doTest("$#&^()`*", "XXXXXXXX");
    
    /*
     * Mix
     */
    doTest("bla/foo;a=3$", "bla/foo;a=3X");
  }

  private void doTest(String src, String target) {
    assertEquals(target, LoggingFilter.cleanup(src));
  }
}

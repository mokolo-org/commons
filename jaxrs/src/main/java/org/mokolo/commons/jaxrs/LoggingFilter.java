/*
 * Copyright Ⓒ 2019 Tilburg University
 * Copyright Ⓒ 2019 Fred Vos
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.mokolo.commons.jaxrs;

import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;

import lombok.extern.slf4j.Slf4j;

/**
 * Base class for implementing the Logged annotation
 * 
 * Create derived class with getApplicationName() implemented.
 * Add Provider and Logged annotations to your class.
 *
 */
@Slf4j
abstract public class LoggingFilter implements ContainerRequestFilter {

  abstract public String getApplicationName();
  
  @Override
  public void filter(ContainerRequestContext request) throws IOException {
    logRequest(this.getApplicationName(), request);
  }
  
  private static void logRequest(String applicationName, ContainerRequestContext request) {
    String method = request.getMethod();
    String path = request.getUriInfo().getPath();
    String query = request.getUriInfo().getRequestUri().getQuery();
    log.info(applicationName+" "+method+" "+cleanup(path)+(query != null ? "?"+cleanup(query) : ""));
  }
  
  /**
   * Replaces any character that is not present in a set of allowed characters
   */
  public static String cleanup(String s) {
    return s.replaceAll("[^a-zA-Z0-9;\\-\\._\\=/]", "X");
  }
}

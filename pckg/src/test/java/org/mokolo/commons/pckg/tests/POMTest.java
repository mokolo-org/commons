package org.mokolo.commons.pckg.tests;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.mokolo.commons.data.DMap;
import org.mokolo.commons.data.Templater;
import org.mokolo.commons.pckg.POM;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class POMTest {

  @Test
  public void canGenerateSimpleDebianControlFileWithOnlyPOMDataSet()
  throws Exception {
    try {
    DMap modelMap = new DMap();
    POM.updateMap(modelMap);
    
    log.debug("Model:\n"+modelMap.toYAML());
    
    String templateString =
        "Package: ${artifactId}\n"
        + "Architecture: all\n"
        + "Version: ${version}\n"
        + "Maintainer: Fred Vos <fred.vos@tilburguniversity.edu>\n"
        + "Description: ${descriptionfolded}\n";
    
    String controlString =
        Templater.applyTemplateString(
            templateString, modelMap.toModel());
    log.debug("debian/control:\n"+controlString);
    
    assertTrue(controlString.contains("commons-pckg"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }
}

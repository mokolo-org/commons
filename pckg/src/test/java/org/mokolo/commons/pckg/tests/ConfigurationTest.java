package org.mokolo.commons.pckg.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.mokolo.commons.pckg.Jar;
import org.mokolo.commons.pckg.Package;
import org.mokolo.commons.pckg.PackagingConfiguration;
import org.mokolo.commons.pckg.Release;

public class ConfigurationTest {

  static private String TESTPACKAGE = "liblogback-java";
  static private String TEST_PACKAGE_AJAR = "/usr/share/java/logback-core.jar";
  static private int TESTPACKAGE_FILECOUNT = 12;
  
  @Test
  public void canGetPackageFilesFromApt() throws Exception {
    if (Release.canTest()) { // Cannot run in Gitlab
      PackagingConfiguration configuration = PackagingConfiguration.getConfiguration();
      configuration.deleteFromCache(TESTPACKAGE, configuration.getDistroName());
      List<String> files = configuration.getFiles(TESTPACKAGE, configuration.getDistroName(), false);
      assertEquals(TESTPACKAGE_FILECOUNT, files.size());
    }
  }

  @Test
  public void canIndexPackages() throws Exception {
    if (Release.canTest()) { // Cannot run in Gitlab
      PackagingConfiguration configuration = PackagingConfiguration.getConfiguration();
      Map<Jar, Package> map = configuration.indexPackages(false);
      String foundPackage = null;
      configuration.getFiles(TESTPACKAGE, configuration.getDistroName(), false);
      for (Map.Entry<Jar, Package> entry : map.entrySet()) {
        if (entry.getKey().path.equals(TEST_PACKAGE_AJAR)) {
          foundPackage = entry.getValue().name;
          break;
        }
      }
      assertNotNull(foundPackage);
      assertEquals(TESTPACKAGE, foundPackage);
    }
  }
}

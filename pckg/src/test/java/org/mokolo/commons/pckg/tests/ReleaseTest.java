package org.mokolo.commons.pckg.tests;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.mokolo.commons.pckg.Release;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ReleaseTest {

  @Test
  public void canGetAllFieldsOfTheRease() throws Exception {
    if (Release.canTest()) { // Cannot run in Gitlab
      Release release = Release.getRelease();
      log.debug(
          "Release = ["+
              release.distributorID+", "+
              release.description+", "+
              release.release+", "+
              release.codename+"]");
      assertNotNull(release.distributorID);
      assertNotNull(release.description);
      assertNotNull(release.release);
      assertNotNull(release.codename);
    }
  }

}

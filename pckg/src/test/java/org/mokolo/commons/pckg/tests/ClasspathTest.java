package org.mokolo.commons.pckg.tests;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.mokolo.commons.pckg.Classpath;

public class ClasspathTest {

  @Test
  public void canListFilesInClasspath() throws Exception {
    try {
      List<File> filesInClasspath = Classpath.listFiles();
      assertFalse(filesInClasspath.isEmpty());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }
  
  @Test
  public void canListAllClassesWithSourcesInClasspath() throws Exception {
    try {
      Map<String, File> classesInClasspath = Classpath.getAllClasses();
      assertFalse(classesInClasspath.isEmpty());
      File file = classesInClasspath.get("org.mokolo.commons.pckg.Classpath");
      assertNotNull(file);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }
}

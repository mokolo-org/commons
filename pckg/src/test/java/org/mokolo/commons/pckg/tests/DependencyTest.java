package org.mokolo.commons.pckg.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.InputStream;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mokolo.commons.pckg.Dependency;

import lombok.Cleanup;

public class DependencyTest {
    
  @Test
  public void canParseOutputOfMavenDependencyList() throws Exception {
    // Running mvn dependency:list -DoutputFile=<file> generates file like dependency-list.txt
    @Cleanup InputStream is = DependencyTest.class.getResourceAsStream("/dependency-list.txt");
    List<Dependency> dependencies = Dependency.parseDependencyListOutput(is);
    assertEquals(13, dependencies.size());
    
    List<Dependency> runtimeDependencies = Dependency.filterRuntimeJarDependencies(dependencies);
    assertEquals(6, runtimeDependencies.size());
  }
  
}

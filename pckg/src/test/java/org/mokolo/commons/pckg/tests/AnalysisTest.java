package org.mokolo.commons.pckg.tests;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Date;

import org.junit.jupiter.api.Test;
import org.mokolo.commons.data.DList;
import org.mokolo.commons.data.DMap;
import org.mokolo.commons.data.DString;
import org.mokolo.commons.pckg.Analysis;
import org.mokolo.commons.pckg.PackagingConfiguration;
import org.mokolo.commons.pckg.Release;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AnalysisTest {

  @Test
  public void canSerializeAnalysisToXml() throws Exception {
    try {
      Analysis analysis = new Analysis();
      analysis.timestamp = new Date();
      analysis.modelMap.put("groupId", new DString("org.mokolo.commons"));
      analysis.modelMap.put("artifactId", new DString("commons-pckg"));
      analysis.modelMap.put("version", new DString("1.2.3"));
      analysis.modelMap.put("name", new DString("Mokolo Commons Pckg"));
      DList developers = new DList();
      DMap developerMap = new DMap();
      developerMap.put("id", new DString("vosf"));
      developerMap.put("name", new DString("Fred Vos"));
      developerMap.put("email", new DString("fred.vos@mokolo.org"));
      developers.add(developerMap);
      analysis.modelMap.put("developers", developers);
      String xml = Analysis.toXml(analysis);
      log.debug("xml:\n"+xml);
      assertTrue(xml.contains("timestamp"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void canGenerateRst() throws Exception {
    if (Release.canTest()) { // Cannot run in Gitlab
      PackagingConfiguration configuration = PackagingConfiguration.getConfiguration();
      try {
        Analysis analysis = configuration.analyze();
        assertNotNull(analysis);
        String rst = analysis.toRestructuredText();
        log.debug("Analysis as rst:\n"+rst);
        assertNotNull(rst);
        assertTrue(rst.contains("Analysis"));
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }
}

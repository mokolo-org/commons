package org.mokolo.commons.pckg;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import lombok.extern.slf4j.Slf4j;

import org.mokolo.commons.data.DColumn;
import org.mokolo.commons.data.DColumnGroup;
import org.mokolo.commons.data.DDate;
import org.mokolo.commons.data.DException;
import org.mokolo.commons.data.DList;
import org.mokolo.commons.data.DMap;
import org.mokolo.commons.data.DRow;
import org.mokolo.commons.data.DSection;
import org.mokolo.commons.data.DString;
import org.mokolo.commons.data.DTable;
import org.mokolo.commons.data.TimestampAdapter;
import org.mokolo.commons.text.FrontMatter;
import org.mokolo.commons.text.MarkupText;
import org.mokolo.commons.text.MarkupTextGenerator;
import org.mokolo.commons.text.RestructuredTextAPI;

/**
 * Contains all relevant data on dependencies for packaging and reporting
 * 
 * Result of the Configuration.analyze() method.
 */
@Slf4j
@XmlRootElement
public class Analysis {
  @XmlAttribute(name = "timestamp")
  @XmlJavaTypeAdapter(TimestampAdapter.class)
  public Date timestamp;
  public String distroName;
  public Map<Dependency, Jar> dependenciesJars;
  public Map<Jar, Package> jarsPackages; // no clauses in packages
  public Set<Package> packages; // packages contain clauses if set in cfg
  @XmlElement public DMap modelMap;

  public static final int MAX_LENGTH_SINGLE_LINE = 60;

  private static JAXBContext context;
  static {
    try {
      context = JAXBContext.newInstance(
          Analysis.class,
          DList.class,
          DMap.class,
          DString.class
          );
    } catch (JAXBException e) {
      log.error("Cannot create context");
    }
  }
  private static Marshaller createMarshaller() throws JAXBException {
    Marshaller marshaller = context.createMarshaller();
    marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
    return marshaller;
  }
  private static Unmarshaller createUnmarshaller() throws JAXBException {
    return context.createUnmarshaller();
  }
  public static Analysis parseXml(InputStream is) throws JAXBException {
    return (Analysis) createUnmarshaller().unmarshal(is);
  }
  public static void serializeToXml(Analysis analysis, OutputStream os) throws JAXBException {
    createMarshaller().marshal(analysis, os);
  }
  public static String toXml(Analysis analysis) throws JAXBException {
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    serializeToXml(analysis, baos);
    return baos.toString(Charset.forName("UTF-8"));
  }

  public Analysis() {
    this.dependenciesJars = new TreeMap<>(new DependencyComparator());
    this.jarsPackages = new HashMap<>();
    this.packages = new TreeSet<>(new PackageComparator());
    this.modelMap = new DMap();
  }

  public void putDependencyJar(Dependency dependency, Jar jar) {
    this.dependenciesJars.put(dependency, jar);
  }

  public void putJarPackage(Jar jar, Package thePackage) {
    this.jarsPackages.put(jar, thePackage);
  }

  public String getClassPathString() {
    List<String> list = new ArrayList<>();
    for (Jar jar : this.dependenciesJars.values()) {
      if (jar != null)
        list.add(jar.path);
    }
    return String.join(":", list);
  }

  public String getDependenciesFolded() {
    StringBuffer buf = new StringBuffer();
    StringBuffer linebuf = new StringBuffer();
    for (Package pckg : this.packages) {
      if (linebuf.length() > 0 &&
          linebuf.length() + pckg.toString().length() + 2
          > MAX_LENGTH_SINGLE_LINE) {
        buf.append(linebuf.toString());
        buf.append("\n");
        linebuf = new StringBuffer();
      }
      linebuf.append(" ");
      linebuf.append(pckg.toString());
      linebuf.append(",");
    }
    if (linebuf.length() > 0)
      buf.append(linebuf.toString().substring(0, linebuf.length()-1));
    if (buf.length() > 0);
    buf.delete(0, 1);
    return buf.toString();
  }

  public List<Dependency> getUnpackagedDependencies() {
    List<Dependency> unpackagedDependencies = new ArrayList<>();
    for (Map.Entry<Dependency, Jar> dj : this.dependenciesJars.entrySet())
      if (dj.getValue() == null)
        unpackagedDependencies.add(dj.getKey());
    return unpackagedDependencies;
  }

  public String toRestructuredText() throws DException {
    DSection main = new DSection();
    main.setTitle(new DString("Analysis"));

    DSection s1 = new DSection();
    s1.setTitle(new DString("Dependencies and found jars/packages"));

    DTable table = new DTable();

    table.addColumn(new DColumn("groupId", null, DString.class, true));
    table.addColumn(new DColumn("artifactId", null, DString.class, true));
    table.addColumn(new DColumn("aVersion", "version", DString.class, true));
    table.addColumnGroup(new DColumnGroup(0, 3, "Dependency"));

    table.addColumn(new DColumn("jVersion", "version", DString.class, false));
    table.addColumn(new DColumn("file", null, DString.class, false));
    table.addColumn(new DColumn("package", null, DString.class, false));
    table.addColumnGroup(new DColumnGroup(3, 3, "Jar (distro = "+this.distroName+")"));

    for (Map.Entry<Dependency, Jar> dj : this.dependenciesJars.entrySet()) {
      DRow row = new DRow();
      row.put("groupId", new DString(dj.getKey().groupId));
      row.put("artifactId", new DString(dj.getKey().artifactId));
      row.put("aVersion", new DString(dj.getKey().version));
      Jar jar = dj.getValue();
      if (jar != null) {
        if (jar.version != null)
          row.put("jVersion", new DString(dj.getValue().version));
        if (jar.path != null) {
          File file = new File(jar.path);
          row.put("file", new DString(file.getName()));
        }
        Package pckg = this.jarsPackages.get(jar);
        if (pckg != null) {
          row.put("package", new DString(pckg.name));
        }
      }
      table.addRow(row);
    }

    s1.add(table);
    main.add(s1);

    DSection s2 = new DSection();
    s2.setTitle(new DString("Debian packages"));

    DList sPackages = new DList();
    for (Package pckg : this.packages)
      sPackages.add(new DString(pckg.toString()));
    s2.add(sPackages);
    main.add(s2);

    DSection s3 = new DSection();
    s3.setTitle(new DString("Unpackaged dependencies"));

    DList sDependencies = new DList();
    for (Dependency dependency : this.getUnpackagedDependencies())
      sDependencies.add(new DString(dependency.toString()));
    s3.add(sDependencies);
    main.add(s3);

    DSection s4 = new DSection();
    s4.setTitle(new DString("Freemarker model"));

    /*
    DList sModel = new DList();
    for (Map.Entry<DAtom, DObject> entry : this.modelMap.getMap().entrySet()) {
      sModel.add(new DString(entry.getKey().toString()+": "+entry.getValue().toString()));
    }
    s4.add(sModel);
    */
    s4.add(this.modelMap);
    main.add(s4);

    DMap metadata = new DMap();
    metadata.put("Date", new DDate(this.timestamp));

    FrontMatter fm = new FrontMatter(metadata);
    MarkupText markupText = MarkupTextGenerator.generate(fm, main, new RestructuredTextAPI());
    RestructuredTextAPI api = new RestructuredTextAPI();
    return api.serialize(markupText);
  }

  /**
   * Adds data to an existing DMap
   * 
   * Map:
   * 
   * 1    dependenciesfolded  String (*)
   * 0..1 classpath           String (**)
   *
   * (*)   Only java libraries, folded,
   *       max length per line = MAX_LENGTH_SINGLE_LINE (***)
   *       all lines following the first one prefixed with
   *       single space
   * (**)  Only for libraries from packaged dependencies
   * (***) Unless single dependency including clause exceeds this
   */
  public void updateMap(DMap map) {
    map.put("dependenciesfolded", new DString(this.getDependenciesFolded()));
    String cp = this.getClassPathString();
    if (cp != null)
      map.put("classpath", new DString(cp));
  }

}

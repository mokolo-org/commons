package org.mokolo.commons.pckg;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.io.FileUtils;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;
import org.mokolo.commons.cmd.CommandRunner;
import org.mokolo.commons.data.DException;
import org.mokolo.commons.io.conf.ConfigurationLoader;

@Slf4j
public class PackagingConfiguration  extends org.mokolo.commons.io.conf.Configuration {
  private static final long serialVersionUID = 6757473299692559174L;
  private static PackagingConfiguration packagingConfiguration;

  private File cacheDirectory;
  @Getter private String distroName;

  private static final String APPLICATION_NAME = "pommade";
  
  private PackagingConfiguration(org.mokolo.commons.io.conf.Configuration parent)
  throws IOException {
    super();
    this.putAll(parent);
    this.cacheDirectory = new File(
        System.getProperty("user.home")+File.separator+
        "."+this.getString("application-name")+File.separator+
        "cache");
    this.cacheDirectory.mkdirs();
    Release release = Release.getRelease();
    this.distroName = release.codename;
    log.debug("Distro name = "+this.distroName);
  }
  
  // Singleton
  public static PackagingConfiguration getConfiguration() {
    if (packagingConfiguration == null) { // First check
      synchronized (PackagingConfiguration.class) {
        if (packagingConfiguration == null) { // Double checked locking
          try {
            log.info("Load configuration for singleton");
            packagingConfiguration = new PackagingConfiguration(
                ConfigurationLoader.getConfiguration(APPLICATION_NAME));
            packagingConfiguration.setPrefix(APPLICATION_NAME);
            packagingConfiguration.updateMode();
          } catch (Exception e) {
            e.printStackTrace();
          }
        }
      }
    }
    return packagingConfiguration;
  }
  
  /**
   * Indexes packages to map jars to packages
   */
  public Map<Jar, Package> indexPackages(boolean standAlone) throws IOException {
    Map<Jar, Package> map = new HashMap<Jar, Package>();
    for (Package pckg : this.getPackages()) {
      List<String> filenames = this.getFiles(pckg.name, this.distroName, standAlone);
      List<Jar> jars = Jar.getJarsListSinglePackage(filenames);
      for (Jar jar : jars)
        map.put(jar, pckg);
    }
    return map;
  }
  
  /**
   * Lists packages as listed in configuration files
   */
  public List<Package> getPackages() {
    List<Package> list = new ArrayList<Package>();
    for (int i=0; i<=9; i++) {
      String propertyKey = "packages-"+i;
      String propertyValue = this.getString(propertyKey);
      if (propertyValue != null) {
        String[] packagesInProperty = propertyValue.split("\\s+");
        for (String packageName : packagesInProperty) {
          Package pckg = new Package(packageName);
          list.add(pckg);
        }
      }
    }
    return list;
  }
  
  public void putDependencyFile(String groupId, String artifactId, String distro, String fileAsString) {
    this.addString("dependency."+groupId+"."+artifactId+"."+distro+".file", fileAsString);
  }
  
  public void putDependencyFile(String groupId, String artifactId, String fileAsString) {
    this.addString("dependency."+groupId+"."+artifactId+".file", fileAsString);
  }

  public void putPackageFiles(String packageName, String... filesAsStrings) {
    this.addString("package."+packageName+".files", String.join(" ", filesAsStrings));
  }
  
  public void putPackageFiles(String packageName, List<String> filesAsStrings) {
    this.addString("package."+packageName+".files", String.join(" ", filesAsStrings));
  }

  public String getConfiguredFileAsString(String groupId, String artifactId, String distro) {
    return this.getString(
        "dependency."+groupId+"."+artifactId+"."+distro+".file",
        this.getString("dependency."+groupId+"."+artifactId+".file"));
  }
  
  public String getComputedFileAsString(String groupId, String artifactId) {
    File baseDirectory = this.getFile("files.base-directory");
    File file = new File(baseDirectory, artifactId+".jar");
    return file.getAbsolutePath();
  }
  
  /**
   * Returns list of files in a Debian-like Linux system, like Debian or Ubuntu
   * 
   * Uses a cache in the $HOME/.pommade/cache/'distro' directory to check if file
   * 'packageName'.files is present and loads it if so.
   * 
   * If standAlone is false and the file is not present,
   * it uses apt-file list 'packageName' and selects all files ending with '.jar'
   * in the /usr/share/java/ directory or deeper, and stores it as a new
   * 'packageName'.files file in the cache.
   * 
   * You can use any name for the distro.
   * You can fill the cache directory with external files before use if you like.
   * You can force new versions of files by emptying the cache and using this
   * method with standAlone is false.
   * 
   * Returns empty list if no file in cache with standAlone is true or no package found.
   * @throws IOException 
   */
  public List<String> getFiles(String packageName, String distro, boolean standAlone)
  throws IOException {
    File distroCacheDirectory = new File(this.cacheDirectory, distro);
    if (! distroCacheDirectory.exists()) {
      distroCacheDirectory.mkdirs();
      log.info("Created cache directory "+distroCacheDirectory);
    }
    
    String filesFileName = packageName+".files";
    File filesFile = new File(distroCacheDirectory, filesFileName);
    if (! filesFile.exists()) {
      if (standAlone)
        log.warn("File "+filesFileName+" not present in cache directory "+distroCacheDirectory.getAbsolutePath());
      else {
        CommandRunner.runCommand("apt-file list "+packageName+" | egrep ' /usr/share/.*\\.jar' | sed -e 's/^.*: //' > "+filesFile.getAbsolutePath());
        log.info("Generated file "+filesFileName+" in cache directory "+distroCacheDirectory.getAbsolutePath());
      }
    }
    List<String> list;
    if (filesFile.exists())
      list = FileUtils.readLines(filesFile, Charset.forName("UTF-8"));
    else
      list = new ArrayList<>();
    return list;
  }
  
  /**
   * Removes package files list from cache (for tests)
   */
  public void deleteFromCache(String packageName, String distro)
  throws IOException {
    File distroCacheDirectory = new File(this.cacheDirectory, distro);
    if (! distroCacheDirectory.exists()) {
      distroCacheDirectory.mkdirs();
      log.info("Created cache directory "+distroCacheDirectory);
    }
    
    String filesFileName = packageName+".files";
    File filesFile = new File(distroCacheDirectory, filesFileName);
    if (filesFile.exists()) {
      filesFile.delete();
      log.info("Deleted file "+filesFileName+" from cache directory "+distroCacheDirectory.getAbsolutePath());
    }
  }

  public Analysis analyze()
  throws IOException, XmlPullParserException, DException {
    Analysis analysis = new Analysis();
    analysis.timestamp = new Date();
    analysis.distroName = this.getDistroName();

    Map<Jar, Package> jarsPackages = this.indexPackages(false);
    try {
      List<Dependency> dependencies = Dependency.getDependencyList();
      dependencies = Dependency.filterRuntimeJarDependencies(dependencies);
      for (Dependency dependency : dependencies) {
        String distroName = this.getDistroName();
        
        // Find filename for dependency:
        String filePath = null;
        filePath = this.getConfiguredFileAsString(dependency.groupId, dependency.artifactId, distroName);
        if (filePath == null)
          filePath = this.getComputedFileAsString(dependency.groupId, dependency.artifactId);
        
        // Can we find a jar for this file?
        Jar jar = null;
        Package pckg = null;
        for (Map.Entry<Jar, Package> entry : jarsPackages.entrySet()) {
          if (entry.getKey().path.equals(filePath)) {
            jar = entry.getKey();
            pckg = entry.getValue();
            break;
          }
        }
        analysis.putDependencyJar(dependency, jar);
        if (jar != null)
          analysis.putJarPackage(jar, pckg);
      }
    } catch (IOException e) {
      log.error(e.getClass().getName()+" occurred. Msg="+e.getMessage());
    }
    
    for (Package pckg : analysis.jarsPackages.values()) {
      if (pckg != null) {
        if (! analysis.packages.contains(pckg)) {
          String clause = this.getString("package."+pckg.name+".clause");
          pckg.setClause(clause);
          analysis.packages.add(pckg);  
        }
      }
    }
    
    POM.updateMap(analysis.modelMap);
    analysis.updateMap(analysis.modelMap);

    return analysis;
  }

}

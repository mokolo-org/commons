package org.mokolo.commons.pckg;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.io.IOUtils;
import org.mokolo.commons.cmd.CommandRunner;
import org.mokolo.commons.cmd.Output;

import lombok.Cleanup;

@XmlRootElement
public class Dependency {
  @XmlElement public String groupId;
  @XmlElement public String artifactId;
  @XmlElement public String type;
  @XmlElement public String version;
  @XmlElement public String context;

  @Override
  public String toString() {
    return this.groupId+":"+this.artifactId+":"+this.version+":"+this.type;
  }
  
  public boolean isJar() {
    return (this.type.equals("jar"));
  }
  
  /**
   * Returns string until first dot in groupId of groupId if it has no dots
   */
  public String getMainGroupId() {
    int pos = this.groupId.indexOf('.');
    if (pos < 0)
      return this.groupId;
    else
      return this.groupId.substring(0, pos);
  }
  
  /**
   * Runs 'mvn dependency:list' in current working directory and parses result
   * 
   * Requires a valid pom.xml file and the Maven Dependency plugin configured.
   * 
   * Runs mvn dependency:list -DOutputfile=<temporary file>,
   * then opens the file and parses its contents with the
   * parseDependencyListOutput method.
   */
  public static List<Dependency> getDependencyList()
  throws IOException {
    File tempFile = File.createTempFile("dependency", "list");
    tempFile.deleteOnExit();
    Output output = CommandRunner.runCommand("mvn dependency:list -DoutputFile="+tempFile.getAbsolutePath());
    CommandRunner.logOutput(output, CommandRunner.LogLevel.QUIET);
    @Cleanup InputStream is = new FileInputStream(tempFile);
    return parseDependencyListOutput(is);
  }
  
  public static List<Dependency> parseDependencyListOutput(InputStream is)
  throws IOException {
    List<String> lines = IOUtils.readLines(is, Charset.forName("UTF-8"));
    List<Dependency> dependencies = new ArrayList<>();
    Pattern pattern = Pattern.compile("^\\s+(.+):(.+):(.+):(.+):(.+)$");
    for (String line : lines) {
      Matcher m = pattern.matcher(line);
      if (m.find()) {
        Dependency dependency = new Dependency();
        dependency.groupId = m.group(1);
        dependency.artifactId = m.group(2);
        dependency.type = m.group(3);
        dependency.version = m.group(4);
        dependency.context = m.group(5);
        dependencies.add(dependency);
      }
    }
    return dependencies;
  }
  
  /* Nuttig? */
  public static List<Dependency> filterRuntimeJarDependencies(List<Dependency> source) {
    List<Dependency> target = new ArrayList<>();
    for (Dependency dependency : source)
      if (dependency.type.equals("jar") && dependency.context.equals("compile"))
        target.add(dependency);
    return target; 
  }
}

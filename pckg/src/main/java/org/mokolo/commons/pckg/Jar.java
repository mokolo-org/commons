package org.mokolo.commons.pckg;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Jar {
  @XmlElement public String path;
  @XmlElement public String version;
  
  public static Pattern pattern = Pattern.compile("^(/.*)-([v0-9.]+)(\\.jar)$");
  
  @Override
  public String toString() {
    return this.path+":"+this.version;
  }
  
  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (o == null || getClass() != o.getClass())
      return false;
    Jar that = (Jar) o;
    return this.path.equals(that.path) && this.version.equals(that.version);
  }
  
  @Override
  public int hashCode() {
    return Objects.hash(this.path, this.version);
  }

  /**
   * Extract relevant jars from a list of *.jar-files in a package
   *
   * Given a list of files like:
   * - /usr/share/java/logback-access-1.2.3.jar
   * - /usr/share/java/logback-access.jar
   * - /usr/share/java/logback-classic-1.2.3.jar
   * - /usr/share/java/logback-classic.jar
   * - /usr/share/java/logback-core-1.2.3.jar
   * - /usr/share/java/logback-core.jar
   * 
   * It must generate a list of the following Jars:
   * - /usr/share/java/logback-access.jar, 1.2.3
   * - /usr/share/java/logback-classic.jar, 1.2.3
   * - /usr/share/java/logback-core.jar, 1.2.3
   *
   */
  public static List<Jar> getJarsListSinglePackage(List<String> files) {
    TreeMap<String, TreeSet<String>> versions = new TreeMap<>();

    for (String filename : files) {
      Jar jar = getJar(filename);
      if (! versions.containsKey(jar.path))
        versions.put(jar.path, new TreeSet<String>());
      if (jar.version == null)
        versions.get(jar.path).add("");
      else
        versions.get(jar.path).add(jar.version);
    }
    
    // We now have a map with unique basenames of all jars
    // and a set of versions for these basenames - empty string for no version.

    /*
    log.debug("versions:");
    for (Map.Entry<String, TreeSet<String>> entry : versions.entrySet()) {
      log.debug("- "+entry.getKey());
      for (String version : entry.getValue())
        log.debug("  - "+version);
    }
    log.debug("");
    */
    
    List<Jar> jars = new ArrayList<Jar>();
    
    for (Map.Entry<String, TreeSet<String>> entry : versions.entrySet()) {
      Jar jar = new Jar();
      jar.path = entry.getKey();
      jar.version = entry.getValue().last();
      if (entry.getValue().size() == 1) {
        if (jar.version.length() > 0)
          jar.path = jar.path.replaceAll("\\.jar$", "-"+jar.version+".jar");
      }
      jars.add(jar);
    }
    
    /*
    log.debug("jars:");
    for (Jar jar : jars)
      log.debug("- "+jar.path+" ("+jar.version+")");
    log.debug("");
    */
    
    return jars;
  }
  
  /**
   * Returns Jar object for filename
   * 
   */
  public static Jar getJar(String filename) {
    Jar jar = new Jar();
    Matcher matcher = pattern.matcher(filename);
    if (matcher.find()) {
      jar.path = matcher.group(1)+matcher.group(3);
      jar.version = matcher.group(2);
    }
    else
      jar.path = filename;
    return jar;
  }
}
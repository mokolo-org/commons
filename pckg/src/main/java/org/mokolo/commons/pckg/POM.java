package org.mokolo.commons.pckg;

import java.io.File;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.text.WordUtils;
import org.apache.maven.model.Developer;
import org.apache.maven.model.License;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;
import org.mokolo.commons.data.DException;
import org.mokolo.commons.data.DList;
import org.mokolo.commons.data.DMap;
import org.mokolo.commons.data.DString;

public class POM {

  /**
   * Adds data to an existing Freemarker Model
   * 
   * Map:
   * 
   * 1    groupId            String
   * 1    artifactId         String
   * 1    version            String
   * 0..1 name               String
   * 0..1 description        String
   * 0..1 descriptionfolded  String (*)
   * 0..1 url                String
   * 1    licenses           List<Map> 0..n
   * 1    developers         List<Map> 0..n
   * 
   * License:
   * 
   * 1    name               String
   * 
   * Developer:
   * 
   * 1    id                 String
   * 1    name               String
   * 0..1 email              String
   * 
   * (*)  Same as description, but wrapped with \n+space as
   *      wrap string; for Debian control file 
   * @throws DException 
   */
  public static void updateMap(DMap map)
  throws FileNotFoundException, IOException, XmlPullParserException, DException {
    List<org.apache.maven.model.Model> mavenModels = loadMavenModels();
    map.put("groupId", getGroupId(mavenModels));
    map.put("artifactId", getArtifactId(mavenModels));
    map.put("version", getVersion(mavenModels));
    map.putIfNotNull("name", getName(mavenModels));
    map.putIfNotNull("description", getDescription(mavenModels));
    map.putIfNotNull("descriptionfolded", getDescriptionFolded(mavenModels));
    map.putIfNotNull("url", getUrl(mavenModels));

    DList licenses = new DList();
    for (License license : getLicenses(mavenModels)) {
      DMap licenseMap = new DMap();
      licenseMap.put("name", new DString(license.getName()));
      licenses.add(licenseMap);
    }
    map.put("licenses", licenses);

    DList developers = new DList();
    for (Developer developer : getDevelopers(mavenModels)) {
      DMap developerMap = new DMap();
      developerMap.put("id", new DString(developer.getId()));
      developerMap.put("name", new DString(developer.getName()));
      developerMap.put("email", new DString(developer.getEmail()));
      developers.add(developerMap);
    }
    map.put("developers", developers);
  }
  
  private static List<org.apache.maven.model.Model> loadMavenModels()
  throws FileNotFoundException, IOException, XmlPullParserException {
    List<org.apache.maven.model.Model> mavenModels = new ArrayList<>();
    File pomFile = new File("pom.xml");
    MavenXpp3Reader reader = new MavenXpp3Reader();
    org.apache.maven.model.Model mavenModel = reader.read(new FileReader(pomFile));
    mavenModels.add(mavenModel);
    if (mavenModel.getParent() != null) {
      File parentPomFile = new File(mavenModel.getParent().getRelativePath());
      if (parentPomFile.exists()) {
        org.apache.maven.model.Model parentMavenModel = reader.read(new FileReader(parentPomFile));
        mavenModels.add(parentMavenModel);
      }
    }
    return mavenModels;
  }

  private static DString getGroupId(List<org.apache.maven.model.Model> mavenModels) {
    String s = null;
    for (org.apache.maven.model.Model model : mavenModels) {
      s = model.getGroupId();
      if (s != null)
        break;
    }
    return new DString(s);
  }
  
  private static DString getArtifactId(List<org.apache.maven.model.Model> mavenModels) {
    String s = null;
    for (org.apache.maven.model.Model model : mavenModels) {
      s = model.getArtifactId();
      if (s != null)
        break;
    }
    return new DString(s);
  }
  
  private static DString getVersion(List<org.apache.maven.model.Model> mavenModels) {
    String s = null;
    for (org.apache.maven.model.Model model : mavenModels) {
      s = model.getVersion();
      if (s != null)
        break;
    }
    return new DString(s);
  }
  
  private static DString getName(List<org.apache.maven.model.Model> mavenModels) {
    String s = null;
    for (org.apache.maven.model.Model model : mavenModels) {
      s = model.getName();
      if (s != null)
        break;
    }
    return new DString(s);
  }
  
  private static DString getDescriptionFolded(
      List<org.apache.maven.model.Model> mavenModels) {
    return new DString(WordUtils.wrap(
        getDescription(mavenModels).getValue(),
        Analysis.MAX_LENGTH_SINGLE_LINE,
        "\n ",
        false));
  }

  private static DString getDescription(List<org.apache.maven.model.Model> mavenModels) {
    String s = null;
    for (org.apache.maven.model.Model model : mavenModels) {
      s = model.getDescription();
      if (s != null)
        break;
    }
    return new DString(s);
  }
  
  private static DString getUrl(List<org.apache.maven.model.Model> mavenModels) {
    String s = null;
    for (org.apache.maven.model.Model model : mavenModels) {
      s = model.getUrl();
      if (s != null)
        break;
    }
    return new DString(s);
  }
  
  private static List<License> getLicenses(List<org.apache.maven.model.Model> mavenModels) {
    List<License> licenses = new ArrayList<>();
    for (org.apache.maven.model.Model model : mavenModels) {
      licenses.addAll(model.getLicenses());
    }
    return licenses;
  }
  
  private static List<Developer> getDevelopers(List<org.apache.maven.model.Model> mavenModels) {
    List<Developer> developers = new ArrayList<>();
    for (org.apache.maven.model.Model model : mavenModels) {
      developers.addAll(model.getDevelopers());
    }
    return developers;
  }
  
}

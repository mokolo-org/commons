package org.mokolo.commons.pckg;

import java.io.File;
import java.io.FileNotFoundException;

public class Repository {

  /**
   * Returns the file as found in $HOME/.m2/repository.
   * 
   * @throws FileNotFoundException if file does not exist 
   */
  public static File getFile(Dependency dependency)
  throws FileNotFoundException {
    File group = new File(getRepository(), dependency.groupId.replaceAll("\\.", File.separator));
    File artifact = new File(group, dependency.artifactId);
    File version = new File(artifact, dependency.version);
    String filename = dependency.artifactId+"-"+dependency.version+"."+dependency.type;
    File file = new File(version, filename);
    if (! file.exists())
      throw new FileNotFoundException(file.getAbsolutePath());
    return file;
  }

  public static File getRepository()
  throws FileNotFoundException {
    File userHome = new File(System.getProperty("user.home"));
    File m2 = new File(userHome, ".m2");
    File repository = new File(m2, "repository");
    if (! repository.exists())
      throw new FileNotFoundException(repository.getAbsolutePath());
    return repository;
  }

}

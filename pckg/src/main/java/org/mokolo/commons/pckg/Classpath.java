package org.mokolo.commons.pckg;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import lombok.Cleanup;

public class Classpath {
  
  /**
   * Generates map of classes in the classpath with their classpath element
   * @throws IOException 
   * @throws ClassNotFoundException 
   *
   */
  public static Map<String, File> getAllClasses() throws IOException {
    Map<String, File> map = new HashMap<>();
    try {
      for (File file : listFiles()) {
        if (file.isDirectory())
          for (String s : findClassesInDirectory(file))
            map.put(s, file);
        else {
          if (file.getName().endsWith(".jar"))
            for (String s : findClassesInJar(file))
              map.put(s, file);
        }
      }
    } catch (NoClassDefFoundError e) {
      e.printStackTrace();
      throw e;
    }
    return map;
  }

  public static void printClassesFiles(File outputFile) throws IOException {
    Map<String, File> classesInClasspath = getAllClasses();
    OutputStream os = new FileOutputStream(outputFile);
    @Cleanup BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os));
    for (Map.Entry<String, File> entry : classesInClasspath.entrySet()) {
      bw.write(entry.getKey()+" -> "+entry.getValue().getAbsolutePath());
      bw.newLine();
    }
  }
  
  /**
   * Lists files (jars + directories) in classpath
   * 
   * @throws IOException 
   */
  public static List<File> listFiles() throws IOException {
    String classpathString = System.getProperty("java.class.path");

    List<File> files = new ArrayList<>();
    for (String s : classpathString.split(File.pathSeparator)) {
      files.add(new File(s));
    }
    return files;
  }

  /**
   * Calls recursive findClasses()
   */
  public static List<String> findClassesInDirectory(File directory) {
    return findClassesInDirectory(directory, null);
  }
  
  /**
   * Recursive method used to find all classes in a given directory and subdirs.
   *
   * Extended version of method found in
   * https://stackoverflow.com/questions/520328/can-you-find-all-classes-in-a-package-using-reflection
   * 
   * @param directory The base directory
   * @param packageName The package name for classes found inside the base directory (null for base)
   * @return The classes
   * @throws ClassNotFoundException
   */
  private static List<String> findClassesInDirectory(File directory, String packageName) {
    List<String> classes = new ArrayList<>();
    if (! directory.exists()) {
      return classes;
    }
    File[] files = directory.listFiles();
    for (File file : files) {
      if (file.isDirectory()) {
        if (! file.getName().contains(".") ) {
          String extendedPackageName = null;
          if (packageName == null)
            extendedPackageName = file.getName();
          else
            extendedPackageName = packageName + "." + file.getName();
          classes.addAll(findClassesInDirectory(file, extendedPackageName));
        }
      } else if (file.getName().endsWith(".class")) {
        classes.add(packageName + '.' + file.getName().substring(0, file.getName().length() - 6));
      }
    }
    return classes;
  }

  /**
   * Lists classes in a jar
   * 
   * https://stackoverflow.com/questions/15720822/how-to-get-names-of-classes-inside-a-jar-file
   * With improvements.
   */
  public static List<String> findClassesInJar(File jar) throws IOException {
    List<String> classes = new ArrayList<>();
    @Cleanup ZipInputStream zip = new ZipInputStream(new FileInputStream(jar));
    for (ZipEntry entry = zip.getNextEntry(); entry != null; entry = zip.getNextEntry()) {
      if (! entry.isDirectory() && entry.getName().endsWith(".class")) {
        String classNameWithClass = entry.getName().replace('/', '.'); // including ".class"
        String className = classNameWithClass.substring(0, classNameWithClass.length() - ".class".length());
        classes.add(className);
      }
    }
    return classes;
  }
  

}

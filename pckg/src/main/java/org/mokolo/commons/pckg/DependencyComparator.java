package org.mokolo.commons.pckg;

import java.util.Comparator;

public class DependencyComparator implements Comparator<Dependency> {

  public int compare(Dependency a, Dependency b) {
    int result = a.groupId.compareTo(b.groupId);
    if (result == 0)
      result = a.artifactId.compareTo(b.artifactId);
    return result;
  }
}

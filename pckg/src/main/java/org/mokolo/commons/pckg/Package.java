package org.mokolo.commons.pckg;

import java.util.Objects;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Setter;

@XmlRootElement
public class Package {
  @XmlElement public String name;
  @Setter @XmlElement public String clause;

  public Package() { }
  
  public Package(String name) {
    this();
    this.name = name;
  }
  
  @Override
  public String toString() {
    if (this.clause != null)
      return this.name + " " + this.clause;
    else
      return this.name;
  }
  
  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (o == null || getClass() != o.getClass())
      return false;
    Package that = (Package) o;
    return this.toString().equals(that.toString());
  }
  
  @Override
  public int hashCode() {
    return Objects.hash(this.toString());
  }


}

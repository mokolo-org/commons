package org.mokolo.commons.pckg;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Properties;

import org.apache.commons.io.IOUtils;
import org.apache.commons.text.StringSubstitutor;

public class PropertiesStringSubstitutor {

  /**
   * Replaces ${} template values in string from stdin
   * with values from properties and writes result to stdout
   * 
   * Parses property-files in argument list into single Map<String, String>
   * @throws IOException 
   */
  public static void main(String[] args) throws IOException {
    Properties props = new Properties();
    
    for (String s : args) {
      File f = new File(s);
      if (! f.exists()) {
        System.err.println("File '"+s+"' does not exist");
        System.exit(1);
      }
      InputStream is = new FileInputStream(f);
      props.load(is);
    }
    
    String i = IOUtils.toString(System.in, Charset.forName("UTF-8"));
    System.out.println(StringSubstitutor.replace(i, props));
  }
}

package org.mokolo.commons.pckg;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.mokolo.commons.cmd.CommandRunner;
import org.mokolo.commons.cmd.CommandRunner.LogLevel;
import org.mokolo.commons.cmd.Output;

import lombok.extern.slf4j.Slf4j;

/**
 * Merges a list of dependencies into a single new jar
 *
 */
@Slf4j
public class DependencyMerger {
  private List<Dependency> dependencies;
  
  public DependencyMerger() {
    this.dependencies = new ArrayList<>();
  }
  
  public void addDependency(Dependency dependency) {
    this.dependencies.add(dependency);
  }
  
  public void generateTargetJar(File targetJar)
  throws IOException {
    
    // Create temporary directory to unpack all jars in:
    File tempDir = Files.createTempDirectory("jpackager").toFile();
    log.info("Generated temp dir "+tempDir.getAbsolutePath());
    
    // Unpack all jars in that directory:
    for (Dependency dependency : this.dependencies) {
      String filename = Repository.getFile(dependency).getAbsolutePath();
      log.info("Unpacking dependency "+filename+" in temp dir");
      Output output = CommandRunner.runCommand("/usr/bin/jar xf "+filename, tempDir);
      CommandRunner.logOutput(output, LogLevel.NORMAL);
    }
    
    // Replace MANIFEST.MF, since it is the MANIFEST.MF of the last added jar:
    log.info("Replacing MANIFEST.MF");
    File manifest = new File(tempDir, "MANIFEST.MF");
    FileUtils.write(manifest, "Manifest-Version: 1.0\n", Charset.forName("UTF-8"));
    
    // Generate target jar:
    log.info("Generating "+targetJar.getAbsolutePath());
    StringBuffer command = new StringBuffer(
        "/usr/bin/jar cvmf MANIFEST.MF "+targetJar.getAbsolutePath());
    command.append(" META-INF/services");
    for (File f : tempDir.listFiles()) {
      if (f.isDirectory() && f.getName().equals(f.getName().toLowerCase()))
        command.append(" "+f.getName());
    }
    Output output = CommandRunner.runCommand(command.toString(), tempDir);
    CommandRunner.logOutput(output, LogLevel.QUIET);
  }
  
}

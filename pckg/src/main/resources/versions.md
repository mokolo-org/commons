# Versions

## Semantic Versioning

Commons uses [Semantic Versioning](https://semver.org/).
Semantic Versioning has three levels:
- MAJOR
- MINOR
- PATCH

Example: 1.2.0

Maven can use more levels, but three levels are common:
- MAJOR
- MINOR
- INCREMENTAL

Example: 1.2.0

Maven has SNAPSHOT versions too.

Example: 1.2-SNAPSHOT

Snapshot-versions can be used at any level.

Maven allows any versioning scheme, with numbers, text et cetera.
See [https://docs.oracle.com/middleware/1212/core/MAVEN/maven_version.htm#MAVEN8855](Understanding Maven Version Numbers).

## Properies

Versions are maintained in properties:

```xml
  <properties>
    <major.version>11</major.version>
    <minor.version>0</minor.version>
    <incremental.version>13</incremental.version>
```

The ``<version>`` in the pom.xml file is based on these three values.
Scripts help to maintain versions of Commons and other software.

## SNAPSHOTs

An ``<incremental.version>-1</incremental.version>`` in the properties for Commons means a SNAPSHOT version.
Example 1 (SNAPSHOT):

```xml
  <version>11.1-SNAPSHOT</version>
  <properties>
    <major.version>11</major.version>
    <minor.version>1</minor.version>
    <incremental.version>-1</incremental.version>
```
 
Example 2 (normal):

```xml
  <version>11.1.0</version>
  <properties>
    <major.version>11</major.version>
    <minor.version>1</minor.version>
    <incremental.version>0</incremental.version>
```

## Commons Versioning and branches

The Commons library maintains a release branch per minor version, like 'v11.1'.
This reflects the major and minor version of the library.
The major version reflects the Java version of the resulting jars.
Currently there are three active release branches, v7.x, v8.x and v11.x,
where 'x' is a number 0..n.

## cbt

Commons Build Toolkit in the src/main/properties directory of commons-pckg.
A script that helps maintain versions.
The script can be used in other projects too when versions follow Commons rules in those projects
and when major, minor and incremental versions are stored in properties like in Commons.

### ``cbt bump-incremental-version``

Bumps incremental version.

### ``cbt bump-minor-version``

Bumps minor version and sets -1 for incremental version, resulting in a '-SNAPSHOT' version.

### ``cbt display-dependency-tree``

Generates the Maven dependency tree.
Helps to detect version conflicts.

### ``cbt display-dependency-updates``

Checks for new versions of dependencies using Maven tools.

### ``cbt get-major-minor-version``

Returns string like '3.2'.

### ``cbt get-version``

Returns full version.

### ``cbt set-version <version>``

Sets version.
Do not use unless you understand the impact.
Used in Docker image builds.

### ``cbt tag-version [message]``

Tags current version in git repository with optional message.
Used in Docker image builds.

### ``cbt update-versions``

Update version of commons libraries in other projects to highest available version for current major version.
Ignores SNAPSHOT versions.

### ``cbt update-versions-snapshots-ok``

Update version of commons libraries in other projects to highest available version for current major version.
Use SNAPSHOT version if available.


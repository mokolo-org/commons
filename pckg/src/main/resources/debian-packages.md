===============
Debian packages
===============

Introduction
++++++++++++
Many Java dependencies (jars/libraries) are available in Debian packages and can be used and shared in Java applications.
When building a Java application for Debian of Ubuntu, one doesn't need to include all dependencies in a fat jar.
Using jars available in packages results in using updated versions of these dependencies.

Goals
+++++

- Small application jar containing application code only
- Use jars from Debian packages if available
- Put all other dependencies in an extra jar

Small application jar
+++++++++++++++++++++


Not all dependencies available in Debian packages
+++++++++++++++++++++++++++++++++++++++++++++++++

/*
 * Copyright Ⓒ 2019 Fred Vos
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.mokolo.commons.sql;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
// import java.sql.Connection;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;

public class DDLProcessor {
  
  private Properties properties;
  
  public DDLProcessor() {
    this.properties = new Properties();
  }
  
  public DDLProcessor(Properties properties) {
    this();
    this.properties = properties;
  }
  
  /**
   * List all CREATE stuff in correct order
   * 
   * Can be used to create a script to run in an interactive tool for the database.
   * Excludes trailing ';' in each line.
   */
  public List<String> listCreateAll() throws InconsistencyException {
    List<String> list = new ArrayList<>();
    for (String table : this.getTablesInCreateOrder())
      for (String s : this.selectStatements(table, "create"))
        list.add(s);
    return list;
  }
  
  /**
   * List all DROP stuff in correct order
   * 
   * Can be used to create a script to run in an interactive tool for the database.
   * Excludes trailing ';' in each line.
   */
  public List<String> listDropAll() throws InconsistencyException {
    List<String> list = new ArrayList<>();
    for (String table : this.getTablesInDropOrder())
      for (String s : this.selectStatements(table, "drop"))
        list.add(s);
    return list;
  }

  private List<String> selectStatements(String table, String action) {
    List<String> list = new ArrayList<>();
    TreeMap<String, String> map = new TreeMap<>();
    for (Object keyObject : this.properties.keySet()) {
      String key = keyObject.toString();
      if (key.startsWith("ddl.table."+table+"."+action))
        map.put(key, this.properties.getProperty(key));
    }
    list.addAll(map.values());
    return list;
  }

  public List<String> getTablesInDropOrder() throws InconsistencyException {
    List<String> tables = this.getTablesInCreateOrder();
    Collections.reverse(tables);
    return tables;
  }
  
  public List<String> getTablesInCreateOrder() throws InconsistencyException {
    Map<String, Table> workTables = new HashMap<>();
    for (Table table : this.getTables())
      workTables.put(table.getName(), table);
    List<String> returnedTables = new ArrayList<>();
    
    while (workTables.size() > 0) {
      String tableDone = null;
      for (Table table : workTables.values()) {
        if (table.getDependencies().isEmpty()) {
          tableDone = table.getName();
          returnedTables.add(tableDone);
          for (Table innerTable : workTables.values())
            innerTable.removeDependency(tableDone);
          break;
        }
      }
      if (tableDone != null)
        workTables.remove(tableDone);
      else
        throw new InconsistencyException("Cannot find candidate table");
    }
    return returnedTables;
  }
  
  public List<Table> getTables() throws InconsistencyException {
    List<Table> list = new ArrayList<>();
    String tablesString = this.properties.getProperty("ddl.tables", "");
    for (String t : tablesString.split("\\s+")) {
      Table table = new Table(t);
      String dependenciesString = this.properties.getProperty("ddl.table."+t+".dependencies", "").trim();
      if (dependenciesString.length() > 0)
        for (String d : dependenciesString.split("\\s+"))
          table.addDependency(d);
      list.add(table);
    }
    verifyTables(list);
    return list;
  }
  
  public static void verifyTables(List<Table> tables) throws InconsistencyException {
    Set<String> tableNames = new HashSet<>();
    for (Table table : tables) {
      if (tableNames.contains(table.getName()))
        throw new InconsistencyException("Table '"+table.getName()+"' multiple entries");
      tableNames.add(table.getName());
    }
    for (Table table : tables) {
      for (String dependency : table.getDependencies())
        if (! tableNames.contains(dependency))
          throw new InconsistencyException("Dependency '"+dependency+"' not found as table");
    }
  }
}

/*
 * Copyright Ⓒ 2019 Fred Vos
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.mokolo.commons.sql;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;

public class Table {
  @Getter private String name;
  @Getter private List<String> dependencies;
  
  public Table() {
    this.dependencies = new ArrayList<>();
  }
  
  public Table(String name) {
    this();
    this.name = name;
  }
  
  public void addDependency(String name) {
    this.dependencies.add(name);
  }
  
  public void removeDependency(String name) {
    this.dependencies.remove(name);
  }
}

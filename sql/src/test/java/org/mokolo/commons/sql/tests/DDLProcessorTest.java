/*
 * Copyright Ⓒ 2019 Fred Vos
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.mokolo.commons.sql.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

import lombok.Cleanup;
import lombok.extern.slf4j.Slf4j;

import org.junit.jupiter.api.Test;
import org.mokolo.commons.sql.DDLProcessor;

@Slf4j
public class DDLProcessorTest {

  @Test
  public void canListTablesInCreateOrder() throws Exception {
    DDLProcessor p = getDDLProcessorInitialized();
    List<String> tables = p.getTablesInCreateOrder();
    assertNotNull(tables, "Tables not null");
    assertEquals(2,  tables.size(), "Two tables");
    assertEquals("organisations", tables.get(0), "Table organisations first");
  }
  
  @Test
  public void canListTablesInDropOrder() throws Exception {
    DDLProcessor p = getDDLProcessorInitialized();
    List<String> tables = p.getTablesInDropOrder();
    assertNotNull(tables, "Tables not null");
    assertEquals(2,  tables.size(), "Two tables");
    assertEquals("departments", tables.get(0), "Table departments first");
  }
  
  @Test
  public void canProduceDDLStatements() throws Exception {
    DDLProcessor p = getDDLProcessorInitialized();
    List<String> statements;
    
    log.debug("CREATE:");
    statements = p.listCreateAll();
    for (String s : statements)
      log.debug(s);
    assertNotNull(statements, "Can create list of CREATE statements");
    assertEquals(9, statements.size(), "9 CREATE statements");
    assertTrue(statements.get(0).contains("CREATE TABLE organisations"), "First CREATE TABLE organisations");

    log.debug("DROP:");
    statements = p.listDropAll();
    for (String s : statements)
      log.debug(s);
    assertNotNull(statements, "Can create list of DROP statements");
    assertEquals(2, statements.size(), "2 DROP statements");
    assertTrue(statements.get(1).contains("DROP TABLE organisations"), "Last DROP TABLE organisations");
}
  
  private static DDLProcessor getDDLProcessorInitialized() throws IOException {
    @Cleanup InputStream is = DDLProcessorTest.class.getResourceAsStream("/ddl.properties");
    Properties properties = new Properties();
    properties.load(is);
    return new DDLProcessor(properties);
  }
}
